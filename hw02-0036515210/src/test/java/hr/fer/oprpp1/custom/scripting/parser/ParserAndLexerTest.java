package hr.fer.oprpp1.custom.scripting.parser;

import java.io.*;
import java.nio.charset.StandardCharsets;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

import hr.fer.oprpp1.custom.scripting.lexer.LexerException;
import hr.fer.oprpp1.custom.scripting.nodes.DocumentNode;
import hr.fer.oprpp1.custom.scripting.nodes.TextNode;

public class ParserAndLexerTest {
	
	@Test
	public void testEx1() {
		String s = this.readExample(1);
		SmartScriptParser parser = new SmartScriptParser(s);
		DocumentNode document = parser.getDocumentNode();
		Assertions.assertTrue(document.numberOfChildren() == 1);
		Assertions.assertEquals(document.getChild(0).getClass(), TextNode.class);
	}
	
	@Test
	public void testEx2() {
		String s = this.readExample(2);
		SmartScriptParser parser = new SmartScriptParser(s);
		DocumentNode document = parser.getDocumentNode();
		Assertions.assertTrue(document.numberOfChildren() == 1);
		Assertions.assertEquals(document.getChild(0).getClass(), TextNode.class);
	}
	
	@Test
	public void testEx3() {
		String s = this.readExample(3);
		SmartScriptParser parser = new SmartScriptParser(s);
		DocumentNode document = parser.getDocumentNode();
		Assertions.assertTrue(document.numberOfChildren() == 1);
		Assertions.assertEquals(document.getChild(0).getClass(), TextNode.class);
	}
	
	@Test
	public void testEx4() {
		String s = this.readExample(4);
		Assertions.assertThrows(LexerException.class, () ->{
			SmartScriptParser parser = new SmartScriptParser(s);
		});
	}
	
	@Test
	public void testEx5() {
		String s = this.readExample(5);
		Assertions.assertThrows(LexerException.class, () ->{
			SmartScriptParser parser = new SmartScriptParser(s);
		});
	}
	
	@Test
	public void testEx6() {
		String s = this.readExample(6);
		SmartScriptParser parser = new SmartScriptParser(s);
		DocumentNode document = parser.getDocumentNode();
		Assertions.assertTrue(document.numberOfChildren() == 1);
		Assertions.assertEquals(document.getChild(0).getClass(), TextNode.class);
	}
	
	@Test
	public void testEx7() {
		String s = this.readExample(7);
		Assertions.assertThrows(LexerException.class, () ->{
			SmartScriptParser parser = new SmartScriptParser(s);
		});
	}
	
	@Test
	public void testEx8() {
		String s = this.readExample(8);
		SmartScriptParser parser = new SmartScriptParser(s);
		DocumentNode document = parser.getDocumentNode();
		Assertions.assertTrue(document.numberOfChildren() == 1);
		Assertions.assertEquals(document.getChild(0).getClass(), TextNode.class);
	}
	
	@Test
	public void testEx9() {
		String s = this.readExample(9);
		Assertions.assertThrows(LexerException.class, () ->{
			SmartScriptParser parser = new SmartScriptParser(s);
		});
	}
	
	@Test
	public void testIsVariable() {
		String str = "ab_77";
		SmartScriptParser parser = new SmartScriptParser(str);
		Assertions.assertTrue(parser.isVariable(str));
	}
	
	@Test
	public void testIsFun() {
		String func = "@afasas";
		SmartScriptParser parser = new SmartScriptParser(func);
		Assertions.assertEquals(parser.findType(func), "function");
	}
	
	private String readExample(int n) {
		  try(InputStream is = this.getClass().getClassLoader().getResourceAsStream("extra/primjer"+n+".txt")) {
		    if(is==null) throw new RuntimeException("Datoteka extra/primjer"+n+".txt je nedostupna.");
		    byte[] data = is.readAllBytes();
		    String text = new String(data, StandardCharsets.UTF_8);
		    return text;
		  } catch(IOException ex) {
		    throw new RuntimeException("Greška pri čitanju datoteke.", ex);
		  }
	}
}
