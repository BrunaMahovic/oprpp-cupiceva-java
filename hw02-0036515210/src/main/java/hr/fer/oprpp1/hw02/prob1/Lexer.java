package hr.fer.oprpp1.hw02.prob1;

/**
 * Class represents a lexical analyzer.
 * 
 * @author Bruna
 *
 */
public class Lexer {

	private char[] data;      // ulazni tekst 
	private Token token;      // trenutni token
	private int currentIndex; // indeks prvog neobrađenog znaka
	LexerState state;
	
	/**
	 * Constructor tokenizes the text
	 * @param text string which is to be analyzed
	 */
	public Lexer(String text) {
		text.replaceAll("\r", " ");
		text.replaceAll("\n", " ");
		text.replaceAll("\t", " ");
		text.replace("\\\\", "\\");
		this.data = text.toCharArray();
		this.currentIndex = 0;
		this.state = LexerState.BASIC;
	}
	
	/**
	 * Finds next token
	 * @return next token
	 */
	public Token nextToken() {
		if (currentIndex > data.length || (data.length > 0 && data[data.length - 1] == '\\'))
			throw new LexerException("");
		
		if (currentIndex == data.length) {
			token = new Token(TokenType.EOF, null);
			currentIndex++;
		}
		
		token = new Token(TokenType.EOF, null);
		String result = "";
		int i;
		
		for (i = currentIndex; i < data.length; i++) {
			if (this.state.equals(LexerState.EXTENDED)) {
				result = "";
				while (i < data.length && !Character.isWhitespace(data[i]) && data[i] != '#')
					result = result + data[i++];

				if (result.length() != 0) {
					token = new Token(TokenType.WORD, result);
					break;
				}
			}
			
			if (Character.isLetter(data[i])) {
				if (i > 0 && data[i - 1] == '\\')
					throw new LexerException("");
				
				while (i < data.length && (Character.isLetter(data[i]) || (data[i] == '\\' && (Character.isDigit(data[i + 1]) || data[i] == '\\')))) {
					if (data[i] == '\\') {
						result = result + data[i + 1];
						i = i + 2;
					}
					
					else 
						result = result + data[i++];
				}
				token = new Token(TokenType.WORD, result);
				break;
			}
			
			if (Character.isDigit(data[i])) {
				if (i > 0 && data[i - 1] == '\\') {
					while (i < data.length && (Character.isDigit(data[i]) || Character.isLetter(data[i])))
						result = result + data[i++];
					token = new Token(TokenType.WORD, result);
				}
				
				else {
					while (i < data.length && Character.isDigit(data[i]))
						result = result + data[i++];
					try {
						token = new Token(TokenType.NUMBER, Long.valueOf(result));
					}
					catch (NumberFormatException e){
						throw new LexerException("");
					}
				}
				break;
			}
			
			int c = (int)data[i];
			if ((c > 32 && c < 48) || (c > 57 && c < 65) || (c > 90 && c < 97 && c != 92) || (c > 122 && c < 127)) {
				if (data[i] == '#') {
					this.setState(state == LexerState.BASIC ? LexerState.EXTENDED : LexerState.BASIC);
				}
				token = new Token(TokenType.SYMBOL, data[i++]);
				break;
			}
		}
		currentIndex = i;
		return token;
	}
	
	/**
	 * Gets token
	 * @return token
	 */
	public Token getToken() {
		return token;
	}
	
	/**
	 * Sets the lexer state
	 * @param state 
	 */
	public void setState(LexerState state) {
		if (state == null)
			throw new NullPointerException();
		this.state = state;
	}
}
