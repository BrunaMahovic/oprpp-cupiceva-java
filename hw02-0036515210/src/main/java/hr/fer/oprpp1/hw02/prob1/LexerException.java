package hr.fer.oprpp1.hw02.prob1;

/**
 * Class LexerException implements exception which happens if something went wrong with lexer analyzer.
 * 
 * @author Bruna
 *
 */
public class LexerException extends RuntimeException {

	private static final long serialVersionUID = 1L;

	/**
     * The action which happens if something went wrong.
     * @param message 
     */
    public LexerException(String message) {
    	super(message);
    }
}
