package hr.fer.oprpp1.hw02.prob1;

/**
 * Contains lexer states
 * 
 * @author Bruna
 *
 */
public enum LexerState {

	BASIC, EXTENDED
}
