package hr.fer.oprpp1.hw02.prob1;

/**
 * A lexical unit that groups one or more consecutive characters
 * 
 * @author Bruna
 *
 */
public class Token {

	TokenType type;
	Object value;
	
	/**
	 * Initializes token
	 * @param type token type
	 * @param value object
	 */
	public Token(TokenType type, Object value) {
		this.type = type;
		this.value = value;
	}
	
	/**
	 * Gets value
	 * @returnvalue
	 */
	public Object getValue() {
		return value;
	}
	
	/**
	 * Gets token type
	 * @return token type
	 */
	public TokenType getType() {
		return type;
	}
}
