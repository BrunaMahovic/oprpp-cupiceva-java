package hr.fer.oprpp1.hw02.demo;

import hr.fer.oprpp1.custom.scripting.lexer.*;
import hr.fer.oprpp1.custom.scripting.parser.SmartScriptParser;

public class SmartScriptTester {
	
	public static void main(String[] args) {
		String str = "This is sample text.\r\n"
				+ "{$ FOR i 1 10 1 $}\r\n"
				+ "  This is {$= i $}-th time this message is generated.\r\n"
				+ "{$END$}\r\n"
				+ "{$FOR i 0 10 2 $}\r\n"
				+ "  sin({$=i$}^2) = {$= i i * @sin  \"0.000\" @decfmt $}\r\n"
				+ "{$END$}";
		
		SmartScriptParser parser = new SmartScriptParser(str);
		/*SmartScriptLexer lexer = new SmartScriptLexer(str);
        while(!lexer.nextToken().getType().equals(TokenType.EOF)) {
            System.out.println(lexer.getToken().getType() + "    " + lexer.getToken().getValue());
        }*/
		
        parser.parse();
	}

}
