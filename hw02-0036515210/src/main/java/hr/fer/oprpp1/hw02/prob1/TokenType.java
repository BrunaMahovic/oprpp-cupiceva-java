package hr.fer.oprpp1.hw02.prob1;

/**
 * Contains token types
 * 
 * @author Bruna
 *
 */
public enum TokenType {
	EOF, WORD, NUMBER, SYMBOL
}
