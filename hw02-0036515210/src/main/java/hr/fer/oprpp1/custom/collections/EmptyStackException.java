package hr.fer.oprpp1.custom.collections;

/**
 * Class EmptyStackException implements exception which happens if stack is empty.
 * 
 * @author Bruna
 *
 */
public class EmptyStackException extends RuntimeException {
	
	private static final long serialVersionUID = 1L;

    /**
     * The action which happens if stack is empty.
     * @param message Stack is empty!
     */
    public EmptyStackException(String message) {
    	super("Stack is empty!");
    }
}
