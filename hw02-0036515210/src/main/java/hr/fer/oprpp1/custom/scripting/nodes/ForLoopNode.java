package hr.fer.oprpp1.custom.scripting.nodes;

import hr.fer.oprpp1.custom.scripting.elems.*;

/**
 * The class represents structured documents
 * 
 * @author Bruna
 *
 */
public class ForLoopNode extends Node {

	private ElementVariable variable;
	private Element startExpression;
	private Element endExpression;
	private Element stepExpression;
	
	public ForLoopNode(ElementVariable variable, Element startExpression, Element endExpression, Element stepExpression) {
		this.variable = variable;
		this.startExpression = startExpression;
		this.endExpression = endExpression;
		this.stepExpression = stepExpression;
	}
	
	public ElementVariable getVariable() {
		return this.variable;
	}
	
	public Element getStartExpression() {
		return this.startExpression;
	}
	
	public Element getEndExpression() {
		return this.endExpression;
	}
	
	public Element getStepExpression() {
		return this.stepExpression;
	}
}
