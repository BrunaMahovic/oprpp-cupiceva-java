package hr.fer.oprpp1.custom.scripting.nodes;

import hr.fer.oprpp1.custom.scripting.elems.Element;

/**
 * The class represents structured documents
 * 
 * @author Bruna
 *
 */
public class EchoNode extends Node {

	private Element[] elements;
	
	public EchoNode(Element[] elements) {
		this.elements = elements;
	}
	
	public Element[] getElements() {
		return this.elements;
	}
}
