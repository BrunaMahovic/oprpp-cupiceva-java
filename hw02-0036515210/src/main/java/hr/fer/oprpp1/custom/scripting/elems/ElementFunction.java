package hr.fer.oprpp1.custom.scripting.elems;

/**
 * Class ElementFunction is used to for the representation of function elements.
 * 
 * @author Bruna
 *
 */
public class ElementFunction extends Element {

	private String name;
	
	/**
	 * Constructor initializes given value
	 * @param name
	 */
	public ElementFunction(String name) {
		this.name = name;
	}
	
	/**
	 * Gets name
	 * @return name
	 */
	public String getName() {
		return this.name;
	}
	
	/**
	 * Returns string representation of value property
	 * @return string representation of value property
	 */
	@Override
	public String asText() {
		return this.getName();
	}
}
