package hr.fer.oprpp1.custom.scripting.elems;

/**
 * Class ElementString is used to for the representation of string elements.
 * 
 * @author Bruna
 *
 */
public class ElementString extends Element {
	
	private String value;
	
	/**
	 * Constructor initializes given value
	 * @param value
	 */
	public ElementString(String value) {
		this.value = value;
	}
	
	/**
	 * Gets value
	 * @return value
	 */
	public String getValue() {
		return this.value;
	}
	
	/**
	 * Returns string representation of value property
	 * @return string representation of value property
	 */
	@Override
	public String asText() {
		return this.getValue();
	}

}
