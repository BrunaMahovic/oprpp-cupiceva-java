package hr.fer.oprpp1.custom.collections;

import java.util.Arrays;
import java.util.ConcurrentModificationException;
import java.util.NoSuchElementException;

/**
 * Class ArrayIndexedCollection implements resizable array-backed collection of objects.
 * 
 * Note: duplicate elements are allowed and storage of null references is not allowed.
 * 
 * @author Bruna
 *
 */
public class ArrayIndexedCollection implements List {

	private int size = 0;
	private Object[] elements;
	private long modificationCount = 0;
	
	/**
	 * Class ArrayIndexedGetter implement methods for working with the elements.
	 * 
	 * @author Bruna
	 *
	 */
	private static class ArrayIndexedGetter implements ElementsGetter {
		ArrayIndexedCollection collection;
		int counter = 0;
		private long savedModificationCount;
		
		/**
		 * The ArrayIndexedGetter constructor which copies elements of one collection to a new one.
		 * @param collection collection which elements are copied in new collection
		 */
		public ArrayIndexedGetter(ArrayIndexedCollection collection) {
			this.collection = collection;
			this.savedModificationCount = collection.modificationCount;
		}
		
		/**
		 * Checks if there is next element 
		 * @return true if there is next element, otherwise false
		 */
		@Override
		public boolean hasNextElement() {
			if (savedModificationCount != collection.modificationCount)
				throw new ConcurrentModificationException();
			
			if (collection.size() > counter)
				return true;
			
			return false;
		}
		
		/**
		 * Gets next element from the collection
		 * @return next element from the collection
		 */
		@Override
		public Object getNextElement() {
			if (!hasNextElement())
				throw new NoSuchElementException();
			
			return collection.get(counter++);
		}
	}
	
	/**
	 * The default ArrayIndexedCollection constructor sets capacity to 16.
	 */
	public ArrayIndexedCollection() {
		this(16);
	}
	
	/**
	 * The ArrayIndexedCollection constructor preallocates the elements array.
	 * @param initialCapacity new capacity of elements array
	 * @throws IllegalArgumentException if initialCapacity < 1
	 */
	public ArrayIndexedCollection(int initialCapacity) {
		if (initialCapacity < 1) 
			throw new IllegalArgumentException();
		
		elements = new Object[initialCapacity];
	}
	
	/**
	 * The ArrayIndexedCollection constructor which copies elements of one collection to a new one.
	 * @param collection collection which elements are copied in new collection
	 */
	public ArrayIndexedCollection(Collection collection) {
		this(collection, 16);
	}
	
	/**
	 * The ArrayIndexedCollection constructor which copies elements of one collection to a new one and preallocates the elements array.
	 * @param collection collection which elements are copied in new collection
	 * @param initialCapacity new capacity of elements array
	 * @throws NullPointerException if collection is null
	 * @throws IllegalArgumentException if initialCapacity < 1
	 */
	public ArrayIndexedCollection(Collection collection, int initialCapacity) {
		if (collection == null)
			throw new NullPointerException();
		
		if (initialCapacity < 1) 
			throw new IllegalArgumentException();
		
		if (initialCapacity < collection.size()) 
			elements = new Object[collection.size()];
		
		else
			elements = new Object[initialCapacity];
		
		addAll(collection);
	}
	
	/**
	 * Creates new instance of ArrayIndexedGetter
	 */
	public ElementsGetter createElementsGetter() {
		return new ArrayIndexedGetter(this);
	}
	
	/**
	 * Ensures that the collection contains the specified element
	 * @param value an element added to the collection
	 * @throws NullPointerException if value is null
	 */
	@Override
	public void add(Object value) {
		if(value == null) 
			throw new NullPointerException();
		
		if (size != elements.length)
			elements[size++] = value;

		else {
			elements = Arrays.copyOf(elements, elements.length * 2);
			elements[size++] = value;
		}
		modificationCount++;
	}
	
	/**
	 * Finds the value of the indexed component in the specified array object.
	 * @param index the index
	 * @return the value of the indexed component
	 * @throws IndexOutOfBoundsException if indexes are not 0 to size-1
	 */
	public Object get(int index) {
		if (index < 0 || index > size - 1) 
			throw new IndexOutOfBoundsException();
		
		return elements[index];
	}
	
	/**
	 * Removes all of the elements from the collection.
	 */
	@Override
	public void clear() {
		while(!isEmpty())
			remove(0);
		modificationCount++;
	}
	
	/**
	 * Puts the given value at the given position in array.
	 * @param value the value which is to be put in array
	 * @param position position where the value will be put
	 * @throws IndexOutOfBoundsException if indexes are not 0 to size
	 * @throws NullPointerException if value is null
	 */
	public void insert(Object value, int position) {
		if (position < 0 || position > size) 
			throw new IndexOutOfBoundsException();
		
		if(value == null) 
			throw new NullPointerException();
		
		if (position == size)
			add(value);
		
		else {
			for (int i = size; i > position; i--)
				elements[i] = elements[i - 1];
			
			elements[position] = value;
			size = size + 1;
			modificationCount++;
		}
	}
	
	/**
	 * Searches for the index of the first occurrence of the given value.
	 * @param value the tested value
	 * @return returns the index of the first occurrence of the given value or -1 if the value is not found
	 */
	public int indexOf(Object value) {
		if (value == null)
			return -1;
		
		for (int i = 0; i < size; i++) {
			if (elements[i].equals(value)) 
				return i;
		}
		return -1;
	}
	
	/**
	 * Removes element at specified index from collection. 
	 * @param index position where the value is
	 * @throws IndexOutOfBoundsException if indexes are not 0 to size-1
	 */
	public void remove(int index) {
		if (index < 0 || index > size - 1) 
			throw new IndexOutOfBoundsException();
		
		for (int i = index; i < size; i++) {
			elements[i] = elements[i + 1];
		}
		size = size - 1;
		modificationCount++;
	}
	
	/**
	 * Checks if the collection contains objects.
	 * @return true if size() is 0, otherwise false
	 */
	@Override
	public boolean isEmpty() {
		return size() == 0;
	}
	
	/**
	 * Shows the number of elements in the collection
	 * @return the number of elements in this collection
	 */
	@Override
	public int size() {
		return this.size;
	}
	
	/**
	 * Checks if this collection contains the specified element
	 * @param value element which is to be tested to see if it is in a collection
	 * @return true if this collection contains the specified element, otherwise false
	 * @throws NullPointerException if value is null
	 */
	@Override
	public boolean contains(Object value) {
		if(value == null) 
			throw new NullPointerException();
		
		if (this.indexOf(value) != -1)
			return true;
		
		return false;
	}
	
	/**
	 * Removes a single instance of the specified element from this collection
	 * @param value element which is to be removed from this collection
	 * @return true if an element was removed, otherwise false
	 * @throws NullPointerException if value is null
	 */
	@Override
	public boolean remove(Object value) {
		if(value == null) 
			throw new NullPointerException();
		
		int result = this.indexOf(value);
		
		if(result != -1) {
			remove(result);
			return true;
		}
		return false;
	}
	
	/**
	 * Creates an array containing all of the elements in this collection
	 * @return an array containing all of the elements in this collection
	 */
	@Override
	public Object[] toArray() {
		Object[] objects = new Object[size];
		for (int i = 0; i < size; i++) {
			objects[i] = elements[i];
		}
		return objects;
	}
}
