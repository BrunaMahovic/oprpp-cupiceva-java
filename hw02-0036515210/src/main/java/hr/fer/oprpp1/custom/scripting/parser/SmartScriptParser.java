package hr.fer.oprpp1.custom.scripting.parser;

import hr.fer.oprpp1.custom.collections.*;
import hr.fer.oprpp1.custom.scripting.elems.*;
import hr.fer.oprpp1.custom.scripting.lexer.*;
import hr.fer.oprpp1.custom.scripting.nodes.*;

/**
 * Class SmartScriptParser implements parser for structured document format.
 * 
 * @author Bruna
 *
 */
public class SmartScriptParser {

	private SmartScriptLexer lexer;
	private ObjectStack stack;
	DocumentNode dNode;
	
	/**
	 * The SmartScriptParser constructor creates an instance of lexer and initialize it .
	 * @param document a string that contains document body
	 */
	public SmartScriptParser(String document) {
		lexer = new SmartScriptLexer(document);
		stack = new ObjectStack();
		parse();
	}
	
	/**
	 * The parser uses lexer for production of tokens
	 */
	public void parse() {
		dNode = new DocumentNode();
		stack.push(dNode);
		ElementVariable variable;
		Element startExpression = null;
		Element endExpression = null;
		Element stepExpression = null;
		ForLoopNode fNode;
		EchoNode eNode;
		char parent = 'D';
		ArrayIndexedCollection array = new ArrayIndexedCollection();
		
		Token token = lexer.nextToken();
		while (token.getType() != TokenType.EOF) {
			
			if (token.getType() == TokenType.TEXT) {
				if (parent == 'D') {
					TextNode tNode = new TextNode(token.getValue().toString());
					dNode = (DocumentNode) stack.pop();
					dNode.addChildNode(tNode);
					stack.push(dNode);
				}
				
				else {
					TextNode tNode = new TextNode(token.getValue().toString());
					fNode = (ForLoopNode) stack.pop();
					fNode.addChildNode(tNode);
					stack.push(fNode);
				}
				
				token = lexer.nextToken();
			}
			
			if (token.getType() == TokenType.FOR) {
				token = lexer.nextToken();
				
				if (!isVariable(token.getValue().toString())) 
					throw new SmartScriptParserException("");
				variable = new ElementVariable(token.getValue().toString());
				token = lexer.nextToken();
				
				String type = findType(token.getValue().toString());
				try {
					switch (type) {
					case "variable":
						startExpression = new ElementVariable(token.getValue().toString());
						break;
					case "integer":
						startExpression = new ElementConstantInteger(Integer.valueOf(token.getValue().toString()));
						break;
					case "double":
						startExpression = new ElementConstantDouble(Double.valueOf(token.getValue().toString()));
						break;
					case "string":
						startExpression = new ElementString(token.getValue().toString());
						break;
					default:
						throw new SmartScriptParserException("");
					}
				}
				catch (Exception e) {
					throw new SmartScriptParserException("");
				}
				token = lexer.nextToken();
				
				String type2 = findType(token.getValue().toString());
				try {
					switch (type2) {
					case "variable":
						endExpression = new ElementVariable(token.getValue().toString());
						break;
					case "integer":
						endExpression = new ElementConstantInteger(Integer.valueOf(token.getValue().toString()));
						break;
					case "double":
						endExpression = new ElementConstantDouble(Double.valueOf(token.getValue().toString()));
						break;
					case "string":
						endExpression = new ElementString(token.getValue().toString());
						break;
					default:
						throw new SmartScriptParserException("");
					}
				}
				catch (Exception e) {
					throw new SmartScriptParserException("");
				}
				token = lexer.nextToken();
				
				if (token.getType() == TokenType.FOR) {
					String type3 = findType(token.getValue().toString());
					try {
						switch (type3) {
						case "variable":
							stepExpression = new ElementVariable(token.getValue().toString());
							break;
						case "integer":
							stepExpression = new ElementConstantInteger(Integer.valueOf(token.getValue().toString()));
							break;
						case "double":
							stepExpression = new ElementConstantDouble(Double.valueOf(token.getValue().toString()));
							break;
						case "string":
							stepExpression = new ElementString(token.getValue().toString());
							break;
						case "function":
							stepExpression = new ElementString(token.getValue().toString());
							break;
						case "operator":
							stepExpression = new ElementString(token.getValue().toString());
							break;
						default:
							throw new SmartScriptParserException("");
						}
					}
					catch (Exception e) {
						throw new SmartScriptParserException("");
					}
					token = lexer.nextToken();
				}
				fNode = new ForLoopNode(variable, startExpression, endExpression, stepExpression);
				stack.push(fNode);
				
				parent = 'F';
			}
			
			if (token.getType() == TokenType.VARIABLE) {
				if (parent == 'D')
					throw new SmartScriptParserException("");
				
				token = lexer.nextToken();
				while (token.getType() == TokenType.VARIABLE) {
					array.add(token.getValue());
					token = lexer.nextToken();
				}
				
				eNode = new EchoNode((Element[]) array.toArray());
				fNode = (ForLoopNode) stack.pop();
				fNode.addChildNode(eNode);
				stack.push(fNode);
			}
			
			if (token.getType() == TokenType.END) {
				if (parent == 'D')
					throw new SmartScriptParserException("");
				
				fNode = (ForLoopNode) stack.pop();
				dNode = (DocumentNode) stack.pop();
				dNode.addChildNode(fNode);
				stack.push(dNode);
				
				token = lexer.nextToken();
				parent = 'D';
			}
		}
	}
	
	/**
	 * Checks if given value is variable
	 * @param value checked value
	 * @return true if value is variable, otherwise false
	 */
	public boolean isVariable(String value) {
		int index = 0;
		if (value.length() > 0 && Character.isLetter(value.charAt(index++))) {
			
			while (index < value.length()) {
				if (!Character.isLetter(value.charAt(index)) && !Character.isDigit(value.charAt(index)) && value.charAt(index) != '_')
					return false;
				index++;
			}
		}
		return true;
	}
	
	/**
	 * Find type for given value
	 * @param value the value we want to specify the type
	 * @return value type
	 */
	public String findType(String value) {
		if (value.startsWith("@"))
			return "function";
		
		if (value.equals("+|-|*|/|^"))
			return "operator";
		
		if (isVariable(value))
			return "variable";
		
		try {
			int i = Integer.valueOf(value);
			return "integer";
		}
		catch (NumberFormatException e){	
		}
		
		try {
			String[] parts = value.split(".");
			int i = Integer.valueOf(parts[0]);
			int j = Integer.valueOf(parts[1]);
			return "double";
		}
		catch (NumberFormatException e){	
		}
		
		if (value.charAt(0) != '"' && value.charAt(value.length() - 1) != '"') 
			return "string";
		
		return "";
	}
	
	/**
	 * Gets initialized document node
	 * @return initialized document node
	 */
	public DocumentNode getDocumentNode() {
		return dNode;
	}
}
