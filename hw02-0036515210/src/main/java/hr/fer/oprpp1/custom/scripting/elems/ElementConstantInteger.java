package hr.fer.oprpp1.custom.scripting.elems;

/**
 * Class ElementConstantInteger is used to for the representation of integer elements.
 * 
 * @author Bruna
 *
 */
public class ElementConstantInteger extends Element {

	private int value;
	
	/**
	 * Constructor ElementConstantInteger initialize given value
	 * @param value
	 */
	public ElementConstantInteger(int value) {
		this.value = value;
	}
	
	/**
	 * Gets value
	 * @return value
	 */
	public int getValue() {
		return this.value;
	}
	
	/**
	 * Returns string representation of value property
	 * @return string representation of value property
	 */
	@Override
	public String asText() {
		return String.valueOf(this.getValue());
	}
}
