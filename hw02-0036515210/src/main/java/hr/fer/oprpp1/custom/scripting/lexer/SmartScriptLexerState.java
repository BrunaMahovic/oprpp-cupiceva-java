package hr.fer.oprpp1.custom.scripting.lexer;

/**
 * Contains lexer states
 * 
 * @author Bruna
 *
 */public enum SmartScriptLexerState {

	EOF, IN_FOR, IN_VARIABLE
}
