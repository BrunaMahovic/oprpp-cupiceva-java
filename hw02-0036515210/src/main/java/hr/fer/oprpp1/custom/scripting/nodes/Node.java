package hr.fer.oprpp1.custom.scripting.nodes;

import hr.fer.oprpp1.custom.collections.ArrayIndexedCollection;

/**
 * The class represents structured documents
 * 
 * @author Bruna
 *
 */
public class Node {

	private ArrayIndexedCollection arrayColl;
	
	/**
	 * Adds node to collection
	 * @param child
	 */
	public void addChildNode(Node child) {
		if (arrayColl == null) 
			arrayColl = new ArrayIndexedCollection();
		arrayColl.add(child);
	}
	
	/**
	 * Gives number of chhildren nodes 
	 * @return number of children nodes
	 */
	public int numberOfChildren() {
		if (arrayColl == null)
			return 0;
		return arrayColl.size();
	}
	
	/**
	 * Gets child node
	 * @param index index of child
	 * @return child node
	 */
	public Node getChild(int index) {
		if (arrayColl == null)
			throw new NullPointerException();
		if (index < 0 || index > arrayColl.size() - 1) 
			throw new IndexOutOfBoundsException();
		return (Node)arrayColl.get(index);
	}
}
