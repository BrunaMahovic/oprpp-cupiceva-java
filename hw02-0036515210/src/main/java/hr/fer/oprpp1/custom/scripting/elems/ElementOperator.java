package hr.fer.oprpp1.custom.scripting.elems;

/**
 * Class ElementOperator is used to for the representation of operator elements.
 * 
 * @author Bruna
 *
 */
public class ElementOperator extends Element {

	private String symbol;
	
	/**
	 * Constructor initializes given value
	 * @param symbol
	 */
	public ElementOperator(String symbol) {
		this.symbol = symbol;
	}
	
	/**
	 * Gets symbol
	 * @return symbol
	 */
	public String getSymbol() {
		return this.symbol;
	}
	
	/**
	 * Returns string representation of value property
	 * @return string representation of value property
	 */
	@Override
	public String asText() {
		return this.getSymbol();
	}
}
