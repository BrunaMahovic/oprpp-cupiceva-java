package hr.fer.oprpp1.custom.scripting.elems;

/**
 * Class ElementVariable is used to for the representation of variable elements.
 * 
 * @author Bruna
 *
 */
public class ElementVariable extends Element {

	private String name;
	
	/**
	 * Constructor initializes given value
	 * @param name
	 */
	public ElementVariable(String name) {
		this.name = name;
	}
	
	/**
	 * Gets name
	 * @return name
	 */
	public String getName() {
		return this.name;
	}
	
	/**
	 * Returns string representation of value property
	 * @return string representation of value property
	 */
	@Override
	public String asText() {
		return this.getName();
	}
}
