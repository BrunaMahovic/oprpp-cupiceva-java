package hr.fer.oprpp1.custom.scripting.lexer;

/**
 * Contains token types
 * 
 * @author Bruna
 *
 */
public enum TokenType {
	EOF, FOR, END, TEXT, VARIABLE
}
