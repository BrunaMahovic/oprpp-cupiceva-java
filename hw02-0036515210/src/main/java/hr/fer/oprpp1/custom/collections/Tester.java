package hr.fer.oprpp1.custom.collections;

/**
 * Interface Tester has method for testing some objects
 * 
 * @author Bruna
 *
 */
public interface Tester {

	/**
	 * Tests given object
	 * @param obj object which is to be tested
	 * @return true if is tested, otherwise false
	 */
	public abstract boolean test(Object obj); 
}
