package hr.fer.oprpp1.custom.scripting.elems;

/**
 * Class Element is used to for the representation of expressions.
 * 
 * @author Bruna
 *
 */
public class Element {

	/**
	 * Makes string from given value
	 * @return empty string
	 */
	public String asText() {
		return "";
	}
}
