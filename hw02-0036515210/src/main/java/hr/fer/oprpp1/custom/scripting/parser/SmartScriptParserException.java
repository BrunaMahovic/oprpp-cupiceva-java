package hr.fer.oprpp1.custom.scripting.parser;

/**
 * Class implements exception which happens if something went wrong with parser.
 * 
 * @author Bruna
 *
 */
public class SmartScriptParserException extends RuntimeException {

	private static final long serialVersionUID = 1L;

	/**
     * The action which happens if something went wrong.
     * @param message 
     */
    public SmartScriptParserException(String message) {
    	super(message);
    }
}
