package hr.fer.oprpp1.custom.scripting.lexer;

import java.util.IllegalFormatException;

/**
 * Class SmartScriptLexer represents a lexical analyzer.
 * 
 * @author Bruna
 *
 */
public class SmartScriptLexer {

	private char[] data;
	private Token token;
	private int currentIndex;
	SmartScriptLexerState state;
	
	/**
	 * Constructor tokenizes the text
	 * @param text string which is to be analyzed
	 */
	public SmartScriptLexer(String document) {
		this.data = document.toCharArray();
		this.currentIndex = 0;
		state = SmartScriptLexerState.EOF;
		if (document.contains("\\") && !(document.contains("\\{")) && !(document.contains("\\\\")))
			throw new LexerException("");
	}
	
	/**
	 * Finds next token
	 * @return next token
	 */
	public Token nextToken() {
		token = new Token(TokenType.EOF, null);
		
		String result = "";
		int i = currentIndex;
		
		while (i < data.length) {
			
			if (state == SmartScriptLexerState.IN_FOR) {
				while (Character.isWhitespace(data[i])) 
					i++;
				
				while (i < data.length - 2 && !Character.isWhitespace(data[i])) {
					result = result + data[i];
					if (data[i + 1] == '$' && data[i + 2] == '}')
						break;
					i++;
				}
					
				token = new Token(TokenType.FOR, result);
				
				if (data[i + 1] == '$' && data[i + 2] == '}') {
					i = i + 3;
					state = SmartScriptLexerState.EOF;
				}
				break;
			}
			
			else if (state == SmartScriptLexerState.IN_VARIABLE) {
				while (Character.isWhitespace(data[i])) 
					i++;
				
				while (i < data.length - 2 && !Character.isWhitespace(data[i])) {
					result = result + data[i];
					if (data[i + 1] == '$' && data[i + 2] == '}')
						break;
					i++;
				}
				 
				token = new Token(TokenType.VARIABLE, result);
				
				if (data[i + 1] == '$' && data[i + 2] == '}') {
					i = i + 3;
					state = SmartScriptLexerState.EOF;
				}
				break;
			}
			
			else {
				boolean text =  false;
				while (i < data.length - 1 && data[i] != '{' && data[i + 1] != '$' && this.state == SmartScriptLexerState.EOF) {
					if (i < data.length - 2 && data[i + 1] == '{' && data[i + 2] == '$')
						text = true;
					result = result + data[i++];
					if (i == data.length - 1)
						text = true;
				}
				
				if (data[i] == '{' && data[i + 1] == '$' && state == SmartScriptLexerState.EOF) {
					i = i + 2;
					while (Character.isWhitespace(data[i])) 
						i++;
					
					try {
						if (Character.toUpperCase(data[i]) == 'F' && Character.toUpperCase(data[i + 1]) == 'O' && Character.toUpperCase(data[i + 2]) == 'R') {
							result = result + data[i] + data[++i] + data[++i];
							++i;
							state = SmartScriptLexerState.IN_FOR;
							token = new Token(TokenType.FOR, result);
							break;
						}
						
						else if (Character.toUpperCase(data[i]) == 'E' && Character.toUpperCase(data[i + 1]) == 'N' && Character.toUpperCase(data[i + 2]) == 'D') {
							result = result + data[i] + data[++i] + data[++i];
							
							while (data[i - 1] != '$' && data[i] != '}')
								i++;
							
							i++;
							state = SmartScriptLexerState.EOF;
							token = new Token(TokenType.END, result);
							break;
						}
					}
					catch (IllegalFormatException e) {
						throw new LexerException("");
					}
				}
				
				else if (text) {
					token = new Token(TokenType.TEXT, result);
					break;
				}	
			}
			i++;
		}
		
		currentIndex = i;
		return token;
	}
	
	/**
	 * Gets token
	 * @return token
	 */
	public Token getToken() {
		return token;
	}
}
