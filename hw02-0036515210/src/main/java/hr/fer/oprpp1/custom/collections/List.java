package hr.fer.oprpp1.custom.collections;

public interface List extends Collection {
	
	/**
	 * Finds the value of the indexed component in the specified collection.
	 * @param index the index
	 * @return the value of the indexed component
	 */
	Object get(int index);
	
	
	/**
	 * Puts the given value at the given position in collection.
	 * @param value the value which is to be put in collection
	 * @param position position where the value will be put
	 */
	void insert(Object value, int position);
	
	
	/**
	 * Searches for the index of the first occurrence of the given value.
	 * @param value the tested value
	 * @return returns the index of the first occurrence of the given value or -1 if the value is not found
	 */
	int indexOf(Object value);
	
	/**
	 * Removes element at specified index from collection. 
	 * @param index position where the value is
	 */
	void remove(int index);

}
