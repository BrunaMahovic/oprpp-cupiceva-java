package hr.fer.oprpp1.custom.scripting.elems;

/**
 * Class ElementConstantDouble is used to for the representation of double elements.
 * 
 * @author Bruna
 *
 */
public class ElementConstantDouble extends Element {

	private double value;
	
	/**
	 * Constructor ElementConstantDouble initialize given value
	 * @param value
	 */
	public ElementConstantDouble(double value) {
		this.value = value;
	}
	
	/**
	 * Gets value
	 * @return value
	 */
	public double getValue() {
		return this.value;
	}
	
	/**
	 * Returns string representation of value property
	 * @return string representation of value property
	 */
	@Override
	public String asText() {
		return String.valueOf(this.getValue());
	}
}
