package hr.fer.oprpp1.custom.collections;

/**
 * Class Dictionary<K,V> remembers the given value under the default key. A Map-like structure.
 * 
 * @author Bruna
 *
 */
public class Dictionary<K,V> {

	private ArrayIndexedCollection<DictionaryMap<K,V>> array = new ArrayIndexedCollection<>();
	
	/**
	 * A class DictionaryMap<K,V> allows the memory of two data: key and value.
	 * 
	 * @author Bruna
	 *
	 */
	private class DictionaryMap<K,V> {
		private K key;
		private V value; 
		
		/**
		 * Constructor initializes given key and value.
		 * @param key given key
		 * @param value given value
		 */
		public DictionaryMap(K key, V value) {
			this.key = key;
			this.value = value;
		}
		
		/**
		 * Gets the key from DictionaryMap.
		 * @return key
		 */
		public K getKey() {
			return this.key;
		}
		
		/**
		 * Gets the value from DictionaryMap.
		 * @return value
		 */
		public V getValue() {
			return this.value;
		}
		
		/**
		 * Changes the value of the key in the DictionaryMap.
		 * @param key new key
		 */
		public void setKey(K key) {
			this.key = key;
		}
		
		/**
		 * Changes the value of the value in the DictionaryMap.
		 * @param value new value
		 */
		public void setValue(V value) {
			this.value = value;
		}
	}
	
	/**
	 * Checks if Dictionary doesn't contain elements.
	 * @return true if size is 0, otherwise false
	 */
	public boolean isEmpty() {
		if (this.size() > 0)
			return false;
		return true;
	}
	
	/**
	 * Shows the number of elements in Dictionary
	 * @return the number of elements in this Dictionary
	 */
	public int size() {
		return array.size();
	}
	
	/**
	 * Removes all of the elements from Dictionary
	 */
	public void clear() {
		array.clear();
	}
	
	/**
	 * Puts a new record or writes a new value over an existing record.
	 * @param key given key
	 * @param value new value
	 * @return if the key already exists then it returns the old value, otherwise null
	 * @throws NullPointerException if key is null
	 */
	public V put(K key, V value) {
		if (key == null)
			throw new NullPointerException();
			
		V result = null;
		for (int i = 0; i < array.size(); i++) {
			DictionaryMap<K,V> pair = array.get(i);
			
			if (pair.getKey().equals(key)) {
				result = pair.getValue();
				pair.setValue(value);
			}
		}
		
		if (result == null) {
			array.add(new DictionaryMap<K,V>(key, value));
		}
		
		return result;
	}
	
	/**
	 * Gets the value under a given key.
	 * @param key given key
	 * @return returns the value under a given key
	 * @throws NullPointerException if key is null
	 */
	public V get(Object key) {
		if (key == null)
			throw new NullPointerException();
		
		for (int i = 0; i < array.size(); i++) {
			DictionaryMap<K,V> pair = array.get(i);
			
			if (pair.getKey().equals(key))
				return pair.getValue();
		}
		return null;
	}
	
	/**
	 * Deletes the entire record under the given key.
	 * @param key given key
	 * @return returns the value under a given key if it existed, otherwise null
	 * @throws NullPointerException if key is null
	 */
	public V remove(K key) {
		if (key == null)
			throw new NullPointerException();
			
		V result = null;
		for (int i = 0; i < array.size(); i++) {
			DictionaryMap<K,V> pair = array.get(i);
			
			if (pair.getKey().equals(key)) {
				result = pair.getValue();
				array.remove(pair);
			}
		}
		return result;
	}
}
