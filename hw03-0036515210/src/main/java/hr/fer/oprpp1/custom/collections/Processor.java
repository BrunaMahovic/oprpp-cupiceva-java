package hr.fer.oprpp1.custom.collections;

/**
 * The Processor is a model of an object capable of performing some operation on the passed object. 
 * 
 * @author Bruna
 *
 */
public interface Processor<T> {
	
	/**
	 * Performs the given action
	 * @param value the element over which the action is performed
	 */
	public abstract void process(T value);
}
