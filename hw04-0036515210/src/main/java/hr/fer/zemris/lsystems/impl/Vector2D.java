package hr.fer.zemris.lsystems.impl;

/**
 * Class Vector2D models a 2D vector whose components are real numbers x and y.
 * 
 * @author Bruna
 *
 */
public class Vector2D {

	private double x;
	private double y;
	
	/**
	 * Constructor initializes given values.
	 * @param x first number
	 * @param y second number
	 */
	public Vector2D(double x, double y) {
		this.x = x;
		this.y = y;
	}
	
	/**
	 * Gets X parameter.
	 * @return X
	 */
	public double getX() {
		return this.x;
	}
	
	/**
	 * Gets Y parameter.
	 * @return Y
	 */
	public double getY() {
		return this.y;
	}
	
	/**
	 * Adds another Vector2D to the current vector.
	 * @param offset
	 */
	public void add(Vector2D offset) {
		this.x = this.getX() + offset.getX();
		this.y = this.getY() + offset.getY();
	}
	
	/** 
	 * Creates a vector by summing two vectors.
	 * @param offset vector
	 * @return new vector
	 */
	public Vector2D added(Vector2D offset) {
		return new Vector2D(this.getX() + offset.getX(), this.getY() + offset.getY());
	}
	
	/**
	 * Rotates current vector by given angle.
	 * @param angle
	 */
	public void rotate(double angle) {
		 double first = (this.getX() * Math.cos(angle)) - (this.getY() * Math.sin(angle));
		 double second = (this.getX() * Math.sin(angle)) + (this.getY() * Math.cos(angle));
		 this.x = first;
		 this.y = second;
	}
	
	/**
	 * Creates a vector by rotating current vector by given angle.
	 * @param angle
	 * @return new vector
	 */
	public Vector2D rotated(double angle) {
		double first = (this.x * Math.cos(angle)) - (this.y * Math.sin(angle));
		double second = (this.x * Math.sin(angle)) + (this.y * Math.cos(angle));
		
		return new Vector2D(first, second);
	}
	
	/**
	 * Scales current vector for given scaler.
	 * @param scaler
	 */
	public void scale(double scaler) {
		this.x = this.getX() * scaler;
		this.y = this.getY() * scaler;
	}
	
	/**
	 * Creates a vector by scaling current vector for given scaler.
	 * @param scaler
	 * @return new vector
	 */
	public Vector2D scaled(double scaler) {
		return new Vector2D(this.getX() * scaler, this.getY() * scaler);
	}
	
	/**
	 * Copies one vector to another.
	 * @return copy of vector
	 */
	public Vector2D copy() {
		return new Vector2D(this.getX(), this.getY());
	}
}
