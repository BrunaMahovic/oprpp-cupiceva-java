package hr.fer.zemris.lsystems.impl;

import hr.fer.zemris.lsystems.Painter;

/**
 * Class Command represents an action for the command.
 * 
 * @author Bruna
 *
 */
public interface Command {

	/**
	 * Implements an action for the command.
	 * @param ctx object stack
	 * @param painter 1f
	 */
	void execute(Context ctx, Painter painter);
}
