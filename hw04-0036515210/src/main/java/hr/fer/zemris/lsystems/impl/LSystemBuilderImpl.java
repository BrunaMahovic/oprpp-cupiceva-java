package hr.fer.zemris.lsystems.impl;

import java.awt.Color;
import java.lang.reflect.Field;

import hr.fer.zemris.lsystems.impl.commands.*;
import hr.fer.oprpp1.custom.collections.*;
import hr.fer.zemris.lsystems.*;

/**
 * Class LSystemBuilderImpl	models configurable objects and then builds one specific Lindermayer's system to its default configuration.
 * 
 * @author Bruna
 *
 */
public class LSystemBuilderImpl implements LSystemBuilder {

	Dictionary<Character, String> dictionaryProductions = new Dictionary<>();
	Dictionary<Character, Command> dictionaryActions = new Dictionary<>();
	private double unitLength = 0.1;
	private double unitLengthDegreeScaler = 1;
	private Vector2D origin = new Vector2D(0, 0);
	private double angle = 0;
	private String axiom = "";
	
	/**
	 * Sets the unit length.
	 */
	@Override
	public LSystemBuilder setUnitLength(double unitLength) {
		this.unitLength = unitLength;
		return this;
	}
	
	/**
	 * Sets the origin.
	 */
	@Override
	public LSystemBuilder setOrigin(double x, double y) {
		this.origin = new Vector2D(x, y);
		return this;
	}
	
	/**
	 * Sets the angle.
	 */
	@Override
	public LSystemBuilder setAngle(double angle) {
		this.angle = angle;
		return this;
	}
	
	/**
	 * Sets the axiom.
	 */
	@Override
	public LSystemBuilder setAxiom(String axiom) {
		this.axiom = axiom;
		return this;
	}
	
	/**
	 * Sets the unit length degree scaler.
	 */
	@Override
	public LSystemBuilder setUnitLengthDegreeScaler(double unitLengthDegreeScaler) {
		this.unitLengthDegreeScaler = unitLengthDegreeScaler;
		return this;
	}
	
	/**
	 * Puts given elements into a dictionary of productions.
	 */
	@Override
	public LSystemBuilder registerProduction(char symbol, String production) {
		dictionaryProductions.put(symbol, production);
		return this;
	}
	
	/**
	 * Puts given elements into a dictionary of commands.
	 */
	@Override
	public LSystemBuilder registerCommand(char symbol, String action) {
		String[] parts = action.split(" ");
		switch(parts[0]) {
		case "draw":
			dictionaryActions.put(symbol, new DrawCommand(Double.parseDouble(parts[1])));
			break;
		case "skip":
			dictionaryActions.put(symbol, new SkipCommand(Double.parseDouble(parts[1])));
			break;
		case "scale":
			dictionaryActions.put(symbol, new ScaleCommand(Double.parseDouble(parts[1])));
			break;
		case "rotate":
			dictionaryActions.put(symbol, new RotateCommand(Double.parseDouble(parts[1])));
			break;
		case "push":
			dictionaryActions.put(symbol, new PushCommand());
			break;
		case "pop":
			dictionaryActions.put(symbol, new PopCommand());
			break;
		case "color":
			dictionaryActions.put(symbol, new ColorCommand(Color.decode("#" + parts[1])));
			break;
		}
		return this;
	}
	
	/**
	 * Configures directives from an array of String lines.
	 */
	@Override
	public LSystemBuilder configureFromText(String[] lines) {
		for (int i = 0; i < lines.length; i++) {
			String[] parts = lines[i].split("\\s+");

			switch(parts[0]) {
			case "origin":
				this.setOrigin(Double.parseDouble(parts[1]), Double.parseDouble(parts[2]));
				break;
			case "angle":
				this.setAngle(Double.parseDouble(parts[1]));
				break;
			case "unitLength":
				this.setUnitLength(Double.parseDouble(parts[1]));
				break;
			case "unitLengthDegreeScaler":
				if (parts[2].length() > 1) {
					String first = parts[2].substring(0, 1);
					String second = parts[2].substring(1);
					this.setUnitLengthDegreeScaler(Double.parseDouble(parts[1])/Double.parseDouble(second));
				}
				else
					this.setUnitLengthDegreeScaler(Double.parseDouble(parts[1])/Double.parseDouble(parts[3]));
				break;
			case "axiom":
				this.setAxiom(parts[1]);
				break;
			case "command":
				if(parts.length > 3)
					this.registerCommand(parts[1].charAt(0), parts[2] + " " + parts[3]);  
				else 
					this.registerCommand(parts[1].charAt(0), parts[2]);
				break;
				
			case "production":
				this.registerProduction(parts[1].charAt(0), parts[2]);
				break;
			}
		}
		return this;
	}
	
	/**
	 * Creates one specific LSystem.
	 */
	@Override
	public LSystem build() {
		/**
		 * Class LS models one Lindermayer's system.
		 * @author Bruna
		 *
		 */
		class LS implements LSystem {
			
			/**
			 * Generates a string after a given number of production application levels.
			 */
			@Override
			public String generate(int level) {
				if (level == 0)
					return axiom;
				
				StringBuilder sb = new StringBuilder();
				sb.append(axiom);
				
				for (int i = 0; i < level; i++) {
					char[] sign = sb.toString().toCharArray();
					sb.setLength(0);
					
					for (int j = 0; j < sign.length; j++) {
						String key = dictionaryProductions.get(sign[j]);
						if (key != null)
							sb.append(key);
						else
							sb.append(sign[j]);
					}
					
				}
				return sb.toString();
			}
			
			/**
			 * Draws the resulting fractal using a received line drawing object that is of type Painter. 
			 */
			@Override
			public void draw(int level, Painter painter) {
				Context ctx = new Context();
				TurtleState state = new TurtleState(origin, new Vector2D(Math.cos(angle*Math.PI/180), Math.sin(angle*Math.PI/180)), 
						Color.BLACK, unitLength * (Math.pow(unitLengthDegreeScaler, level)));
				ctx.pushState(state);
				String generated = generate(level);
				
				for (char s: generated.toCharArray()) {
					Command cmd = dictionaryActions.get(s);
					if(cmd != null)
						cmd.execute(ctx, painter);
				}
			}
		}
		return new LS();
	}
}
