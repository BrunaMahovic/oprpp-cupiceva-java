package hr.fer.zemris.lsystems.impl.commands;

import hr.fer.zemris.lsystems.Painter;
import hr.fer.zemris.lsystems.impl.*;

/**
 * Class RotateCommand represents an action for the command.
 * 
 * @author Bruna
 *
 */
public class RotateCommand implements Command {

	private double angle;
	
	/**
	 * Constructor initializes given angle.
	 * @param angle
	 */
	public RotateCommand(double angle) {
		this.angle = angle * Math.PI / 180;
	}
	
	/**
	 * Modifies the turtle's direction vector in the current state by rotating it by a given angle.
	 */
	@Override
	public void execute(Context ctx, Painter painter) {
		TurtleState state = ctx.getCurrentState();
		Vector2D vector = state.getDirection();
		state.setDirection(vector.rotated(this.angle));
	}
}
