package hr.fer.zemris.lsystems.impl;

import hr.fer.oprpp1.custom.collections.ObjectStack;

/**
 * Class Context performs the fractal display procedure and offers a stack on which it is possible to place and retrieve turtle states.
 * 
 * @author Bruna
 *
 */
public class Context {

	ObjectStack stack = new ObjectStack();
	
	/**
	 * Gets the state from the top of the stack without removal.
	 * @return current state
	 */
	public TurtleState getCurrentState() {
		return (TurtleState) stack.peek();
	}
	
	/**
	 * Pushes given state on the stack.
	 * @param state state pushed on the stack
	 */
	public void pushState(TurtleState state) {
		stack.push(state);
	}
	
	/**
	 * Removes last state pushed on stack from stack.
	 */
	public void popState() {
		stack.pop();
	}
}
