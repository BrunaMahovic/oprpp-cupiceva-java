package hr.fer.zemris.lsystems.impl.commands;

import hr.fer.zemris.lsystems.Painter;
import hr.fer.zemris.lsystems.impl.*;

/**
 * Class PushCommand represents an action for the command.
 * 
 * @author Bruna
 *
 */
public class PushCommand implements Command {

	/**
	 * Copies the state from the top and the copy places on the stack.
	 */
	@Override
	public void execute(Context ctx, Painter painter) {
		TurtleState state = ctx.getCurrentState().copy();
		ctx.pushState(state);
	}
}
