package hr.fer.zemris.lsystems.impl.commands;

import hr.fer.zemris.lsystems.Painter;
import hr.fer.zemris.lsystems.impl.*;

/**
 * Class DrawCommand represents an action for the command.
 * 
 * @author Bruna
 *
 */
public class DrawCommand implements Command {

	private double step;
	
	/**
	 * Constructor initializes given value.
	 * @param step
	 */
	public DrawCommand(double step) {
		this.step = step;
	}
	
	/**
	 * Draws a line with default color from the current turtle position to the calculated one and remembers the new turtle position in the current state.
	 */
	@Override
	public void execute(Context ctx, Painter painter) {
		TurtleState state = ctx.getCurrentState();
		double len = Math.sqrt(Math.pow(state.getDirection().getX(), 2) + Math.pow(state.getDirection().getY(), 2));
		Vector2D vector = state.getDirection().scaled(this.step * state.getLength() / len);
		//System.out.println(state.getLength());

		painter.drawLine(state.getCurrentPosition().getX(), 
				state.getCurrentPosition().getY(), 
				state.getCurrentPosition().getX() + vector.getX(), 
				state.getCurrentPosition().getY() + vector.getY(), 
				state.getColor(), 
				1f);
		state.setCurrentPosition(state.getCurrentPosition().added(vector));
	}
}
