package hr.fer.oprpp1.hw04.db;

/**
 * Class Lexer represents a lexical analyzer.
 * 
 * @author Bruna
 *
 */
public class Lexer {

	private String line;
	private ConditionalExpression expression;
	private int currentIndex;
	IComparisonOperator comparisonOperator;
	
	/**
	 * Constructor initializes given value and sets currentIndex to 0.
	 * @param line given input
	 */
	public Lexer(String line) {
		this.line = line;
		this.currentIndex = 0;
	}
	
	/**
	 * Finds next ConditionalExpression.
	 * @return next ConditionalExpression
	 */
	public ConditionalExpression nextConditionalExpression() {
		line = line.replaceFirst("query", "");
		String[] parts = line.split("and|And|aNd|anD|ANd|AnD|aND|AND");

		if (currentIndex == parts.length)
			return null;
		parts[currentIndex] = parts[currentIndex].replaceAll("\\s+", "");

		if (parts[currentIndex].contains(">="))
			comparisonOperator = ComparisonOperators.GREATER_OR_EQUALS;
		else if (parts[currentIndex].contains("<="))
			comparisonOperator = ComparisonOperators.LESS_OR_EQUALS;
		else if (parts[currentIndex].contains(">"))
			comparisonOperator = ComparisonOperators.GREATER;
		else if (parts[currentIndex].contains("<"))
			comparisonOperator = ComparisonOperators.LESS;
		else if (parts[currentIndex].contains("!="))
			comparisonOperator = ComparisonOperators.NOT_EQUALS;
		else if (parts[currentIndex].contains("="))
			comparisonOperator = ComparisonOperators.EQUALS;
		else if (parts[currentIndex].contains("LIKE"))
			comparisonOperator = ComparisonOperators.LIKE;
		
		String[] side = parts[currentIndex].split(">=|<=|>|<|!=|=|LIKE");
		
		if (side[1].startsWith("\"") && side[1].endsWith("\"")) {
			side[1] = side[1].replaceAll("\"", "");
		}
		
		switch (side[0]) {
		case "jmbag": 
			expression = new ConditionalExpression(FieldValueGetters.JMBAG, side[1], comparisonOperator);
			break;
		case "firstName": 
			expression = new ConditionalExpression(FieldValueGetters.FIRST_NAME, side[1], comparisonOperator);
			break;
		case "lastName": 
			expression = new ConditionalExpression(FieldValueGetters.LAST_NAME, side[1], comparisonOperator);
			break;
		}

		++currentIndex;
		return expression;
	}
}
