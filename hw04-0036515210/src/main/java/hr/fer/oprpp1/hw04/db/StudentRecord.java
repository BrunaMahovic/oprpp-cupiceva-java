package hr.fer.oprpp1.hw04.db;

/**
 * Class StudentRecord represents records for each student.
 * 
 * @author Bruna
 *
 */
public class StudentRecord {

	private String jmbag;
	private String lastName;
	private String firstName;
	private int finalGrade;
	
	/**
	 * Constructor initializes given values.
	 * @param jmbag student's jmbag
	 * @param lastName student's last name
	 * @param firstName student's first name
	 * @param finalGrade student's final grade
	 */
	public StudentRecord(String jmbag, String lastName, String firstName, int finalGrade) {
		this.jmbag = jmbag;
		this.lastName = lastName;
		this.firstName = firstName;
		this.finalGrade = finalGrade;
	}
	
	/**
	 * Gets student's jmbag.
	 * @return student's jmbag
	 */
	public String getJmbag() {
		return jmbag;
	}

	/**
	 * Gets student's last name.
	 * @return student's last name
	 */
	public String getLastName() {
		return lastName;
	}

	/**
	 * Gets student's first name.
	 * @return student's first name
	 */
	public String getFirstName() {
		return firstName;
	}
	
	/**
	 * Gets student's final grade.
	 * @return student's final grade
	 */
	public int getFinalGrade() {
		return finalGrade;
	}

	/**
	 * Checks if records are equal.
	 * @param student student record
	 * @return true if records are equal, otherwise false
	 */
	public boolean equals(StudentRecord student) {
		if (this.hashCode() == student.hashCode())
				return true;
		return false;
	}
	
	/**
	 * Gets hashCode for value.
	 */
	public int hashCode() {
		return this.jmbag.hashCode();
	}
}
