package hr.fer.oprpp1.hw04.db;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Class StudentDatabase represents an internal list of student records.
 * 
 * @author Bruna
 *
 */
public class StudentDatabase {
	
	List<StudentRecord> list = new ArrayList<>();
	Map<String, Integer> map = new HashMap<>();
	int counter = 0;
	
	/**
	 * Constructor gets a list of String objects and creates an internal list of student records.
	 * @param list list of String objects
	 */
	public StudentDatabase(ArrayList<String> list) {
		for (String student: list) {
			String[] parts = student.split("\\s+");
			
			if(parts.length > 4) {
				String s = parts[1] + " " + parts[2];
				parts[1] = s;
				parts[2] = parts[3];
				parts[3] = parts[4];
			}
			
			if (this.map.containsKey(parts[0]))
				throw new IllegalArgumentException("Ovaj jmbag već postoji.");
			
			if (Integer.valueOf(parts[3]) > 5 || Integer.valueOf(parts[3]) < 1)
				throw new IllegalArgumentException("Ocjena mora biti broj od 1 do 5.");
			
			this.list.add(new StudentRecord(parts[0], parts[1], parts[2], Integer.valueOf(parts[3])));
			this.map.put(parts[0], counter++);
		}
	}
	
	/**
	 * This method uses index to obtain requested record.
	 * @param jmbag given jmbag
	 * @return student record if it exists, otherwise null
	 */
	public StudentRecord forJMBAG(String jmbag) {
		if (this.map.containsKey(jmbag))
			return this.list.get(this.map.get(jmbag));
		return null;
	}
	
	/**
	 * Loops through all student records in its internal list and it calls accepts method on given filter-object with current record
	 * @param filter a reference to an object which is an instance of functional interface IFilter
	 * @return temporary list of accepted records
	 */
	public List<StudentRecord> filter(IFilter filter) {
		List<StudentRecord> temporary = new ArrayList<>();
		for (StudentRecord student: this.list) {
			if (filter.accepts(student))
				temporary.add(student);
		}
		return temporary;
	}
}
