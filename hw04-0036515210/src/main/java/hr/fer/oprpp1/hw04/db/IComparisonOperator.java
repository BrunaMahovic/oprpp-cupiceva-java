package hr.fer.oprpp1.hw04.db;

/**
 * Interface IComparisonOperator represents a strategy.
 * 
 * @author Bruna
 *
 */
public interface IComparisonOperator {

	/**
	 * Checks if conditions are satisfied.
	 * @param value1 first value
	 * @param value2 second value
	 * @return returns true if conditions are satisfied, otherwise false
	 */
	public boolean satisfied(String value1, String value2);
}
