package hr.fer.oprpp1.hw04.db;

/**
 * Interface IFieldValueGetter represents a strategy which is responsible for obtaining a requested field value from given StudentRecord.
 * 
 * @author Bruna
 *
 */
public interface IFieldValueGetter {

	/**
	 * Gets a requested field value from given StudentRecord.
	 * @param record student record
	 * @return a requested field value
	 */
	public String get(StudentRecord record);
}
