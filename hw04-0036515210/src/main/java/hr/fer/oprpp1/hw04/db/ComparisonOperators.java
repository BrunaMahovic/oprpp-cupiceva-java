package hr.fer.oprpp1.hw04.db;

/**
 * Class ComparisonOperators implements concrete strategies for each comparison operator.
 * 
 * @author Bruna
 *
 */
public class ComparisonOperators {

	public static final IComparisonOperator LESS = (value1, value2) -> value1.compareTo(value2) < 0;
	
	public static final IComparisonOperator LESS_OR_EQUALS = (value1, value2) -> value1.compareTo(value2) <= 0;
	
	public static final IComparisonOperator GREATER = (value1, value2) -> value1.compareTo(value2) > 0;

	public static final IComparisonOperator GREATER_OR_EQUALS = (value1, value2) -> value1.compareTo(value2) >= 0;

	public static final IComparisonOperator EQUALS = (value1, value2) -> value1.compareTo(value2) == 0;

	public static final IComparisonOperator NOT_EQUALS = (value1, value2) -> value1.compareTo(value2) != 0;

	public static final IComparisonOperator LIKE = (value1, value2) -> {
		char[] array = value2.toCharArray();
		int counter = 0;
		int placed = -1;
		
		for (int i = 0; i < array.length; i++) {
			if (array[i] == '*') {
				counter++;
				placed = i;
			}
		}

		if (counter > 1)
			throw new IllegalArgumentException("Previše * u stringu.");
		
		if (counter == 0)
			return value1.equals(value2);
		
		if (placed == 0) {
			int len = value2.length() - 1;
			return value2.substring(1).equals(value1.substring(value1.length() - len));
		}
		
		if (placed == value2.length() - 1) {
			int len = value2.length() - 1;
			return value2.substring(0, value2.length() - 1).equals(value1.substring(0, len));
		}
		
		else {
			String[] part = value2.split("\\*");
			if (part[0].equals(value1.substring(0, part[0].length())) 
					&& part[1].equals(value1.substring(value1.length() - part[1].length())))
				return true;
		}
		
		return false;
	};
}
