package hr.fer.oprpp1.hw04.db;

/**
 * Functional interface IFilter represents a filter.
 * 
 * @author Bruna
 *
 */
public interface IFilter {

	/**
	 * Method is used on given filter-object with current record;
	 * @param record student record
	 * @return true if record is accepted, otherwise false
	 */
	public boolean accepts(StudentRecord record);
}
