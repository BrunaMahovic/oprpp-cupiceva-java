package hr.fer.oprpp1.hw04.db;

/**
 * Class FieldValueGetters implements concrete strategies for each String field of StudentRecord class.
 * 
 * @author Bruna
 *
 */
public class FieldValueGetters {

	public static final IFieldValueGetter FIRST_NAME = (record) -> {
		if (record == null)
			throw new IllegalArgumentException("Student record ne smije biti null.");
		
		return record.getFirstName();
	};
	
	public static final IFieldValueGetter LAST_NAME = (record) -> {
		if (record == null)
			throw new IllegalArgumentException("Student record ne smije biti null.");
		
		return record.getLastName();
	};
	
	public static final IFieldValueGetter JMBAG = (record) -> {
		if (record == null)
			throw new IllegalArgumentException("Student record ne smije biti null.");
		
		return record.getJmbag();
	};
}
