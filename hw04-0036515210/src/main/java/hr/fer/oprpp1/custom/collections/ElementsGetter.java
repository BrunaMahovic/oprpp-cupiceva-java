package hr.fer.oprpp1.custom.collections;

/**
 * Interface ElementsGetter has methods for working with the elements.
 * 
 * @author Bruna
 *
 */
public interface ElementsGetter<T> {

	/**
	 * Checks if there is next element 
	 * @return true if there is next element, otherwise false
	 */
	public boolean hasNextElement();
	
	/**
	 * Gets next element from the collection
	 * @return next element from the collection
	 */
	public T getNextElement();
	
	/**
	 * Calls the default processor over all remaining elements of the collection 
	 * @param p default processor
	 */
	default void processRemaining(Processor<T> p) {
		while (hasNextElement())
			p.process(getNextElement());
	}
}
