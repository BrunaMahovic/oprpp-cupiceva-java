package hr.fer.oprpp1.custom.collections;

/**
 * Class Collection represents some general collection of objects.
 * 
 * @author Bruna
 *
 */
public interface Collection<T> {
	
	/**
	 * Checks if the collection contains objects.
	 * @return true if size() is 0, otherwise false
	 */
	default public boolean isEmpty() {
		return size() == 0;
	}
	
	/**
	 * Shows the number of elements in the collection
	 * @return the number of elements in this collection
	 */
	public abstract int size();
	
	/** 
	 * Ensures that the collection contains the specified element
	 * @param value an element added to the collection
	 */
	public abstract void add(T value);
	
	/**
	 * Checks if this collection contains the specified element
	 * @param value element which is to be tested to see if it is in a collection
	 * @return true if this collection contains the specified element, otherwise false
	 */
	public abstract boolean contains(Object value);
	
	/**
	 * Removes a single instance of the specified element from this collection
	 * @param value element which is to be removed from this collection
	 * @return true if an element was removed, otherwise false
	 */
	public abstract boolean remove(Object value);
	
	/**
	 * Creates an array containing all of the elements in this collection
	 * @return an array containing all of the elements in this collection
	 */
	public abstract Object[] toArray();
	
	/**
	 * Performs the given action for each element of the collection
	 * @param processor instance whose action to be performed for each element
	 */
	default public void forEach(Processor<? super T> processor) {
		ElementsGetter<T> getter = this.createElementsGetter();
		
		while (getter.hasNextElement()) {
			processor.process(getter.getNextElement());
		}
	}
	
	/**
	 * Adds all of the elements in the specified collection to this collection
	 * @param other collection whose elements are to be added to this collection
	 */
	default public void addAll(Collection<? extends T> other) {
		class LocalProcessor implements Processor<T>{
			public void process(T value) {
				add(value);
			}
		}
		LocalProcessor lp = new LocalProcessor();
		other.forEach(lp);
	}
	
	/**
	 * Removes all of the elements from the collection
	 */
	public abstract void clear();
	
	/**
	 * Creates new instance of ElementsGetter
	 */
	public abstract ElementsGetter<T> createElementsGetter();
	
	/**
	 * Retrieves all the elements from the submitted collection and add ones that the submitted tester accepts to the current collection.
	 * @param col given colllection
	 * @param tester given tester which decides which elements will be tested
	 */
	default public void addAllSatisfying(Collection<? extends T> col, Tester<? super T> tester) {
		ElementsGetter<? extends T> getter = col.createElementsGetter();
		
		while (getter.hasNextElement()) {
			T element = getter.getNextElement();
			
			if (tester.test(element)) {
				this.add(element);
			}
		}
	}
}

