package hr.fer.oprpp1.custom.collections;

/**
 * Class ObjectStack implements a stack collection of objects with ArrayIndexedCollection methods.
 * 
 * @author Bruna
 *
 */
public class ObjectStack<T> {

	private ArrayIndexedCollection<T> arr = new ArrayIndexedCollection<T>();
	
	/**
	 * Checks if the collection contains objects.
	 * @return true if size() is 0, otherwise false
	 */
	public boolean isEmpty() {
		return arr.isEmpty();
	}
	
	/**
	 * Shows the number of elements in the collection
	 * @return the number of elements in this collection
	 */
	public int size() {
		return arr.size();
	}
	
	/**
	 * Pushes given value on the stack.
	 * @param value value pushed on the stack
	 * @throws NullPointerException if value is null
	 */
	public void push(T value) {
		if(value == null) 
			throw new NullPointerException();
		
		arr.add(value);	
	}
	
	/**
	 * Removes last value pushed on stack from stack.
	 * @return last value pushed on stack
	 * @throws EmptyStackException if size of stack is null
	 */
	public T pop() {
		if (arr.size() == 0)
			throw new EmptyStackException("");

		T obj = arr.get(arr.size() - 1);
		arr.remove(arr.size() - 1);
		return obj;
	}
	
	/**
	 * Finds last element placed on stack.
	 * @return last element placed on stack
	 * @throws EmptyStackException if size of stack is null
	 */
	public T peek() {
		if (arr.size() == 0)
			throw new EmptyStackException("");

		return arr.get(arr.size() - 1);
	}
	
	/**
	 * Removes all of the elements from the stack.
	 */
	public void clear() {
		arr.clear();
	}
}
