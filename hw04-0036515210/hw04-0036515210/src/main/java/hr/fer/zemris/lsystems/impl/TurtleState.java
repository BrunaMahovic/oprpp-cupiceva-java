package hr.fer.zemris.lsystems.impl;

import java.awt.Color;

/**
 * Class TurtleState remembers the current position of the turtle.
 * @author Bruna
 *
 */
public class TurtleState {

	private Vector2D currentPosition;
	private Vector2D direction;
	private Color color;
	private double length;
	
	/**
	 * Constructor initializes given values.
	 * @param currentPosition
	 * @param direction
	 * @param color
	 * @param length
	 */
	public TurtleState(Vector2D currentPosition, Vector2D direction, Color color, double length) {
		this.currentPosition = currentPosition;
		this.direction = direction;
		this.color = color;
		this.length = length;
	}
	
	/**
	 * Gets the current position.
	 * @return current position
	 */
	public Vector2D getCurrentPosition() {
		return this.currentPosition;
	}
	
	/**
	 * Sets the current position.
	 * @param currentPosition
	 */
	public void setCurrentPosition(Vector2D currentPosition) {
		this.currentPosition = currentPosition;
	}
	
	/**
	 * Gets the direction.	
	 * @return direction
	 */
	public Vector2D getDirection() {
		return this.direction;
	}
	
	/**
	 * Sets the direction.
	 * @param direction
	 */
	public void setDirection(Vector2D direction) {
		this.direction = direction;
	}
	
	/**
	 * Gets the color.
	 * @return color
	 */
	public Color getColor() {
		return this.color;
	}
	
	/**
	 * Sets the color.
	 * @param color
	 */
	public void setColor(Color color) {
		this.color = color;
	}
	
	/**
	 * Gets the length.
	 * @return length
	 */
	public double getLength() {
		return this.length;
	}
	
	/**
	 * Sets the length.
	 * @param length
	 */
	public void setLength(double length) {
		this.length = length;
	}
	
	/**
	 * Creates a new object with a copy of the current state.
	 * @return a copy of the current state
	 */
	public TurtleState copy() {
		TurtleState replica = new TurtleState(this.currentPosition, this.direction, this.color, this.length);
		return replica;
	}
}
