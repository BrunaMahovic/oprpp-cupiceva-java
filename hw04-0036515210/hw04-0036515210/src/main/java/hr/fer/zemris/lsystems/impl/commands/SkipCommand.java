package hr.fer.zemris.lsystems.impl.commands;

import hr.fer.zemris.lsystems.Painter;
import hr.fer.zemris.lsystems.impl.*;

/**
 * Class SkipCommand represents an action for the command.
 * 
 * @author Bruna
 *
 */
public class SkipCommand implements Command {

	private double step;
	
	/**
	 * Constructor initializes given step.
	 * @param step
	 */
	public SkipCommand(double step) {
		this.step = step;
	}
	
	/**
	 * Calculates where the turtle must go and remembers the new position of the turtle in the current state.
	 */
	@Override
	public void execute(Context ctx, Painter painter) {
		TurtleState state = ctx.getCurrentState();
		double len = Math.sqrt(Math.pow(state.getDirection().getX(), 2) + Math.pow(state.getDirection().getY(), 2));
		Vector2D vector = state.getDirection().scaled(this.step * state.getLength() / len);
		state.setCurrentPosition(new Vector2D(state.getCurrentPosition().getX() + vector.getX(), state.getCurrentPosition().getY() + vector.getY()));
	}
}
