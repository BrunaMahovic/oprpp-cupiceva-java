package hr.fer.zemris.lsystems.impl.commands;

import java.awt.Color;

import hr.fer.zemris.lsystems.Painter;
import hr.fer.zemris.lsystems.impl.*;

/**
 * Class ColorCommand represents an action for the command.
 * 
 * @author Bruna
 *
 */
public class ColorCommand implements Command {

	private Color color;
	
	/**
	 * Constructor initializes given color.
	 * @param color
	 */
	public ColorCommand(Color color) {
		this.color = color;
	}
	
	/**
	 * Writes the committed color in the current state of the turtle.
	 */
	@Override
	public void execute(Context ctx, Painter painter) {
		TurtleState state = ctx.getCurrentState();
		state.setColor(this.color);
	}
}
