package hr.fer.zemris.lsystems.impl.commands;

import hr.fer.zemris.lsystems.Painter;
import hr.fer.zemris.lsystems.impl.*;

/**
 * Class ScaleCommand represents an action for the command.
 * 
 * @author Bruna
 *
 */
public class ScaleCommand implements Command {

	private double factor;
	
	/**
	 * Constructor initializes given factor.
	 * @param factor
	 */
	public ScaleCommand(double factor) {
		this.factor = factor;
	}
	
	/**
	 * Updates the effective offset length in the current state by scaling with a given factor.
	 */
	@Override
	public void execute(Context ctx, Painter painter) {
		TurtleState state = ctx.getCurrentState();
		double len = state.getLength();
		state.setLength(len * this.factor);
	}
}
