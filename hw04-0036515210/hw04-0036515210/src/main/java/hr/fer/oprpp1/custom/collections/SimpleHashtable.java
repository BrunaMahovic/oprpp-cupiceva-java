package hr.fer.oprpp1.custom.collections;

import java.util.Arrays;
import java.util.ConcurrentModificationException;
import java.util.Iterator;
import java.util.NoSuchElementException;

public class SimpleHashtable<K,V> implements Iterable<SimpleHashtable.TableEntry<K,V>> {
	
	public static class TableEntry<K,V> {
		private K key;
		private V value;
		private TableEntry<K,V> next;
		
		public TableEntry(K key, V value, TableEntry<K,V> next) {
			if (key == null)
				throw new NullPointerException();
			this.key = key;
			this.value = value;
			this.next = next;
		}
		
		public K getKey() {
			return this.key;
		}
		
		public V getValue() {
			return this.value;
		}
		
		public void setValue(V value) {
			this.value = value;
		}
	}

	private TableEntry<K,V>[] table;
	private int size;
	private long modificationCount = 0;
	
	public SimpleHashtable() {
		this(16);
	}
	
	public SimpleHashtable(int capacity) {
		if (capacity < 1)
			throw new IllegalArgumentException();
		
		boolean isPower = false;
		while (!isPower) {
			isPower = true;
			while (capacity != 1) {
				if (capacity % 2 != 0) {
					isPower = false;
					break;
				}
				capacity = capacity / 2;
			}
			if (isPower)
				break;
			capacity++;
		}
		
		this.table = (TableEntry<K,V>[]) new TableEntry[capacity];
		this.size = 0;
	}
	
	public V put(K key, V value) {
		if (key == null)
			throw new NullPointerException();
		
		if (this.size() / table.length >= 0.75) {
			TableEntry<K,V>[] table2 = this.toArray();	
			int l = table.length * 2;
			this.table = (TableEntry<K,V>[]) new TableEntry[l];
			int k;
			
			for (int i = 0; i < table2.length; i++) {
				if (table2[i] != null) {
					k = Math.abs(table2[i].getKey().hashCode() % table.length);
					TableEntry<K,V> pom = table[k];
					
					if (table[k] == null)
						table[k] = new TableEntry<K,V>(table2[i].getKey(), table2[i].getValue(), null);
					else {
						while (pom.next != null)
							pom = pom.next;
						pom.next = new TableEntry<K,V>(table2[i].getKey(), table2[i].getValue(), null);
					}
				}
			}
		}
		
		int index = Math.abs(key.hashCode() % table.length);
		TableEntry<K,V> element = table[index];
		
		if (element == null) {			
			table[index] = new TableEntry<K,V>(key, value, null);
			size++;
			modificationCount++;
			return null;
		}
		
		while (element.next != null) {
			if (element.getKey().equals(key)) {
				V result = element.getValue();
				element.setValue(value);
				modificationCount++;
				return result;
			}
			
			element = element.next;
		}
		
		element.next = new TableEntry<K,V>(key, value, null);
		size++;
		modificationCount++;
		return null;
	}
	
	public V get(Object key) {
		if (key == null)
			return null;
		
		int index = Math.abs(key.hashCode() % table.length);
		TableEntry<K,V> element = table[index];
		
		while (element != null) {
			if (element.getKey().equals(key)) {
				return element.getValue();
			}
			element = element.next;
		}
		return null;
	}
	
	public int size() {
		return this.size;
	}
	
	public boolean containsKey(Object key) {
		if (key == null)
			return false;
		
		int index = Math.abs(key.hashCode() % table.length);
		TableEntry<K,V> element = table[index];
		
		while (element != null) {
			if (element.getKey().equals(key)) {
				return true;
			}
			element = element.next;
		}
		return false;
	}
	
	public boolean containsValue(Object value) {
		if (value == null)
			return false;
			
		for (int i = 0; i < table.length; i++)  {
			TableEntry<K,V> element = table[i];
			while (element != null) {
				if (element.getValue().equals(value)) {
					return true;
				}
				element = element.next;
			}
		}
		return false;
	}
	
	public V remove(Object key) {
		if (key == null)
			return null;
		
		int index = Math.abs(key.hashCode() % table.length);
		TableEntry<K,V> element = table[index];
		
		V result = null;
		
		if (table[index].getKey().equals(key)) {
			result = table[index].getValue();
			table[index] = table[index].next;
			size--;
			modificationCount++;
			return result;
		}
		
		while (element.next != null) {
			if (element.next.getKey().equals(key)) {
				result = element.next.getValue();
				element.next = element.next.next;
				size--;
				modificationCount++;
				break;
			}
			element = element.next;
		}
		
		return result;
	}
	
	public boolean isEmpty() {
		if (this.size() == 0)
			return true;
		return false;
	}
	
	public String toString() {
		StringBuilder sb = new StringBuilder();
		sb.append("[");
		
		for (int i = 0; i < table.length; i++) {
			TableEntry<K,V> element = table[i];
			
			while (element != null) {
				sb.append(element.getKey() + "=" + element.getValue() + ", ");
				element = element.next;
			}
		}
		
		sb.deleteCharAt(sb.length() - 1);
		sb.deleteCharAt(sb.length() - 1);
		sb.append("]");
		
		return sb.toString();
	}
	
	public TableEntry<K,V>[] toArray() {
		TableEntry<K,V>[] table2 = (TableEntry<K,V>[]) new TableEntry[this.size()];
		
		int index = 0;
		for (int i = 0; i < table.length; i++) {
			TableEntry<K,V> element = table[i];
			
			while (element != null) {
				table2[index++] = new TableEntry<K,V>(element.getKey(), element.getValue(), null);
				element = element.next;
			}	
		}
		return table2;
	}
	
	public void clear() {
		for (int i = 0; i < table.length; i++) {
			table[i] = null;
		}
		size = 0;
		modificationCount++;
	}
	
	private class IteratorImpl implements Iterator<SimpleHashtable.TableEntry<K,V>> {
		
		int counter;
		int slot;
		private long savedModificationCount;
		int size2;
		TableEntry<K,V> result = null;
		boolean r = false;
		
		public IteratorImpl() {
			this.savedModificationCount = modificationCount;
			this.size2 = size();
			this.counter = 0;
			this.slot = 0;
		}
		
		public boolean hasNext() { 
			if (savedModificationCount != modificationCount)
				throw new ConcurrentModificationException();
			
			if (size2 > counter)
				return true;
			
			return false;
		}
		
		public SimpleHashtable.TableEntry next() {
			if (savedModificationCount != modificationCount)
				throw new ConcurrentModificationException();
			
			if (counter >= size2)
				throw new NoSuchElementException();
			
			r = false;
			
			if (counter == 0 || result.next == null) {
				if(counter != 0)
					slot++;

				while (slot < table.length && table[slot] == null)
					slot++;
									
				if (slot < table.length)
					result = table[slot];
				
				else 
					throw new NoSuchElementException();
			}
			
			else 
				result = result.next;
			
			counter++;
			return result;
		}
		
		public void remove() {
			if (r == true)
				throw new IllegalStateException();
			
			if (savedModificationCount != modificationCount)
				throw new ConcurrentModificationException();
			
			SimpleHashtable.this.remove(result.getKey());
			r = true;
			savedModificationCount++;
		}
	}
	
	public Iterator<SimpleHashtable.TableEntry<K,V>> iterator() { 
		return new IteratorImpl();
	}
}
