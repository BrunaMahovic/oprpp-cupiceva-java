package hr.fer.oprpp1.hw04.db;

import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

/**
 * Class StudentDB is used to run the program.
 * @author Bruna
 *
 */
public class StudentDB {

	public static void main(String[] args) {
		
		Scanner sc = new Scanner(System.in);
		
		while (true) {
			String input = sc.nextLine();
			
			ArrayList<String> lines = null;
			try {
				lines = (ArrayList) Files.readAllLines(Paths.get("./src/database.txt"), StandardCharsets.UTF_8);
			} catch (IOException e) {
				e.printStackTrace();
			}
			
			if (input.equals("exit")) {
				System.out.println("Goodbye!");
				return;
			}
			
			StudentDatabase sdb = new StudentDatabase(lines);
			QueryParser parser = new QueryParser(input);
			List<ConditionalExpression> listCE = parser.getQuery();
			QueryFilter filter = new QueryFilter(listCE);
			List<StudentRecord> listOfStudents = sdb.filter(filter);
			
			StringBuilder sb = new StringBuilder();
			sb.append("+============+");
			
			int lengthFirst = 0;
			int lengthLast = 0;
			for (StudentRecord record: listOfStudents) {
				if (record.getLastName().length() > lengthLast)
					lengthLast = record.getLastName().length();
				
				if (record.getFirstName().length() > lengthFirst)
					lengthFirst = record.getFirstName().length();
			}
			
			for(int i = 0; i < lengthLast + lengthFirst + 7; i++) {
				sb.append("=");
				if (i == lengthLast + 1 || i == lengthLast + lengthFirst + 3 || i == lengthLast + lengthFirst + 6)
					sb.append("+");
			}
			sb.append("\n");
			 
			StringBuilder sb2 = new StringBuilder();
			
			for (StudentRecord record: listOfStudents) {
				sb2.append("| " + record.getJmbag() + " | " + record.getLastName());
				
				for (int i = 0; i < lengthLast - record.getLastName().length(); i++)
					sb2.append(" ");
				sb2.append(" | " + record.getFirstName());
				
				for (int i = 0; i < lengthFirst - record.getFirstName().length(); i++)
					sb2.append(" ");
				sb2.append(" | " + record.getFinalGrade() + " |\n");
			}
			
			String result = sb.toString() + sb2.toString() + sb.deleteCharAt(sb.length() - 1).toString();

			if (listOfStudents.size() == 1 && parser.isDirectQuery())
				System.out.println("Using index for record retrieval.");
				
			if (listOfStudents.size() > 0)
				System.out.println(result);
			
			System.out.println("Records selected: " + listOfStudents.size());
		}
	}
}
