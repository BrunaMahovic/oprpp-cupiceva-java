package hr.fer.oprpp1.custom.collections;

import java.util.*;

/**
 * Class LinkedListIndexedCollection implements linked list-backed collection of objects.
 * 
 * @author Bruna
 *
 */
public class LinkedListIndexedCollection<T> implements List<T> {

	/**
	 * Class ListNode has pointers to previous and next list node and additional reference for value storage.
	 * 
	 * @author Bruna
	 *
	 */
	private static class ListNode<T> {
		ListNode<T> previous;
		ListNode<T> next;
		T value;
		
		ListNode(T d) 
        { 
            value = d; 
            next = null; 
        } 
	}
	private int size;
	ListNode<T> first; 
	ListNode<T> last; 
	private long modificationCount = 0;
    
	/**
	 * Class LinkedListIndexedGetter implement methods for working with the elements.
	 * 
	 * @author Bruna
	 *
	 */
	private static class LinkedListIndexedGetter<T> implements ElementsGetter<T> {
		LinkedListIndexedCollection<T> collection;
		ListNode<T> current;
		private long savedModificationCount;
		
		/**
		 * The LinkedListIndexedGetter constructor which copies elements of one collection to a new one.
		 * @param collection collection which elements are copied in new collection
		 */
		public LinkedListIndexedGetter(LinkedListIndexedCollection<T> collection) {
			this.collection = collection;
			this.savedModificationCount = collection.modificationCount;
			this.current = collection.first;
		}
		
		/**
		 * Checks if there is next element 
		 * @return true if there is next element, otherwise false
		 */
		@Override
		public boolean hasNextElement() {
			if (savedModificationCount != collection.modificationCount)
				throw new ConcurrentModificationException();
			
			if (current != null)
				return true;
			
			return false;
		}
		
		/**
		 * Gets next element from the collection
		 * @return next element from the collection
		 */
		@Override
		public T getNextElement() {
			if (!hasNextElement())
				throw new NoSuchElementException();
			
			T result = current.value;
			current = current.next;
			return result;
		}
	}
	
	/**
     * The default LinkedListIndexedCollection constructor creates an empty collection.
     */
    public LinkedListIndexedCollection() {
    	first=last=null;
    	size = 0;
    }
    
    /**
     * The LinkedListIndexedCollection constructor which copies elements of one collection to a new one.
     * @param collection
     */
    public LinkedListIndexedCollection(Collection<T> collection) {
    	addAll(collection);
    	size = collection.size();
    }
    
    /**
	 * Creates new instance of LinkedListIndexedGetter
	 */
    public ElementsGetter<T> createElementsGetter() {
		return new LinkedListIndexedGetter<T>(this);
	}
    
    /**
     * Ensures that the collection contains the specified element
	 * @param value an element added to the collection
	 * @throws NullPointerException if value is null
     */
    @Override
    public void add(T value) {
		if(value == null) 
			throw new NullPointerException();
        
        if (first == null) {
            first = new ListNode<T>(value);
        	last = first;
        }
        
        else {
	        ListNode<T> newNode = new ListNode<T>(value);
	        
	        newNode.previous = last;
	        last.next = newNode;
	        last = newNode;
        }
        size++;
        modificationCount++;
    }
    
    /**
	 * Finds the value of the indexed component in the specified list object.
	 * @param index the index
	 * @return the value of the indexed component
	 * @throws IndexOutOfBoundsException if indexes are not 0 to size-1
	 */
    public T get(int index) {
    	if (index < 0 || index > size - 1) 
			throw new IndexOutOfBoundsException();
		    	
    	if (index == 0)
    		return first.value;
    	
    	ListNode<T> obj;
    	if (index < size / 2) {
    		int num = 0;
    		obj = first;
    		while (num != index) {
	    		obj = obj.next;
	    		++num;
	    	}
    	}
    	
    	else {
    		int num = size - 1;
    		obj = last;
    		while (num != index) {
	    		obj = obj.previous;
	    		num--;
	    	}
    	}
    	
    	return obj.value;
    }
    
    /**
     * Removes all of the elements from the collection.
     */
    @Override
    public void clear() {
    	this.first = this.last = null;
    	size = 0;
    	modificationCount++;
    }
    
    /**
	 * Puts the given value at the given position in list.
	 * @param value the value which is to be put in list
	 * @param position position where the value will be put
	 * @throws IndexOutOfBoundsException if indexes are not 0 to size
	 * @throws NullPointerException if value is null
	 */
    public void insert(T value, int position) {
    	if (position < 0 || position > size) 
			throw new IndexOutOfBoundsException();
		
		if (value == null) 
			throw new NullPointerException();
		
		if (position == size) {
			add(value);
			return;
		}
			
		ListNode<T> newNode = new ListNode<T>(value);
		ListNode<T> obj = first;
        int num = 0;
        
        if (position == 0)
        	first = newNode;
        
        while(num != position) {
        	obj = obj.next;
        	num++;
        }
        
        newNode.next = obj;
        obj.previous = newNode;
        size++;
        modificationCount++;
    }
    
    /**
	 * Searches for the index of the first occurrence of the given value.
	 * @param value the tested value
	 * @return returns the index of the first occurrence of the given value or -1 if the value is not found
	 */
    public int indexOf(Object value) {
    	if(value != null) {
    		ListNode<T> obj = first;
            
            for (int i = 0; i != size; i++) {
            	if(value.equals(obj.value))
                	return i;
            	
            	obj = obj.next;
            }
    	}
    	return -1;
    }
    
    /**
	 * Removes element at specified index from collection. 
	 * @param index position where the value is
	 * @throws IndexOutOfBoundsException if indexes are not 0 to size-1
	 */
    public void remove(int index) {
    	if (index < 0 || index > size - 1) 
			throw new IndexOutOfBoundsException();
    	
    	if (index == 0)
			first = first.next;
    	
    	else {
    		ListNode<T> obj = first;
    		
    		for (int i = 0; i != index; i++)
        		obj = obj.next;
        	obj = obj.previous;
        	
        	if(obj.next.next != null)
        		obj.next = obj.next.next;
        
        	else {
        		obj.next = null;
        		last = last.previous;
        	}
    	}
    	size--;
    	modificationCount++;
    }
    
    /**
     * Checks if the collection contains objects.
	 * @return true if size() is 0, otherwise false
     */
    @Override
	public boolean isEmpty() {
    	return size() == 0;
	}
	
	/**
	 * Shows the number of elements in the collection
	 * @return the number of elements in this collection
	 */
	@Override
	public int size() {
		return this.size;
	}
	
	/**
	 * Checks if this collection contains the specified element
	 * @param value element which is to be tested to see if it is in a collection
	 * @return true if this collection contains the specified element, otherwise false
	 * @throws NullPointerException if value is null
	 */
	@Override
	public boolean contains(Object value) {
		if(value == null) 
			throw new NullPointerException();
		
		if (this.indexOf(value) != -1)
			return true;
		
		return false;
	}
	
	/**
	 * Removes a single instance of the specified element from this collection
	 * @param value element which is to be removed from this collection
	 * @return true if an element was removed, otherwise false
	 * @throws NullPointerException if value is null
	 */
	@Override
	public boolean remove(Object value) {
		if(value == null) 
			throw new NullPointerException();
		
		int result = this.indexOf(value);
		
		if(result != -1) {
			remove(result);
			return true;
		}
		return false;
	}
	
	/**
	 * Creates an array containing all of the elements in this collection
	 * @return an array containing all of the elements in this collection
	 */
	@Override
	public Object[] toArray() {
		Object[] objects = new Object[size];
		
		ListNode<T> obj = first;
		int i = 0;
		
        while (obj != null) {    	
        	objects[i++] = obj.value;
        	obj = obj.next;
        }
		return objects;
	}
}
