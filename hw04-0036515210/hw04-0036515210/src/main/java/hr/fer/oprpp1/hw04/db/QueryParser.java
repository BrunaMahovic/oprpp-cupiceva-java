package hr.fer.oprpp1.hw04.db;

import java.util.ArrayList;
import java.util.List;

/**
 * Class Parser represents a parser of query statement.
 * 
 * @author Bruna
 *
 */
public class QueryParser {

	private String line;
	private Lexer lexer;
	List<ConditionalExpression> list = new ArrayList<>();
	
	/**
	 * Constructor initializes given query string and Lexer.
	 * @param line query string
	 */
	public QueryParser(String line) {
		this.line = line;
		lexer = new Lexer(line);
		parse();
	}
	
	/**
	 * Checks if query was of the form jmbag="xxx".
	 * @return true if form was correct, otherwise false
	 */
	public boolean isDirectQuery() {
		if (list.size() != 1)
			return false;
		
		if (list.get(0).getFieldGetter() == FieldValueGetters.JMBAG && list.get(0).getComparisonOperator() == ComparisonOperators.EQUALS)
			return true;
		
		return false;
	}
	
	/**
	 * Gets the string of the form "xxx" which was given in equality comparison in direct query.
	 * @return the string of the form "xxx" which was given in equality comparison in direct query.
	 * @throws IllegalStateException if the query was not a direct one
	 */
	public String getQueriedJMBAG() {
		if (!isDirectQuery())
			throw new IllegalStateException();
		
		return list.get(0).getStringLiteral();
	}
	
	/**
	 * Gets a list of conditional expressions from query.
	 * @return a list of conditional expressions
	 */
	public List<ConditionalExpression> getQuery() {
		return this.list;
		
	}
	
	/**
	 * The parser uses lexer for creating expressions.
	 */
	public void parse() {
		ConditionalExpression expression;
		
		while (true) {
			expression = lexer.nextConditionalExpression();

			if (expression == null)
				break;
			
			list.add(expression);
		}
	}
}
