package hr.fer.oprpp1.hw04.db;

/**
 * Class ConditionalExpression models the complete conditional expression.
 * 
 * @author Bruna
 *
 */
public class ConditionalExpression {

	private IFieldValueGetter fieldGetter;
	private String stringLiteral;
	private IComparisonOperator comparisonOperator;
	
	/**
	 * Constructor initializes given values.
	 * @param fieldGetter a reference to IFieldValueGetter strategy
	 * @param stringLiteral a reference to string literal 
	 * @param comparisonOperator a reference to IComparisonOperator strategy
	 */
	public ConditionalExpression(IFieldValueGetter fieldGetter, String stringLiteral, IComparisonOperator comparisonOperator) {
		this.fieldGetter = fieldGetter;
		this.stringLiteral = stringLiteral;
		this.comparisonOperator = comparisonOperator;
	}
	
	/**
	 * Gets the IFieldValueGetter strategy.
	 * @return IFieldValueGetter strategy
	 */
	public IFieldValueGetter getFieldGetter() {
		return this.fieldGetter;
	}
	
	/**
	 * Gets the string literal.
	 * @return string literal
	 */
	public String getStringLiteral() {
		return this.stringLiteral;
	}
	
	/**
	 * Gets the IComparisonOperator strategy.
	 * @return IComparisonOperator strategy
	 */
	public IComparisonOperator getComparisonOperator() {
		return this.comparisonOperator;
	}
}
