package hr.fer.oprpp1.hw04.db;

import java.util.List;

/**
 * Class QueryFilter represents a filter.
 * 
 * @author Bruna
 *
 */
public class QueryFilter implements IFilter {

	private List<ConditionalExpression> list;
	
	/**
	 * Constructor initializes given list.
	 * @param list given list
	 */
	public QueryFilter(List<ConditionalExpression> list) {
		this.list = list;
	}
	
	/**
	 * Method is used on given filter-object with current record;
	 * @param record student record
	 * @return true if record is accepted, otherwise false
	 */
	@Override
	public boolean accepts(StudentRecord record) {
		for (ConditionalExpression expr: list) {
			if (!expr.getComparisonOperator().satisfied(expr.getFieldGetter().get(record), expr.getStringLiteral()))
				return false;
		}
		return true;
	}
}
