package hr.fer.zemris.lsystems.impl;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;

import org.junit.jupiter.api.Test;

import hr.fer.oprpp1.custom.collections.Dictionary;

public class GenerateMethodTest {
	
	@Test
	public void testKochCurveZero() {
		Dictionary<Character, String> productions = new Dictionary<Character, String>();
		productions.put('F', "F+F--F+F");
		String axiom = "F";
		String result = generate(0, axiom, productions);
		String expected = "F";
		assertTrue(result.equals(expected));
	}
	
	@Test
	public void testKochCurveOne() {
		Dictionary<Character, String> productions = new Dictionary<Character, String>();
		productions.put('F', "F+F--F+F");
		String axiom = "F";
		String result = generate(1, axiom, productions);
		String expected = "F+F--F+F";
		assertTrue(result.equals(expected));
	}
	
	@Test
	public void testKochCurveTwo() {
		Dictionary<Character, String> productions = new Dictionary<Character, String>();
		productions.put('F', "F+F--F+F");
		String axiom = "F";
		String result = generate(2, axiom, productions);
		String expected = "F+F--F+F+F+F--F+F--F+F--F+F+F+F--F+F";
		assertTrue(result.equals(expected));
	}
	
	@Test
	public void testKochIslandZero() {
		Dictionary<Character, String> productions = new Dictionary<Character, String>();
		productions.put('F', "F-F+F+FF-F-F+F");
		String axiom = "F-F-F-F";
		String result = generate(0, axiom, productions);
		String expected = "F-F-F-F";
		assertTrue(result.equals(expected));
	}
	
	@Test
	public void testKochIslandOne() {
		Dictionary<Character, String> productions = new Dictionary<Character, String>();
		productions.put('F', "F-F+F+FF-F-F+F");
		String axiom = "F-F-F-F";
		String result = generate(1, axiom, productions);
		String expected = "F-F+F+FF-F-F+F-F-F+F+FF-F-F+F-F-F+F+FF-F-F+F-F-F+F+FF-F-F+F";
		assertTrue(result.equals(expected));
	}
	
	@Test
	public void testPlant1Zero() {
		Dictionary<Character, String> productions = new Dictionary<Character, String>();
		productions.put('F', "F[+F]F[-F]F");
		String axiom = "GF";
		String result = generate(0, axiom, productions);
		String expected = "GF";
		assertTrue(result.equals(expected));
	}
	
	@Test
	public void testPlant1One() {
		Dictionary<Character, String> productions = new Dictionary<Character, String>();
		productions.put('F', "F[+F]F[-F]F");
		String axiom = "GF";
		String result = generate(1, axiom, productions);
		String expected = "GF[+F]F[-F]F";
		assertTrue(result.equals(expected));
	}
	
	@Test
	public void testPlant1Two() {
		Dictionary<Character, String> productions = new Dictionary<Character, String>();
		productions.put('F', "F[+F]F[-F]F");
		String axiom = "GF";
		String result = generate(2, axiom, productions);
		String expected = "GF[+F]F[-F]F[+F[+F]F[-F]F]F[+F]F[-F]F[-F[+F]F[-F]F]F[+F]F[-F]F";
		assertTrue(result.equals(expected));
	}
	
	@Test
	public void testSierpinskiZero() {
		Dictionary<Character, String> productions = new Dictionary<Character, String>();
		productions.put('R', "L-R-L");
		productions.put('L', "R+L+R");
		String axiom = "R";
		String result = generate(0, axiom, productions);
		String expected = "R";
		assertTrue(result.equals(expected));
	}
	
	@Test
	public void testSierpinskiOne() {
		Dictionary<Character, String> productions = new Dictionary<Character, String>();
		productions.put('R', "L-R-L");
		productions.put('L', "R+L+R");
		String axiom = "R";
		String result = generate(1, axiom, productions);
		String expected = "L-R-L";
		assertTrue(result.equals(expected));
	}
	
	@Test
	public void testSierpinskiTwo() {
		Dictionary<Character, String> productions = new Dictionary<Character, String>();
		productions.put('R', "L-R-L");
		productions.put('L', "R+L+R");
		String axiom = "R";
		String result = generate(2, axiom, productions);
		String expected = "R+L+R-L-R-L-R+L+R";
		assertTrue(result.equals(expected));
	}
	
	
	public String generate(int level, String axiom, Dictionary<Character, String> productions) {
		if (level == 0)
			return axiom;
		
		StringBuilder sb = new StringBuilder();
		sb.append(axiom);
		
		for (int i = 0; i < level; i++) {
			char[] sign = sb.toString().toCharArray();
			sb.setLength(0);
			
			for (int j = 0; j < sign.length; j++) {
				String key = productions.get(sign[j]);
				if (key != null)
					sb.append(key);
				else
					sb.append(sign[j]);
			}
			
		}
		return sb.toString();
	}
}
