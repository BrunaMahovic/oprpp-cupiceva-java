package hr.fer.oprpp1.hw05.crypto;

/**
 * Class Util has two public static methods: hextobyte(keyText) and bytetohex(bytearray).
 * 
 * @author Bruna
 *
 */
public class Util {

	/**
	 * Takes hex-encoded String and return appropriate byte[]
	 * @param keyText hex-encoded String
	 * @return byte[]
	 * @throws IllegalArgumentException if string is not valid
	 */
	public static byte[] hexToByte(String keyText) throws IllegalArgumentException {
		int len = keyText.length();
		if (len % 2 != 0)
			throw new IllegalArgumentException();
		
		byte[] result = new byte[len / 2];
		
	    for (int i = 0; i < len; i += 2) {
	    	char[] array = keyText.substring(i, i + 2).toCharArray();
	    	int d;
	    	int digits = 0;
	    	for (int j = 0; j < 2; j++) {
	    		if (array[j] >= '0' && array[j] <= '9')
	    			d = array[j] - 48;
	    		else if (array[j] >= 'A' && array[j] <= 'F')
	    			d = array[j] - 65 + 10;
	    		else if (array[j] >= 'a' && array[j] <= 'f')
	    			d = array[j] - 97 + 10;
	    		else 
	    			throw new IllegalArgumentException();
	    		
	    		digits += d * Math.pow(16, (j == 0 ? 1 : 0));
	    	}
	    	
	        result[i / 2] = (byte) digits;
	    }
	    
	    return result;
	}
	
	/** Takes a byte array and creates its hex-encoding: for each byte of given array, two characters are returned in string, in big-endian notation.
	 * @param byteArray
	 * @return
	 */
	public static String byteToHex(byte[] byteArray) {
		int len = byteArray.length;
		String result = "";
		
		for (int i = 0; i < len; i++)
			result += String.format("%02X", byteArray[i]);
		
		return result.toLowerCase();
	}
}
