package hr.fer.oprpp1.hw05.crypto;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.security.InvalidAlgorithmParameterException;
import java.security.InvalidKeyException;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.security.spec.AlgorithmParameterSpec;
import java.util.Scanner;

import javax.crypto.BadPaddingException;
import javax.crypto.Cipher;
import javax.crypto.IllegalBlockSizeException;
import javax.crypto.NoSuchPaddingException;
import javax.crypto.spec.IvParameterSpec;
import javax.crypto.spec.SecretKeySpec;

/**
 * Class allows the user to encrypt/decrypt given file using the AES crypto-algorithm and the 128-bit encryption key or calculate and check the SHA-256 file digest.
 * 
 * @author Bruna
 *
 */
public class Crypto {

	static Scanner sc;
	
	/**
	 * Calculates sha-256 digest for given file.
	 * @param file given file
	 */
	public static void digestCalculation(String file) {
		System.out.println("Please provide expected sha-256 digest for " + file + ":");
		System.out.print("> ");
		String sha = sc.next();
		byte[] result = null;
		
		try (FileInputStream fis = new FileInputStream(file)) {
			byte[] bytes = new byte[4096];
			
			try {
				MessageDigest md = MessageDigest.getInstance("SHA-256");
				int bytesNumber;
				
				while ((bytesNumber = fis.read(bytes)) != -1)
					md.update(bytes, 0, bytesNumber);
				result = md.digest();
			} catch (NoSuchAlgorithmException e) {
				e.printStackTrace();
			}
			fis.close();

		} catch (FileNotFoundException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
		
		if (Util.byteToHex(result).equals(sha))
			System.out.println("Digesting completed. Digest of " + file + " matches expected digest.");
		else
			System.out.println("Digesting completed. Digest of " + file + " does not match the expected digest. Digest was: " + Util.byteToHex(result));
	}
	
	/**
	 * Encrypt/decrypt given file using the AES crypto-algorithm and the 128-bit encryption key.
	 * @param inputFile inpuFile
	 * @param outputFile outputFile
	 * @param work encrypt or decrypt
	 */
	public static void crypt(String inputFile, String outputFile, String work) {
		System.out.println("Please provide password as hex-encoded text (16 bytes, i.e. 32 hex-digits):");
		System.out.print("> ");
		String password = sc.next();
		System.out.println("Please provide initialization vector as hex-encoded text (32 hex-digits):");
		System.out.print("> ");
		String vector = sc.next();
		
		try (FileInputStream fis = new FileInputStream(inputFile);
				FileOutputStream fos = new FileOutputStream(outputFile)) {
			SecretKeySpec key = new SecretKeySpec(Util.hexToByte(password), "AES");
			AlgorithmParameterSpec parameter = new IvParameterSpec(Util.hexToByte(vector));
			
			byte[] bytes = new byte[4096];
			byte[] result;
			int bytesNumber = fis.read(bytes);
			
			try {
				Cipher cipher = Cipher.getInstance("AES/CBC/PKCS5Padding");
				cipher.init(work.equals("encryption") ? Cipher.ENCRYPT_MODE : Cipher.DECRYPT_MODE, key, parameter);
				
				while (bytesNumber > 0) {
					fos.write(cipher.update(bytes, 0, bytesNumber));
					bytesNumber = fis.read(bytes);
				}
				result = cipher.doFinal();
				fos.write(result);
				
				if (work.equals("encryption"))
						System.out.println("Encryption completed. Generated file " + outputFile + " based on file " + inputFile + ".");
				else 
					System.out.println("Decryption completed. Generated file " + outputFile + " based on file " + inputFile + ".");
			} catch (NoSuchAlgorithmException e) {
				e.printStackTrace();
			} catch (NoSuchPaddingException e) {
				e.printStackTrace();
			} catch (InvalidKeyException e) {
				e.printStackTrace();
			} catch (InvalidAlgorithmParameterException e) {
				e.printStackTrace();
			} catch (IllegalBlockSizeException e) {
				e.printStackTrace();
			} catch (BadPaddingException e) {
				e.printStackTrace();
			}	
		} 
		catch (FileNotFoundException e) {
			e.printStackTrace();
		} 
		catch (IOException e) {
			e.printStackTrace();
		} 
	}
	
	public static void main(String[] args) {
		sc = new Scanner(System.in);
		
		String work = sc.next();
		
		if (work.equals("encryption") || work.equals("decryption"))
			crypt(sc.next(), sc.next(), work);
		else
			digestCalculation(work);
		
		sc.close();
	}
}
