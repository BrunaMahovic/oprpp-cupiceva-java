package hr.fer.zemris.java.hw05.shell;

import java.util.List;

/**
 * Interface has methods which are specific for every command.
 * 
 * @author Bruna
 *
 */
public interface ShellCommand {

	/**
	 * A work that the command will perform at its call.
	 * @param env environment
	 * @param arguments arguments
	 * @return shell status
	 */
	public ShellStatus executeCommand(Environment env, String arguments);
	
	/**
	 * Gets command name.
	 * @return command name
	 */
	public String getCommandName();
	
	/**
	 * Gets command description.
	 * @return command description
	 */
	public List<String> getCommandDescription();
}
