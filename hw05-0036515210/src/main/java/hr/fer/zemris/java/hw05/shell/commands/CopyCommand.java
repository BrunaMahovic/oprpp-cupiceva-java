package hr.fer.zemris.java.hw05.shell.commands;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import hr.fer.zemris.java.hw05.shell.*;

/**
 * CopyCommand copies one file to another file or directory.
 * 
 * @author Bruna
 *
 */
public class CopyCommand implements ShellCommand {

	List<String> list = new ArrayList<>();
	
	/**
	 * Initializes description list.
	 */
	public CopyCommand() {
		list.add("The copy command expects two arguments: source file name and destination file name (i.e. paths and");
		list.add("names). If destination file exists, you should ask user is it allowed to overwrite it. Your copy command must");
		list.add("work only with files (no directories). If the second argument is directory, you should assume that user wants");
		list.add("to copy the original file into that directory using the original file name.");
	}
	
	@Override
	public ShellStatus executeCommand(Environment env, String arguments) {
		String file1;
		String file2;
		
		if (arguments.contains("\"")) {
			char[] array = arguments.toCharArray();
			int[] position = new int[4];
			int counter = 0;
 			for (int i = 0; i < array.length; i++) {
				if (array[i] == '"')
					position[counter++] = i;
			}
 			
 			if (position[0] == 0) {
 				if (array[position[1] + 1] != ' ')
 					return ShellStatus.CONTINUE;
 				file1 = arguments.substring(position[0] + 1, position[1]); 
 				
 				if (counter == 2)
 					file2 = arguments.substring(position[1] + 1).trim();
 				if (counter == 4) {
 					if (position[3] != array.length - 1)
 	 					return ShellStatus.CONTINUE;
 					file2 = arguments.substring(position[2] + 1, position[3]);
 				}
 				else
 					return ShellStatus.CONTINUE;
 			}
 			else {
 				if (position[1] != array.length - 1)
 					return ShellStatus.CONTINUE;
 				file2 = arguments.substring(position[0] + 1, position[1]); 
 				file1 = arguments.substring(0, position[0]).trim();
 			}	
		}
		else {
			String[] array = arguments.split("\\s+");
			file1 = array[0];
			file2 = array[1];
		}
		
		File src = new File(file1);
		File dest = new File(file2);
		
		if (!dest.exists()) {
			env.writeln(dest.getName() + "does not exist.");
			return ShellStatus.CONTINUE;
		}
		
		if (dest.isDirectory()) {
			file2 = file2 + "\\" + src.getName();
			dest = new File(file2);
		}
		
		env.write("Is it allowed to overwrite " + dest.getName() + " (y/n):" + env.getPromptSymbol());
		if(env.readLine().equals("n")) {
			env.writeln("Can't write in the file.");
			return ShellStatus.CONTINUE;
		}
		
		try (FileInputStream fis = new FileInputStream(src);
			FileOutputStream fos = new FileOutputStream(dest)) {
			byte[] bytes = new byte[4096];
			int bytesNumber;
			
			while ((bytesNumber = fis.read(bytes)) != -1)
				fos.write(bytes, 0, bytesNumber);
			
			env.writeln(src.getName() + " is successfully copied into " + dest.getName());
			
			fis.close();
			fos.close();
			
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
		
		return ShellStatus.CONTINUE;
	}

	@Override
	public String getCommandName() {
		return "copy";
	}

	@Override
	public List<String> getCommandDescription() {
		return Collections.unmodifiableList(list);
	}

}
