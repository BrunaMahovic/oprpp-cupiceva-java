package hr.fer.zemris.java.hw05.shell.commands;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import hr.fer.zemris.java.hw05.shell.*;

/**
 * CatCommand opens given file and writes its content to console.
 * 
 * @author Bruna
 *
 */
public class CatCommand implements ShellCommand {

	List<String> list = new ArrayList<>();

	/**
	 * Initializes description list.
	 */
	public CatCommand() {
		list.add("This command opens given file and writes its content to console.");
		list.add("Command cat takes one or two arguments. The first argument is path to some file and is mandatory. The");
		list.add("second argument is charset name that should be used to interpret chars from bytes.");
	}
	
	@Override
	public ShellStatus executeCommand(Environment env, String arguments) {
		String file1;
		String charset = null;
		
		if (arguments.contains("\"")) {
			char[] array = arguments.toCharArray();
			int[] position = new int[4];
			int counter = 0;
 			for (int i = 0; i < array.length; i++) {
				if (array[i] == '"')
					position[counter++] = i;
			}
 			
 			if (position[0] == 0) {
 				if (array[position[1] + 1] != ' ')
 					return ShellStatus.CONTINUE;
 				file1 = arguments.substring(position[0] + 1, position[1]); 
 				
 				if (counter == 2) {
 					if (position[1] != array.length - 1)
 						charset = arguments.substring(position[1] + 1).trim();
 				}	
 				else
 					return ShellStatus.CONTINUE;
 			}
 			else 
 				return ShellStatus.CONTINUE;	
		}
		else {
			String[] array = arguments.split("\\s+");
			file1 = array[0];
			if (array.length == 2)
				charset = array[1];
		}
		
		File src = new File(file1);
		
		try (FileInputStream fis = new FileInputStream(src)) {
			byte[] bytes = new byte[4096];
			int bytesNumber;
			
			while ((bytesNumber = fis.read(bytes)) != -1) {
				String s;
				if (charset == null)
					s = new String(bytes, StandardCharsets.US_ASCII);
				else
					s = new String(bytes, charset);
				env.write(s);
			}
			
			env.writeln(src.getName() + " successfully writes its content to console.");
		
			fis.close();
			
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
		
		
		return ShellStatus.CONTINUE;
	}

	@Override
	public String getCommandName() {
		return "cat";
	}

	@Override
	public List<String> getCommandDescription() {
		return Collections.unmodifiableList(list);
	}

}
