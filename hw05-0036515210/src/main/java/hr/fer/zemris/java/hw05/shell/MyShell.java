package hr.fer.zemris.java.hw05.shell;

import java.util.SortedMap;
import java.util.TreeMap;

import hr.fer.zemris.java.hw05.shell.commands.*;

/**
 * Class is used for communication between user and program.
 * 
 * @author Bruna
 *
 */
public class MyShell {

	public static void main(String[] args) {
		
		System.out.println("Welcome to MyShell v 1.0");

		SortedMap<String, ShellCommand> commands = new TreeMap<>();
		commands.put("charsets", new CharsetsCommand());
		commands.put("cat", new CatCommand());
		commands.put("ls", new LsCommand());
		commands.put("tree", new TreeCommand());
		commands.put("copy", new CopyCommand());
		commands.put("mkdir", new MkdirCommand());
		commands.put("hexdump", new HexdumpCommand());
		commands.put("symbol", new SymbolCommand());
		commands.put("help", new HelpCommand());
		commands.put("exit", new ExitCommand());
		
		Environment env = new MyEnvironment(commands, '>', '\\', '|');
		ShellStatus status = ShellStatus.CONTINUE;
		
		while (status != ShellStatus.TERMINATE) {
			
			env.write(String.valueOf(env.getPromptSymbol()) + " ");
			String line = env.readLine();
			StringBuilder sb = new StringBuilder();
			try {
				while (line.endsWith(String.valueOf(env.getMorelinesSymbol()))) {
					sb.append(line.substring(0, line.length() - 1));
					env.write(String.valueOf(env.getMultilineSymbol()) + " ");
					line = env.readLine();
				}
				sb.append(line);
				line = sb.toString();
					
				String[] parts = line.split("\\s+");
				
				status = commands.get(parts[0]).executeCommand(env, parts.length == 1 ? "" : line.substring(line.indexOf(' ') + 1));
			}
			catch (Exception e) {
				throw new ShellIOException("");
			}
		}
	}
}
