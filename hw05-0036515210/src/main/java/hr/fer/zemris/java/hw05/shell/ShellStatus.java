package hr.fer.zemris.java.hw05.shell;

/**
 * Shows the status of a shell.
 * 
 * @author Bruna
 *
 */
public enum ShellStatus {

	CONTINUE, TERMINATE
}
