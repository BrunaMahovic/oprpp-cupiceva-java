package hr.fer.zemris.java.hw05.shell.commands;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import hr.fer.zemris.java.hw05.shell.*;

/**
 * SymbolCommand writes out and changes symbols.
 * 
 * @author Bruna
 *
 */
public class SymbolCommand implements ShellCommand {

	List<String> list = new ArrayList<>();
	
	/**
	 * Initializes description list.
	 */
	public SymbolCommand() {
		list.add("Command symbol can be used to change other symbols.");
	}
	
	@Override
	public ShellStatus executeCommand(Environment env, String arguments) {
		String[] words = arguments.trim().split(" ");
		
		if (words.length == 1) {
			if (words[0].equals("PROMPT"))
				env.writeln("Symbol for " + words[0] + " is '" + String.valueOf(env.getPromptSymbol()) + "'");
			else if (words[0].equals("MORELINES"))
				env.writeln("Symbol for " + words[0] + " is '" + String.valueOf(env.getMorelinesSymbol()) + "'");
			else if (words[0].equals("MULTILINE"))
				env.writeln("Symbol for " + words[0] + " is '" + String.valueOf(env.getMultilineSymbol()) + "'");
		}
		else if (words.length == 2) {
			if (words[0].equals("PROMPT")) {
				env.writeln("Symbol for " + words[0] + " changed from '" + String.valueOf(env.getPromptSymbol()) + "' to '" + words[1] + "'");
				env.setPromptSymbol(words[1].charAt(0));
			}
			else if (words[0].equals("MORELINES")) {
				env.writeln("Symbol for " + words[0] + " changed from '" + String.valueOf(env.getMorelinesSymbol()) + "' to '" + words[1] + "'");
				env.setMorelinesSymbol(words[1].charAt(0));
			}
			else if (words[0].equals("MULTILINE")) {
				env.writeln("Symbol for " + words[0] + " changed from '" + String.valueOf(env.getMultilineSymbol()) + "' to '" + words[1] + "'");
				env.setMultilineSymbol(words[1].charAt(0));
			}	
		}
		else 
			return ShellStatus.CONTINUE;
		
		return ShellStatus.CONTINUE;
	}

	@Override
	public String getCommandName() {
		return "symbol";
	}

	@Override
	public List<String> getCommandDescription() {
		return Collections.unmodifiableList(list);
	}

}
