package hr.fer.zemris.java.hw05.shell.commands;

import java.io.File;
import java.io.IOException;
import java.nio.file.FileVisitResult;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.nio.file.SimpleFileVisitor;
import java.nio.file.attribute.BasicFileAttributes;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import hr.fer.zemris.java.hw05.shell.*;

/**
 * TreeCommand prints a tree.
 * 
 * @author Bruna
 *
 */
public class TreeCommand implements ShellCommand {

	List<String> list = new ArrayList<>();
	
	/**
	 * Initializes description list.
	 */
	public TreeCommand() {
		list.add("The tree command expects a single argument: directory name and prints a tree (each directory level shifts");
		list.add("output two charatcers to the right).");
	}
	
	@Override
	public ShellStatus executeCommand(Environment env, String arguments) {
		if (arguments.startsWith("\"")) {
			if (arguments.endsWith("\""))
				arguments.replaceAll("\"", "");
			else 
				return ShellStatus.CONTINUE;
		}
		
		File directory = new File(arguments);
		if (!directory.isDirectory()) {
			env.writeln(directory.getName() + " is not a directory.");
			return ShellStatus.CONTINUE;
		}
		
		try {
			Files.walkFileTree(Paths.get(arguments), new SimpleFileVisitor<Path>() {
				int i = -1;
			    @Override
			    public FileVisitResult preVisitDirectory(Path dir, BasicFileAttributes attrs)
			            throws IOException {
			    	i++;
			    	env.writeln(new String(" ").repeat(i * 2) + dir.getFileName().toString());
			        return FileVisitResult.CONTINUE;
			    }

			    @Override
			    public FileVisitResult visitFile(Path file, BasicFileAttributes attrs)
			            throws IOException {
			    	env.writeln(new String(" ").repeat((i + 1) * 2) + file.getFileName().toString());
			        return FileVisitResult.CONTINUE;
			    }
			    
			    @Override
			    public FileVisitResult postVisitDirectory(Path file, IOException e) {
			    	i--;
			        return FileVisitResult.CONTINUE;
			    }
			});
		} catch (IOException e) {
			e.printStackTrace();
		}
		return ShellStatus.CONTINUE;
	}

	@Override
	public String getCommandName() {
		return "tree";
	}

	@Override
	public List<String> getCommandDescription() {
		return Collections.unmodifiableList(list);
	}

}
