package hr.fer.zemris.java.hw05.shell;

/**
 * Class implements exception which happens if reading or writing fails.
 * 
 * @author Bruna
 *
 */
public class ShellIOException extends RuntimeException {

	private static final long serialVersionUID = 1L;

    /**
     * The action which happens if reading or writing fails.
     * @param message
     */
    public ShellIOException(String message) {
    	super("Reading or writing failed!");
    }
}
