package hr.fer.zemris.java.hw05.shell;

import java.util.Collections;
import java.util.Scanner;
import java.util.SortedMap;

/**
 * Class has methods which communicates with user.
 * 
 * @author Bruna
 *
 */
public class MyEnvironment implements Environment {

	SortedMap<String, ShellCommand> commands;
	Character PROMPTSYMBOL;
	Character MORELINESSYMBOL;
	Character MULTILINESYMBOL;
	Scanner sc = new Scanner(System.in);
	
	/**
	 * Initializes given values.
	 * @param commands
	 * @param PROMPTSYMBOL
	 * @param MORELINESSYMBOL
	 * @param MULTILINESYMBOL
	 */
	public MyEnvironment(SortedMap<String, ShellCommand> commands, Character PROMPTSYMBOL, Character MORELINESSYMBOL, Character MULTILINESYMBOL) {
		this.commands = commands;
		this.PROMPTSYMBOL = PROMPTSYMBOL;
		this.MORELINESSYMBOL = MORELINESSYMBOL;
		this.MULTILINESYMBOL = MULTILINESYMBOL;
	}
	
	@Override
	public String readLine() throws ShellIOException {
		return sc.nextLine();
	}

	@Override
	public void write(String text) throws ShellIOException {
		System.out.print(text);
	}

	@Override
	public void writeln(String text) throws ShellIOException {
		System.out.println(text);
	}

	@Override
	public SortedMap<String, ShellCommand> commands() {
		return Collections.unmodifiableSortedMap(commands);
	}

	@Override
	public Character getMultilineSymbol() {
		return MULTILINESYMBOL;
	}

	@Override
	public void setMultilineSymbol(Character symbol) {
		MULTILINESYMBOL = symbol;
	}

	@Override
	public Character getPromptSymbol() {
		return PROMPTSYMBOL;
	}

	@Override
	public void setPromptSymbol(Character symbol) {
		PROMPTSYMBOL = symbol;
	}

	@Override
	public Character getMorelinesSymbol() {
		return MORELINESSYMBOL;
	}

	@Override
	public void setMorelinesSymbol(Character symbol) {
		MORELINESSYMBOL = symbol;
	}

}
