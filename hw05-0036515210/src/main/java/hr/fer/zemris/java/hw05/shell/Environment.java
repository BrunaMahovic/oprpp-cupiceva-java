package hr.fer.zemris.java.hw05.shell;

import java.util.SortedMap;

/**
 * Interface has methods which communicates with user.
 * 
 * @author Bruna
 *
 */
public interface Environment {

	/**
	 * Reads line with scanner.
	 * @return String line
	 * @throws ShellIOException
	 */
	public String readLine() throws ShellIOException;
	
	/**
	 * Prints given text to console or in a file.
	 * @param text given text
	 * @throws ShellIOException
	 */
	public void write(String text) throws ShellIOException;
	
	/**
	 * Prints line of given text to console or in a file.
	 * @param text given text
	 * @throws ShellIOException
	 */
	public void writeln(String text) throws ShellIOException;
	
	/**
	 * Returns unmodifiable map, so that the client can not delete commands by clearing the map.
	 * @return unmodifiable map
	 */
	public SortedMap<String, ShellCommand> commands();
	
	/**
	 * Gets multiline symbol.
	 * @return multiline symbol
	 */
	public Character getMultilineSymbol();
	
	/**
	 * Sets multiline symbol.
	 * @param symbol
	 */
	public void setMultilineSymbol(Character symbol);
	
	/**
	 * Gets prompt symbol.
	 * @return prompt symbol
	 */
	public Character getPromptSymbol();
	
	/**
	 * Sets prompt symbol.
	 * @param symbol
	 */
	public void setPromptSymbol(Character symbol);
	
	/**
	 * Gets morelines symbol.
	 * @return morelines symbol
	 */
	public Character getMorelinesSymbol();
	
	/**
	 * Sets morelines symbol.
	 * @param symbol
	 */
	public void setMorelinesSymbol(Character symbol);
}
