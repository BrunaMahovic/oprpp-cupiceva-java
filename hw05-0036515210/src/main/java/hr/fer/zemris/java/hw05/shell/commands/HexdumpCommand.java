package hr.fer.zemris.java.hw05.shell.commands;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import hr.fer.zemris.java.hw05.shell.*;

/**
 * HexdumpCommand produces hex-output for file content.
 * 
 * @author Bruna
 *
 */
public class HexdumpCommand implements ShellCommand {

	List<String> list = new ArrayList<>();
	
	/**
	 * Initializes description list.
	 */
	public HexdumpCommand() {
		list.add("The hexdump command expects a single argument: file name, and produces hex-output.");
		list.add(" On the right side of the image only a standard subset of characters is shown; for all other characters a");
		list.add(".' is printed instead (i.e. replace all bytes whose value is less than 32 or greater than 127 with '.').");
	}
	
	@Override
	public ShellStatus executeCommand(Environment env, String arguments) {
		if (arguments.startsWith("\"")) {
			if (arguments.endsWith("\""))
				arguments.replaceAll("\"", "");
			else 
				return ShellStatus.CONTINUE;
		}
		
		File file = new File(arguments);
		if (file.isDirectory()) {
			env.writeln(file.getName() + "is a directory. Argument must be a file.");
			return ShellStatus.CONTINUE;
		}
		
		try (FileInputStream fis = new FileInputStream(file)) {
			byte[] bytes = new byte[16];
			int bytesNumber;
			int b = 0;
			int counter = 0;
			int ascii;
			
			while ((bytesNumber = fis.read(bytes)) != -1) {
				env.write(new String("0").repeat(8 - Integer.toHexString(counter).length()) + Integer.toHexString(counter).toUpperCase() + ": ");
				for (int i = 0; i < bytesNumber; i++) {
					ascii = (int) bytes[0];
					if (ascii > 32 && ascii < 127)
						env.write(i == 7 ? Integer.toHexString(ascii).toUpperCase() + "|" : Integer.toHexString(ascii).toUpperCase() + " ");
					else {
						bytes[i] = '.';
						env.write(i == 7 ? " " + Integer.toHexString(ascii).toUpperCase() + "|" : "0" + Integer.toHexString(ascii).toUpperCase() + " ");
					}
						
				}
				counter += 16;
				b = bytesNumber;
			}
			
			if (bytesNumber < 16) {
				for (int i = (16 - b); i >= 0; i--) {
					env.write((16 - 8 == i) ? "|" : "   ");
				}
			}
			env.writeln("| " + new String(bytes).substring(0, bytes.length));
			fis.close();
			
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
		
		return ShellStatus.CONTINUE;
	}

	@Override
	public String getCommandName() {
		return "hexdump";
	}

	@Override
	public List<String> getCommandDescription() {
		return Collections.unmodifiableList(list);
	}

}
