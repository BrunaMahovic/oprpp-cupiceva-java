package hr.fer.zemris.java.hw05.shell.commands;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Map;
import java.util.SortedMap;

import hr.fer.zemris.java.hw05.shell.*;

/**
 * HelpCommand provides information about available commands and their descriptions.
 * 
 * @author Bruna
 *
 */
public class HelpCommand implements ShellCommand {

	List<String> list = new ArrayList<>();
	
	/**
	 * Initializes description list.
	 */
	public HelpCommand() {
		list.add("Provides information about available commands and their descriptions.");
	}
	
	@Override
	public ShellStatus executeCommand(Environment env, String arguments) {
		if (!arguments.equals("")) {
			ShellCommand command = env.commands().get(arguments.trim());
			for (String d: command.getCommandDescription())
				env.writeln(d);
		}
		else {
			SortedMap<String, ShellCommand> commands = env.commands();
			for (Map.Entry<String, ShellCommand> command: commands.entrySet()) {
				env.writeln(command.getValue().getCommandName() + ":");
				for (String d: command.getValue().getCommandDescription())
					env.writeln(d);
				env.writeln("");
			}
		}
		
		return ShellStatus.CONTINUE;
	}

	@Override
	public String getCommandName() {
		return "help";
	}

	@Override
	public List<String> getCommandDescription() {
		return Collections.unmodifiableList(list);
	}

}
