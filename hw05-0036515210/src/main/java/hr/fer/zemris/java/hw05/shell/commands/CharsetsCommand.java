package hr.fer.zemris.java.hw05.shell.commands;

import java.nio.charset.Charset;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import hr.fer.zemris.java.hw05.shell.*;

/**
 * CharsetsCommand lists names of supported charsets for your Java platform.
 * @author Bruna
 *
 */
public class CharsetsCommand implements ShellCommand {

	List<String> list = new ArrayList<>();
	
	/**
	 * Initializes description list. 
	 */
	public CharsetsCommand() {
		list.add("Command charsets takes no arguments and lists names of supported charsets for your Java platform.");
	}
	
	@Override
	public ShellStatus executeCommand(Environment env, String arguments) {
		for (String charset: Charset.availableCharsets().keySet())
			env.writeln(charset);
		
		return ShellStatus.CONTINUE;
	}

	@Override
	public String getCommandName() {
		return "charsets";
	}

	@Override
	public List<String> getCommandDescription() {
		return Collections.unmodifiableList(list);
	}

}
