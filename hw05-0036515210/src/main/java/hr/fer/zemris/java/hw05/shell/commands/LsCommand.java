package hr.fer.zemris.java.hw05.shell.commands;

import java.io.File;
import java.io.IOException;
import java.nio.file.DirectoryStream;
import java.nio.file.Files;
import java.nio.file.LinkOption;
import java.nio.file.Path;
import java.nio.file.attribute.BasicFileAttributeView;
import java.nio.file.attribute.BasicFileAttributes;
import java.nio.file.attribute.FileTime;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Date;
import java.util.List;

import hr.fer.zemris.java.hw05.shell.*;

/**
 * LsCommand writes a directory listing.
 * 
 * @author Bruna
 *
 */
public class LsCommand implements ShellCommand {

	List<String> list = new ArrayList<>();
	
	/**
	 * Initializes description list.
	 */
	public LsCommand() {
		list.add("Command ls takes a single argument – directory – and writes a directory listing (not recursive).");
		list.add("The output consists of 4 columns. First column indicates if current object is directory (d), readable (r),");
		list.add("writable (w) and executable (x). Second column contains object size in bytes that is right aligned and");
		list.add("occupies 10 characters. Follows file creation date/time and finally file name.");
	}
	
	@Override
	public ShellStatus executeCommand(Environment env, String arguments) {
		if (arguments.startsWith("\"")) {
			if (arguments.endsWith("\""))
				arguments.replaceAll("\"", "");
			else 
				return ShellStatus.CONTINUE;
		}
		
		File directory = new File(arguments.trim());
		if (!directory.isDirectory()) {
			env.writeln(directory.getName() + " is not a directory.");
			return ShellStatus.CONTINUE;
		}
		
		try (DirectoryStream<Path> stream = Files.newDirectoryStream(directory.toPath())) {
			
			for (Path path: stream) {
				env.write(Files.isDirectory(path) ? "d" : "-");
				env.write(Files.isReadable(path) ? "r" : "-");
				env.write(Files.isWritable(path) ? "w" : "-");
				env.write(Files.isExecutable(path) ? "x" : "-");
				
				String size = String.valueOf(Files.size(path));
				env.write(new String(" ".repeat(10 - size.length())) + Files.size(path) + " ");
				
				SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
				BasicFileAttributeView faView = Files.getFileAttributeView(
						path, BasicFileAttributeView.class, LinkOption.NOFOLLOW_LINKS
						);
				BasicFileAttributes attributes = faView.readAttributes();
				FileTime fileTime = attributes.creationTime();
				String formatted = sdf.format(new Date(fileTime.toMillis()));
				env.write(formatted + " ");
				
				env.writeln(path.getFileName().toString());
				
			}
			
		} catch (IOException e) {
			e.printStackTrace();
		}
		
		return ShellStatus.CONTINUE;
	}

	@Override
	public String getCommandName() {
		return "ls";
	}

	@Override
	public List<String> getCommandDescription() {
		return Collections.unmodifiableList(list);
	}

}
