package hr.fer.zemris.java.hw05.shell.commands;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import hr.fer.zemris.java.hw05.shell.*;

/**
 * MkdirCommand creates the appropriate directory structure.
 * 
 * @author Bruna
 *
 */
public class MkdirCommand implements ShellCommand {

	List<String> list = new ArrayList<>();
	
	/**
	 * Initializes description list.
	 */
	public MkdirCommand() {
		list.add("The mkdir command takes a single argument: directory name, and creates the appropriate directory ");
		list.add("structure.");
	}
	
	@Override
	public ShellStatus executeCommand(Environment env, String arguments) {
		if (arguments.startsWith("\"")) {
			if (arguments.endsWith("\""))
				arguments.replaceAll("\"", "");
			else 
				return ShellStatus.CONTINUE;
		}
		
		Path path = Paths.get(arguments);

	    try {
			Files.createDirectories(path);
		} catch (IOException e) {
			env.writeln(path.toString() + " is not properly initialized.");
			e.printStackTrace();
		}
		
		return ShellStatus.CONTINUE;
	}

	@Override
	public String getCommandName() {
		return "mkdir";
	}

	@Override
	public List<String> getCommandDescription() {
		return Collections.unmodifiableList(list);
	}

}
