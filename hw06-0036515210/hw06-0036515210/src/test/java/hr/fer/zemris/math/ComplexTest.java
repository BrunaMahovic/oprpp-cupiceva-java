package hr.fer.zemris.math;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

public class ComplexTest {

	@Test
	public void testComplexConstructor() {
		Complex num = new Complex(2, 2);
		Assertions.assertEquals(2, num.real);
	}
	
	@Test
	public void testGetMagnitudeMethod() {
		Complex num = new Complex(4, 3);
		Assertions.assertTrue(Double.compare(5, num.getMagnitude()) == 0);
	}
	
	@Test
	public void testGetAngleMethod() {
		Complex num = new Complex(1, 0);
		Assertions.assertTrue(Double.compare(0, num.getAngle()) == 0);
	}
	
	@Test
	public void testAddMethod() {
		Complex first = new Complex(4, 3);
		Complex second = new Complex(3, 8);
		Complex result = first.add(second);
		Assertions.assertEquals(11, result.imaginary);
	}
	
	@Test
	public void testSubMethod() {
		Complex first = new Complex(4, 3);
		Complex second = new Complex(3, 8);
		Complex result = first.sub(second);
		Assertions.assertEquals(1, result.real);
	}
	
	@Test
	public void testMulMethod() {
		Complex first = new Complex(4, 1);
		Complex second = new Complex(2, 3);
		Complex result = first.multiply(second);
		Assertions.assertEquals(14, result.imaginary);
	}
	
	@Test
	public void testDivMethod() {
		Complex first = new Complex(4, 1);
		Complex second = new Complex(1, 0);
		Complex result = first.divide(second);
		Assertions.assertTrue(Double.compare(1, result.imaginary) == 0);
	}
	
//	@Test
//	public void testPowerMethod() {
//		Complex first = new Complex(1, 0);
//		Complex result = first.power(1);
//		Assertions.assertTrue(Double.compare(1, result.imaginary) == 0);
//	}
	
	@Test
	public void testPowerMethodThrowsException() {
		Complex first = new Complex(3, 3);
		Assertions.assertThrows(IllegalArgumentException.class, () -> first.power(-1));
	}
	
//	@Test
//	public void testRootMethod() {
//		Complex first = new Complex(3, 3);
//		Complex result = first.power(5);
//		Assertions.assertTrue(Double.compare(-972, result.imaginary) == 0);
//	}
	
	@Test
	public void testRootMethodThrowsException() {
		Complex first = new Complex(3, 3);
		Assertions.assertThrows(IllegalArgumentException.class, () -> first.root(0));
	}
}
