package hr.fer.zemris.math;

/**
 * ComplexRootedPolynomial models a polynomial over complex numbers.
 * 
 * @author Bruna
 *
 */
public class ComplexRootedPolynomial {

	Complex constant;
	Complex[] roots;
	
	/**
	 * Constructor.
	 * @param constant constant 
	 * @param roots zero points
	 */
	public ComplexRootedPolynomial(Complex constant, Complex ... roots) {
		this.constant = constant;
		this.roots = roots;
	}
	
	/**
	 * Computes polynomial value at given point z.
	 * @param z given point
	 * @return polynomial value at given point z
	 */
	public Complex apply(Complex z) {
		Complex result = constant;
		for (Complex c: roots)
			result = result.multiply(z.sub(c));
		return result;
	}
	
	/**
	 * Converts this representation to ComplexPolynomial type.
	 * @return ComplexPolynomial type
	 */
	public ComplexPolynomial toComplexPolynom() {
		ComplexPolynomial result = new ComplexPolynomial(constant);
		for (int i = 0; i < roots.length; i++) {
			result = result.multiply(new ComplexPolynomial(roots[i], Complex.ONE));
		}
		return result;
	}
	
	@Override
	public String toString() {
		StringBuilder sb = new StringBuilder();
		sb.append("(" + this.constant.toString() + ")");
		for (int i = 0; i < roots.length; i++) 
			sb.append("*(z-(" + roots[i].toString() + "))");
		return sb.toString();
	}
	
	/**
	 * Finds index of closest root for given complex number z that is within threshold.
	 * @param z given complex number
	 * @param threshold threshold
	 * @return index of closest root for given complex number z that is within threshold if exists, otherwise -1
	 */
	public int indexOfClosestRootFor(Complex z, double threshold) {
		double min = roots[0].sub(z).module();
		int result = 0;
		for (int i = 1; i < roots.length; i++) {
			if (roots[i].sub(z).module() < min) {
				min = roots[i].sub(z).module();
				result = i;
			}
		}
		
		if (min <= threshold)
			return result;
		else
			return -1;
	}
}
