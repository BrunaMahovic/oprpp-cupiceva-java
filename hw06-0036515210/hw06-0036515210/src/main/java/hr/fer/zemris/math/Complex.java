package hr.fer.zemris.math;

import java.util.ArrayList;
import java.util.List;

/**
 * Complex implements a complex number and defines complex mathematical functions.
 * 
 * @author Bruna
 *
 */
public class Complex {

	double real;
	double imaginary;
	
	public static final Complex ZERO = new Complex(0,0);
	public static final Complex ONE = new Complex(1,0);
	public static final Complex ONE_NEG = new Complex(-1,0);
	public static final Complex IM = new Complex(0,1);
	public static final Complex IM_NEG = new Complex(0,-1);
	
	/**
	 * Constructs the complex number.
	 */
	public Complex() {
		
	}
	
	/**
	 * Constructs the complex number z = x + i*y.
	 * @param re real part	
	 * @param im imaginary part
	 */
	public Complex(double re, double im) {
		this.real = re;
		this.imaginary = im;
	}
	
	/**
	 * Module of this complex number
	 * @return module of complex number
	 */
	public double module() {
		if (this.real != 0 || this.imaginary != 0) {
            return Math.sqrt(this.real * this.real + this.imaginary * this.imaginary);
        } 
        return 0;   
	}
	
	/**
	 * Multiplies another complex to the current complex number.
	 * @param c another complex number
	 * @return a new complex number
	 */
	public Complex multiply(Complex c) {
		double r = this.real * c.real - this.imaginary * c.imaginary;
        double i = this.real * c.imaginary + this.imaginary * c.real;
        return new Complex(r, i);
	}
	
	/**
	 * Divides the current complex by another complex number.
	 * @param c another complex number
	 * @return a new complex number
	 */
	public Complex divide(Complex c) {
		Complex first = this.multiply(new Complex(c.real, -1.0 * c.imaginary));
		double second = c.real * c.real + c.imaginary * c.imaginary;
		return new Complex(first.real / second, first.imaginary / second);
	}
	
	/**
	 * Adds another complex to the current complex number.
	 * @param c another complex number
	 * @return a new complex number
	 */
	public Complex add(Complex c) {
		return new Complex(this.real + c.real, this.imaginary + c.imaginary);
	}
	
	/**
	 * Subtracts another complex from the current complex number.
	 * @param c another complex number
	 * @return a new complex number
	 */
	public Complex sub(Complex c) {
		return new Complex(this.real - c.real, this.imaginary - c.imaginary);
	}
	
	/**
	 * Negates current complex number.
	 * @return negated current complex number
	 */
	public Complex negate() {
		return new Complex(-1 * this.real, -1 * this.imaginary);
	}
	
	/**
	 * Calculates the complex number to the passed integer power.
	 * @param n the power
	 * @return a new complex number
	 */
	public Complex power(int n) {
		if (n < 0) throw new IllegalArgumentException();
		Complex solution = new Complex(this.real, this.imaginary);
        for(int j = 1; j < n; j++)
            solution = solution.multiply(this);
        
        return solution;
	}
	
	/**
	 * Gets a magnitude of complex number.
	 * @return magnitude of complex number
	 */
	public double getMagnitude() {
		return Math.sqrt(Math.pow(this.real, 2) + Math.pow(this.imaginary, 2));
	}
	
	/**
	 * Gets an angle of complex number.
	 * @return angle of complex number
	 */
	public double getAngle() {
		return Math.atan2(this.imaginary, this.real);
	}
	
	/**
	 * Calculates the root of the complex number.
	 * @param n the root
	 * @return list of complex numbers
	 */
	public List<Complex> root(int n) {
		if (n <= 0) throw new IllegalArgumentException();
		List<Complex> list = new ArrayList<>();
	   
    	double R = Math.pow(this.getMagnitude(), 1 / n);
    	double pi = Math.PI;
    	double s = this.getAngle();
        for (int k = 0; k < n; k++) {
    	    list.add(new Complex(R * Math.cos((s + 2*k*pi) / n), R * Math.sin((s + 2*k*pi) / n)));
        }
	    
	    return list;
	}
	
	/**
	 * Gets the complex number in x + iy format.
	 */
	@Override
	public String toString() {
		if (this.imaginary >= 0) 		
			return String.valueOf(this.real) + "+i" + String.valueOf(this.imaginary);
		else 
			return String.valueOf(this.real) + "-i" + String.valueOf(this.imaginary).substring(1);
	}
}
