package hr.fer.zemris.java.fractals;

import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;
import java.util.concurrent.BlockingQueue;
import java.util.concurrent.LinkedBlockingQueue;
import java.util.concurrent.atomic.AtomicBoolean;
import hr.fer.zemris.java.fractals.viewer.FractalViewer;
import hr.fer.zemris.java.fractals.viewer.IFractalProducer;
import hr.fer.zemris.java.fractals.viewer.IFractalResultObserver;
import hr.fer.zemris.math.Complex;
import hr.fer.zemris.math.ComplexPolynomial;
import hr.fer.zemris.math.ComplexRootedPolynomial;

/**
 * Main program used for drawing.
 * 
 * @author Bruna
 *
 */
public class NewtonParallel {
 
	private static ComplexRootedPolynomial crp;
	private static ComplexPolynomial cp;
	private static ComplexPolynomial cpd;
	
	public static int workers = Runtime.getRuntime().availableProcessors();
	public static int tracks = 4 * Runtime.getRuntime().availableProcessors();
	
	public static void main(String[] args) {
		NewtonParallel np = new NewtonParallel();
		np.parseArgs(args);
		
		System.out.println("Welcome to Newton-Raphson iteration-based fractal viewer.");
		System.out.println("Please enter at least two roots, one root per line. Enter 'done' when done.");

		Newton newton = new Newton();
		Scanner sc = new Scanner(System.in);
		String input = "";
		int i = 1;
		List<Complex> list = new ArrayList<>();
		
		while (true) {
			System.out.print("Root " + i + "> ");
			input = sc.nextLine();
			if (input.toLowerCase().equals("done"))
				break;
			list.add(newton.parse(input));
			i++;					
		}
		
		Complex[] array = new Complex[list.size()];
		list.toArray(array);
		
		crp = new ComplexRootedPolynomial(Complex.ONE, array);
		cp = crp.toComplexPolynom();
		cpd = cp.derive();
		
		FractalViewer.show(new MojProducer());	
		sc.close();
		System.out.println("Image of fractal will appear shortly. Thank you.");
	}
	
	/**
	 * Parses values ​​of the arguments.
	 * @param args arguments
	 * @throws NumberFormatException if arguments are in the wrong format
	 */
	public void parseArgs(String[] args) throws NumberFormatException {
		int w = 0;
		int t = 0;
		for (int i = 0; i < args.length; i++) {
			if (args[i].equals("-w")) {
				workers = Integer.valueOf(args[++i]);
				if (workers < 1)
					throw new IllegalArgumentException();
				w++;
			}
			else if (args[i].equals("-t")) {
				tracks = Integer.valueOf(args[++i]);
				if (tracks < 1)
					throw new IllegalArgumentException();
				t++;
			}
			else if (args[i].startsWith("--workers=")) {
				workers = Integer.valueOf(args[i].substring(args[i].indexOf('=') + 1));
				if (workers < 1)
					throw new IllegalArgumentException();
				w++;
			}
			else if (args[i].startsWith("--tracks=")) {
				tracks = Integer.valueOf(args[i].substring(args[i].indexOf('=') + 1));
				if (tracks < 1)
					throw new IllegalArgumentException();
				t++;
			}
		}
		
		if (w > 1 || t > 1)
			throw new IllegalArgumentException();
	}

	/**
	 * The class defines a method of no arguments called run.
	 * 
	 * @author Bruna
	 *
	 */
	public static class PosaoIzracuna implements Runnable {
		double reMin;
		double reMax;
		double imMin;
		double imMax;
		int width;
		int height;
		int yMin;
		int yMax;
		int m;
		short[] data;
		AtomicBoolean cancel;
		public static PosaoIzracuna NO_JOB = new PosaoIzracuna();
		
		/**
		 * Constructor.
		 */
		private PosaoIzracuna() {
		}
		
		/**
		 * Constructor.
		 * @param reMin 
		 * @param reMax
		 * @param imMin
		 * @param imMax
		 * @param width
		 * @param height
		 * @param yMin
		 * @param yMax
		 * @param m
		 * @param data
		 * @param cancel
		 */
		public PosaoIzracuna(double reMin, double reMax, double imMin,
				double imMax, int width, int height, int yMin, int yMax, 
				int m, short[] data, AtomicBoolean cancel) {
			super();
			this.reMin = reMin;
			this.reMax = reMax;
			this.imMin = imMin;
			this.imMax = imMax;
			this.width = width;
			this.height = height;
			this.yMin = yMin;
			this.yMax = yMax;
			this.m = m;
			this.data = data;
			this.cancel = cancel;
		}
		
		@Override
		public void run() {
			System.out.println("Zapocinjem izracun...");
			int offset = yMin * width;
			for(int y = yMin; y <= yMax; y++) {
				if(cancel.get()) break;
				for(int x = 0; x < width; x++) {
					double cre = x / (width-1.0) * (reMax - reMin) + reMin;
					double cim = (height-1.0-y) / (height-1) * (imMax - imMin) + imMin;
					Complex zn = new Complex(cre, cim);
					double module = 0;
					int iters = 0;
					Complex znold = new Complex();
					do {
						znold = zn;
						zn = zn.sub(cp.apply(zn).divide(cpd.apply(zn)));
						module = znold.sub(zn).module();
						iters++;
					} while(module > 0.001 && iters < m);
					int index = crp.indexOfClosestRootFor(zn, 0.002);
					data[offset] = (short)(index + 1);
					offset++;
				}	
			}
		}
	}
	
	/**
	 * Class has method which is used for drawing.
	 * 
	 * @author Bruna
	 *
	 */
	public static class MojProducer implements IFractalProducer {
		
		/**
		 * Method is used for drawing.
		 */
		@Override
		public void produce(double reMin, double reMax, double imMin, double imMax,
				int width, int height, long requestNo, IFractalResultObserver observer, AtomicBoolean cancel) {
			System.out.println("Zapocinjem izracun...");
			int m = 16*16*16;
			short[] data = new short[width * height];
			int brojTraka = tracks;
			int brojYPoTraci = height / brojTraka;
			
			final BlockingQueue<PosaoIzracuna> queue = new LinkedBlockingQueue<>();

			Thread[] radnici = new Thread[workers];
			for(int i = 0; i < radnici.length; i++) {
				radnici[i] = new Thread(new Runnable() {
					@Override
					public void run() {
						while(true) {
							PosaoIzracuna p = null;
							try {
								p = queue.take();
								if(p==PosaoIzracuna.NO_JOB) break;
							} catch (InterruptedException e) {
								continue;
							}
							p.run();
						}
					}
				});
			}
			for(int i = 0; i < radnici.length; i++) {
				radnici[i].start();
			}
			
			for(int i = 0; i < brojTraka; i++) {
				int yMin = i*brojYPoTraci;
				int yMax = (i+1)*brojYPoTraci-1;
				if(i==brojTraka-1) {
					yMax = height-1;
				}
				PosaoIzracuna posao = new PosaoIzracuna(reMin, reMax, imMin, imMax, width, height, yMin, yMax, m, data, cancel);
				while(true) {
					try {
						queue.put(posao);
						break;
					} catch (InterruptedException e) {
					}
				}
			}
			for(int i = 0; i < radnici.length; i++) {
				while(true) {
					try {
						queue.put(PosaoIzracuna.NO_JOB);
						break;
					} catch (InterruptedException e) {
					}
				}
			}
			
			for(int i = 0; i < radnici.length; i++) {
				while(true) {
					try {
						radnici[i].join();
						break;
					} catch (InterruptedException e) {
					}
				}
			}
			
			System.out.println("Racunanje gotovo. Idem obavijestiti promatraca tj. GUI!");
			observer.acceptResult(data, (short)(cp.order()+1), requestNo);
		}
	}
}
