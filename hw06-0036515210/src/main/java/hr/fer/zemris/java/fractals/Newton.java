package hr.fer.zemris.java.fractals;

import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;
import java.util.concurrent.atomic.AtomicBoolean;

import hr.fer.zemris.java.fractals.viewer.FractalViewer;
import hr.fer.zemris.java.fractals.viewer.IFractalProducer;
import hr.fer.zemris.java.fractals.viewer.IFractalResultObserver;
import hr.fer.zemris.math.*;

/**
 * Main program used for drawing.
 * 
 * @author Bruna
 *
 */
public class Newton {

	public static void main(String[] args) {
		System.out.println("Welcome to Newton-Raphson iteration-based fractal viewer.");
		System.out.println("Please enter at least two roots, one root per line. Enter 'done' when done.");

		Newton newton = new Newton();
		Scanner sc = new Scanner(System.in);
		String input = "";
		int i = 1;
		List<Complex> list = new ArrayList<>();
		
		while (true) {
			System.out.print("Root " + i + "> ");
			input = sc.nextLine();
			if (input.toLowerCase().equals("done"))
				break;
			list.add(newton.parse(input));
			i++;					
		}
		
		Complex[] array = new Complex[list.size()];
		list.toArray(array);
		
		ComplexRootedPolynomial crp = new ComplexRootedPolynomial(Complex.ONE, array);
		ComplexPolynomial cp = crp.toComplexPolynom();
		ComplexPolynomial cpd = cp.derive();
		
		FractalViewer.show(new MojProducer(crp, cp, cpd));	
		sc.close();
		System.out.println("Image of fractal will appear shortly. Thank you.");
	}
	
	/**
	 * Parses values ​​of the input string.
	 * @param complex input string
	 * @return a new complex number
	 */
	public Complex parse(String complex) {
		String[] parts = complex.split(" ");

		if (parts.length == 3) {
			try {
				double real = Double.valueOf(parts[0]);
				String im;
				if (parts[2].equals("i"))
					im = parts[2].replaceAll("i", "1");
				else
					im = parts[2].replaceAll("i", "");
				double imaginary = Double.valueOf(im);
				if (parts[1].equals("-"))
					imaginary = -1 * imaginary;
				return new Complex(real, imaginary);
			}
			catch (NumberFormatException e) {
				e.printStackTrace();
			}
		}
		
		if (parts.length == 1) {
			try {
				if (parts[0].contains("i")) {
					String imaginary;
					if (parts[0].equals("i") || parts[0].equals("-i"))
						imaginary = parts[0].replaceAll("i", "1");
					else 
						imaginary = parts[0].replaceAll("i", "");
					return new Complex(0, Double.valueOf(imaginary));
				}
				else
					return new Complex(Double.valueOf(parts[0]), 0);
			}
			catch (NumberFormatException e) {
				e.printStackTrace();
			}				
		}
		
		else
			throw new IllegalArgumentException();
		
		return new Complex();
	}
	
	/**
	 * Class has method which is used for drawing.
	 * 
	 * @author Bruna
	 *
	 */
	public static class MojProducer implements IFractalProducer {
		
		private ComplexRootedPolynomial crp;
		private ComplexPolynomial cp;
		private ComplexPolynomial cpd;
		
		/**
		 * Constructor.
		 * @param crp
		 * @param cp
		 * @param cpd
		 */
		public MojProducer(ComplexRootedPolynomial crp, ComplexPolynomial cp, ComplexPolynomial cpd) {
			this.crp = crp;
			this.cp = cp;
			this.cpd = cpd;
		}
		
		/**
		 * Method is used for drawing.
		 */
		@Override
		public void produce(double reMin, double reMax, double imMin, double imMax,
				int width, int height, long requestNo, IFractalResultObserver observer, AtomicBoolean cancel) {
			System.out.println("Zapocinjem izracun...");
			int m = 16*16*16;
			int offset = 0;
			short[] data = new short[width * height];
			for(int y = 0; y < height; y++) {
				if(cancel.get()) break;
				for(int x = 0; x < width; x++) {
					double cre = x / (width-1.0) * (reMax - reMin) + reMin;
					double cim = (height-1.0-y) / (height-1) * (imMax - imMin) + imMin;
					Complex zn = new Complex(cre, cim);
					double module = 0;
					int iters = 0;
					Complex znold = new Complex();
					do {
						znold = zn;
						zn = zn.sub(cp.apply(zn).divide(cpd.apply(zn)));
						module = znold.sub(zn).module();
						iters++;
					} while(module > 0.001 && iters < m);
					int index = crp.indexOfClosestRootFor(zn, 0.002);
					data[offset] = (short)(index + 1);
					offset++;
				}	
			}
			System.out.println("Racunanje gotovo. Idem obavijestiti promatraca tj. GUI!");
			observer.acceptResult(data, (short)(cp.order()+1), requestNo);
		}
	}
}
