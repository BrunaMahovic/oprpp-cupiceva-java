package hr.fer.zemris.math;

/**
 * ComplexPolynomial models a polynomial over complex numbers.
 * 
 * @author Bruna
 *
 */
public class ComplexPolynomial {

	Complex[] factors;

	/**
	 * Constructor.
	 * @param factors array of complex numbers
	 */
	public ComplexPolynomial(Complex ...factors) {
		this.factors = factors;
	}
	
	/**
	 * Gets order of this polynomial
	 * @return order of this polynomial
	 */
	public short order() {
		return (short) (factors.length - 1);
	}
	
	/**
	 * Computes a new polynomial by multiplying polynomials.
	 * @param p another polynomial
	 * @return a new polynomial
	 */
	public ComplexPolynomial multiply(ComplexPolynomial p) {
		Complex[] result = new Complex[this.order() + p.order() + 1];
		for (int i = 0; i < this.order() + 1; i++) {
			for (int j = 0; j < p.order() + 1; j++) {
				if (result[i + j] == null)
					result[i + j] = this.factors[i].multiply(p.factors[j]);
				else
					result[i + j] = result[i + j].add(this.factors[i].multiply(p.factors[j]));
			}
		}
		return new ComplexPolynomial(result);
	}
	
	/**
	 * Computes first derivative of this polynomial.
	 * @return derived polynomial
	 */
	public ComplexPolynomial derive() {
		Complex[] result = new Complex[this.order()];
		for (int i = 1; i < this.order() + 1; i++)
			result[i - 1] = this.factors[i].multiply(new Complex(i, 0));
		
		return new ComplexPolynomial(result); 
	}
	
	/**
	 * Computes polynomial value at given point z.
	 * @param z given point
	 * @return polynomial value at given point z
	 */
	public Complex apply(Complex z) {
		Complex result = factors[0];
		for (int i = 1; i <= this.order(); i++)
			result = result.add(this.factors[i].multiply(z.power(i)));
		return result;
	}
	
	@Override
	public String toString() {
		StringBuilder sb = new StringBuilder();
		for (int i = this.order(); i >= 0; i--) 
			sb.append("(" + factors[i].toString() + (i == 0 ? ")" : ")*z^" + i + "+"));
		return sb.toString();
	}
}
