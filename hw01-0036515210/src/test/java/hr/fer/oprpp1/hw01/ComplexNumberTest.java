package hr.fer.oprpp1.hw01;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

import hr.fer.oprpp1.custom.collections.LinkedListIndexedCollection;

public class ComplexNumberTest {

	@Test
	public void testComplexNumberConstructor() {
		
		ComplexNumber cn = new ComplexNumber(2, 3);
		double real = 2;
		
		Assertions.assertEquals(real, cn.getReal());
	}
	
	@Test
	public void testFromReal() {
		
		ComplexNumber cn = new ComplexNumber(0, 0);
		ComplexNumber actual = cn.fromReal(5);
		
		Assertions.assertEquals(5, actual.getReal());
	}
	
	@Test
	public void testFromImaginary() {
		
		ComplexNumber cn = new ComplexNumber(0, 0);
		ComplexNumber actual = cn.fromImaginary(5);
		
		Assertions.assertEquals(5, actual.getImaginary());
	}
	
	@Test
	public void testFromMagnitudeAndAngle() {
		
		ComplexNumber cn = new ComplexNumber(0, 0);
		ComplexNumber actual = cn.fromMagnitudeAndAngle(Math.sqrt(2), 0);
		ComplexNumber expected = new ComplexNumber(Math.sqrt(2), 0);
		
		Assertions.assertEquals(expected.getReal(), actual.getReal());
	}
	
	@Test
	public void testParse() {
		
		ComplexNumber cn = new ComplexNumber(0, 0);
		ComplexNumber actual = cn.parse("-2-3i");
		ComplexNumber expected = new ComplexNumber(-2, -3);
		
		Assertions.assertEquals(expected.getReal(), actual.getReal());
	}
	
	@Test
	public void testGetReal() {
		
		ComplexNumber cn = new ComplexNumber(10, 0);
		
		Assertions.assertEquals(10, cn.getReal());
	}
	
	@Test
	public void testGetImaginary() {
		
		ComplexNumber cn = new ComplexNumber(10, 0);
		
		Assertions.assertEquals(0, cn.getImaginary());
	}
	
	@Test
	public void testGetMagnitude() {
		
		ComplexNumber cn = new ComplexNumber(Math.sqrt(2), 0);
		
		Assertions.assertEquals(Math.sqrt(2), cn.getMagnitude());
	}
	
	@Test
	public void testGetAngle() {
		
		ComplexNumber cn = new ComplexNumber(Math.sqrt(2), 0);
		
		Assertions.assertEquals(0, cn.getAngle());
	}
	
	@Test
	public void testAdd() {
		
		ComplexNumber cn = new ComplexNumber(2, 3).add(new ComplexNumber(4, 1));
		
		Assertions.assertEquals(6, cn.getReal());
	}
	
	@Test
	public void testSub() {
		
		ComplexNumber cn = new ComplexNumber(2, 3).sub(new ComplexNumber(4, 1));
		
		Assertions.assertEquals(-2, cn.getReal());
	}
	
	@Test
	public void testMul() {
		
		ComplexNumber cn = new ComplexNumber(2, 3).mul(new ComplexNumber(4, 1));
		
		Assertions.assertEquals(5, cn.getReal());
	}
	
	@Test
	public void testDiv() {
		
		ComplexNumber cn = new ComplexNumber(2, 3).div(new ComplexNumber(4, 1));
		double expected = 11.0 / 17.0;
		
		Assertions.assertEquals(expected, cn.getReal());
	}
	
	@Test
	public void testPowerException() {
				
		Assertions.assertThrows(IllegalArgumentException.class, () -> new ComplexNumber(2, 2).power(-1));
	}
	
	@Test
	public void testPower() {
		
		ComplexNumber cn = new ComplexNumber(2, 2).power(3);
		
		Assertions.assertEquals(-16, cn.getReal());
	}
	
	@Test
	public void testRootException() {
				
		Assertions.assertThrows(IllegalArgumentException.class, () -> new ComplexNumber(2, 2).root(-1));
	}
	
	/*@Test
	public void testRoot() {
		
		ComplexNumber[] array = new ComplexNumber(2, 2).root(3);
		double actual = array[0].getReal();
		double expected = 0.5 + Math.sqrt(3.0) / 2;
		
		Assertions.assertEquals(expected, actual);
	}*/
	
	@Test
	public void testToString() {
		
		String expected = "2.0-6.0i";
		
		Assertions.assertEquals(expected, new ComplexNumber(2, -6).toString());
	}
}
