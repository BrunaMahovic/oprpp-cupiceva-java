package hr.fer.oprpp1.custom.collections;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

public class ArrayIndexedCollectionTest {

	@Test
	public void testDefaultArrayIndexedCollectionConstructor() {
		
		ArrayIndexedCollection arr = new ArrayIndexedCollection();
		int expectedSize = 0;
		
		Assertions.assertEquals(expectedSize, arr.size());
	}
	
	@Test
	public void testIntArgumentArrayIndexedCollectionConstructor() {
				
		Assertions.assertThrows(IllegalArgumentException.class, () -> new ArrayIndexedCollection(0));
	}
	
	@Test
	public void testCollectionArgumentArrayIndexedCollectionConstructor() {
				
		Assertions.assertThrows(NullPointerException.class, () -> new ArrayIndexedCollection(null));
	}
	
	@Test
	public void testTwoArgumentArrayIndexedCollectionConstructorException() {
		
		ArrayIndexedCollection arr = new ArrayIndexedCollection();
				
		Assertions.assertThrows(IllegalArgumentException.class, () -> new ArrayIndexedCollection(arr, 0));
	}
	
	@Test
	public void testTwoArgumentArrayIndexedCollectionConstructor() {
		
		ArrayIndexedCollection arr = new ArrayIndexedCollection();
		arr.add(5);
		ArrayIndexedCollection coll = new ArrayIndexedCollection(arr, 1);
		Object expected = 5;
		
		Assertions.assertEquals(expected, coll.get(0));
	}
	
	@Test
	public void testAddException() {
		
		Object value = null;
		ArrayIndexedCollection arr = new ArrayIndexedCollection();
				
		Assertions.assertThrows(NullPointerException.class, () -> arr.add(value));
	}
	
	@Test
	public void testAddAndGet() {
		
		ArrayIndexedCollection arr = new ArrayIndexedCollection();
		arr.add(9);
		Object expected = 9;
				
		Assertions.assertEquals(expected, arr.get(0));
	}
	
	@Test
	public void testGetException() {
		
		ArrayIndexedCollection arr = new ArrayIndexedCollection();
				
		Assertions.assertThrows(IndexOutOfBoundsException.class, () -> arr.get(-1));
	}
	
	@Test
	public void testClear() {
		
		ArrayIndexedCollection arr = new ArrayIndexedCollection();
		arr.add(1);
		arr.add(9);
		int expectedSize = 0;
		arr.clear();
		
		Assertions.assertEquals(expectedSize, arr.size());
	}
	
	@Test
	public void testInsertIOOBException() {
		
		ArrayIndexedCollection arr = new ArrayIndexedCollection();
				
		Assertions.assertThrows(IndexOutOfBoundsException.class, () -> arr.insert(9, -1));
	}
	
	@Test
	public void testInsertNPException() {
		
		ArrayIndexedCollection arr = new ArrayIndexedCollection();
				
		Assertions.assertThrows(NullPointerException.class, () -> arr.insert(null, arr.size()));
	}
	
	@Test
	public void testInsert() {
		
		ArrayIndexedCollection arr = new ArrayIndexedCollection();
		arr.insert(5, arr.size());
		Object expected = 5;
				
		Assertions.assertEquals(expected, arr.get(arr.size() - 1));
	}
	
	@Test
	public void testIndexOf() {
		
		ArrayIndexedCollection arr = new ArrayIndexedCollection();
		arr.add(7);
		int expected = 0;
				
		Assertions.assertEquals(expected, arr.indexOf(7));
	}
	
	@Test
	public void testRemoveException() {
		
		ArrayIndexedCollection arr = new ArrayIndexedCollection();
				
		Assertions.assertThrows(IndexOutOfBoundsException.class, () -> arr.remove(-1));
	}
	
	@Test
	public void testRemove() {
		
		ArrayIndexedCollection arr = new ArrayIndexedCollection();
		arr.add(7);
		arr.remove(0);
		int expected = 0;
				
		Assertions.assertEquals(expected, arr.size());
	}
	
	@Test
	public void testIsEmpty() {
		
		ArrayIndexedCollection arr = new ArrayIndexedCollection();
		boolean expected = true;
				
		Assertions.assertEquals(expected, arr.isEmpty());
	}
	
	@Test
	public void testSize() {
		
		ArrayIndexedCollection arr = new ArrayIndexedCollection();
		arr.add(7);
		int expected = 1;
				
		Assertions.assertEquals(expected, arr.size());
	}
	
	@Test
	public void testContainsException() {
		
		ArrayIndexedCollection arr = new ArrayIndexedCollection();
				
		Assertions.assertThrows(NullPointerException.class, () -> arr.contains(null));
	}
	
	@Test
	public void testContains() {
		
		ArrayIndexedCollection arr = new ArrayIndexedCollection();
		arr.add(7);
		boolean expected = true;
				
		Assertions.assertEquals(expected, arr.contains(7));
	}
	
	
	@Test
	public void testRemoveNPException() {
		
		ArrayIndexedCollection arr = new ArrayIndexedCollection();
				
		Assertions.assertThrows(NullPointerException.class, () -> arr.remove(null));
	}
	
	@Test
	public void testRemoveValue() {
		
		ArrayIndexedCollection arr = new ArrayIndexedCollection();
		arr.add("E");
		boolean expected = true;
				
		Assertions.assertEquals(expected, arr.remove("E"));
	}
	
	@Test
	public void testToArray() {
		
		ArrayIndexedCollection arr = new ArrayIndexedCollection();
		arr.add(7);
		Object[] actual = arr.toArray();
		Object[] expected = new Object[1];
		expected[0] = 7;
		
		Assertions.assertArrayEquals(expected, actual, "Fail");
	}
	
	@Test
	public void testAddAllAndForEach() {
		
		ArrayIndexedCollection arr = new ArrayIndexedCollection();
		arr.add(5);
		ArrayIndexedCollection coll = new ArrayIndexedCollection(arr, 1);
		Object expected = 5;
		
		Assertions.assertEquals(expected, coll.get(0));
	}
}
