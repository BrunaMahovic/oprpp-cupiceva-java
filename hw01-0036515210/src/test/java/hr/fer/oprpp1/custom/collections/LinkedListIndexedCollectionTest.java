package hr.fer.oprpp1.custom.collections;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

public class LinkedListIndexedCollectionTest {

	@Test
	public void testDefaultLinkedListIndexedCollectionConstructor() {
		
		LinkedListIndexedCollection arr = new LinkedListIndexedCollection();
		int expectedSize = 0;
		
		Assertions.assertEquals(expectedSize, arr.size());
	}
		
	@Test
	public void testTwoArgumentLinkedListIndexedCollectionConstructor() {
		
		LinkedListIndexedCollection arr = new LinkedListIndexedCollection();
		arr.add(5);
		LinkedListIndexedCollection coll = new LinkedListIndexedCollection(arr);
		Object expected = 5;
		
		Assertions.assertEquals(expected, coll.get(0));
	}
	
	@Test
	public void testAddException() {
		
		Object value = null;
		LinkedListIndexedCollection arr = new LinkedListIndexedCollection();
				
		Assertions.assertThrows(NullPointerException.class, () -> arr.add(value));
	}
	
	@Test
	public void testAddAndGet() {
		
		LinkedListIndexedCollection arr = new LinkedListIndexedCollection();
		arr.add(9);
		Object expected = 9;
				
		Assertions.assertEquals(expected, arr.get(0));
	}
	
	@Test
	public void testGetException() {
		
		LinkedListIndexedCollection arr = new LinkedListIndexedCollection();
				
		Assertions.assertThrows(IndexOutOfBoundsException.class, () -> arr.get(-1));
	}
	
	@Test
	public void testClear() {
		
		LinkedListIndexedCollection arr = new LinkedListIndexedCollection();
		arr.add(1);
		arr.add(9);
		int expectedSize = 0;
		arr.clear();
		
		Assertions.assertEquals(expectedSize, arr.size());
	}
	
	@Test
	public void testInsertIOOBException() {
		
		LinkedListIndexedCollection arr = new LinkedListIndexedCollection();
				
		Assertions.assertThrows(IndexOutOfBoundsException.class, () -> arr.insert(9, -1));
	}
	
	@Test
	public void testInsertNPException() {
		
		LinkedListIndexedCollection arr = new LinkedListIndexedCollection();
				
		Assertions.assertThrows(NullPointerException.class, () -> arr.insert(null, arr.size()));
	}
	
	@Test
	public void testInsert() {
		
		LinkedListIndexedCollection arr = new LinkedListIndexedCollection();
		arr.insert(5, arr.size());
		Object expected = 5;
				
		Assertions.assertEquals(expected, arr.get(arr.size() - 1));
	}
	
	@Test
	public void testIndexOf() {
		
		LinkedListIndexedCollection arr = new LinkedListIndexedCollection();
		arr.add(7);
		int expected = 0;
				
		Assertions.assertEquals(expected, arr.indexOf(7));
	}
	
	@Test
	public void testRemoveException() {
		
		LinkedListIndexedCollection arr = new LinkedListIndexedCollection();
				
		Assertions.assertThrows(IndexOutOfBoundsException.class, () -> arr.remove(-1));
	}
	
	@Test
	public void testRemove() {
		
		LinkedListIndexedCollection arr = new LinkedListIndexedCollection();
		arr.add(7);
		arr.remove(0);
		int expected = 0;
				
		Assertions.assertEquals(expected, arr.size());
	}
	
	@Test
	public void testIsEmpty() {
		
		LinkedListIndexedCollection arr = new LinkedListIndexedCollection();
		boolean expected = true;
				
		Assertions.assertEquals(expected, arr.isEmpty());
	}
	
	@Test
	public void testSize() {
		
		LinkedListIndexedCollection arr = new LinkedListIndexedCollection();
		arr.add(7);
		int expected = 1;
				
		Assertions.assertEquals(expected, arr.size());
	}
	
	@Test
	public void testContainsException() {
		
		LinkedListIndexedCollection arr = new LinkedListIndexedCollection();
				
		Assertions.assertThrows(NullPointerException.class, () -> arr.contains(null));
	}
	
	@Test
	public void testContains() {
		
		LinkedListIndexedCollection arr = new LinkedListIndexedCollection();
		arr.add(7);
		boolean expected = true;
				
		Assertions.assertEquals(expected, arr.contains(7));
	}
	
	
	@Test
	public void testRemoveNPException() {
		
		LinkedListIndexedCollection arr = new LinkedListIndexedCollection();
				
		Assertions.assertThrows(NullPointerException.class, () -> arr.remove(null));
	}
	
	@Test
	public void testRemoveValue() {
		
		LinkedListIndexedCollection arr = new LinkedListIndexedCollection();
		arr.add("E");
		boolean expected = true;
				
		Assertions.assertEquals(expected, arr.remove("E"));
	}
	
	@Test
	public void testToArray() {
		
		LinkedListIndexedCollection arr = new LinkedListIndexedCollection();
		arr.add(7);
		Object[] actual = arr.toArray();
		Object[] expected = new Object[1];
		expected[0] = 7;
		
		Assertions.assertArrayEquals(expected, actual, "Fail");
	}
	
	@Test
	public void testAddAllAndForEach() {
		
		ArrayIndexedCollection arr = new ArrayIndexedCollection();
		arr.add(5);
		ArrayIndexedCollection coll = new ArrayIndexedCollection(arr, 1);
		Object expected = 5;
		
		Assertions.assertEquals(expected, coll.get(0));
	}
}
