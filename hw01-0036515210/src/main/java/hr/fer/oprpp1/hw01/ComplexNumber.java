package hr.fer.oprpp1.hw01;

/**
 * Complex number class. A Complex consists of a real and imaginary part of type double.
 * 
 * @author Bruna
 *
 */
public class ComplexNumber {
	
	private double real;
	private double imaginary;

	/**
     * Constructs a new <code>ComplexNumber</code> object.
     * @param real the real part of the complex number
     * @param imaginary the imaginary part of the complex number
     */
	public ComplexNumber(double real, double imaginary) {
		this.real = real;
        this.imaginary = imaginary;
	}
	
	/**
	 * Constructs a new <code>ComplexNumber</code> from real number
	 * @param real real part of complex number
	 * @return a new <code>ComplexNumber</code> 
	 */
	public static ComplexNumber fromReal(double real) {
		return new ComplexNumber(real, 0);
	}
	
	/**
	 * Constructs a new <code>ComplexNumber</code> from imaginary number
	 * @param imaginary imaginary part of complex number
	 * @return a new <code>ComplexNumber</code>
	 */
	public static ComplexNumber fromImaginary(double imaginary) {
		return new ComplexNumber(0, imaginary);
	}
	
	/**
	 * Constructs a new <code>ComplexNumber</code> from magnitude and angle
	 * @param magnitude magnitude of complex number
	 * @param angle angle of complex number
	 * @return a new <code>ComplexNumber</code>
	 */
	public static ComplexNumber fromMagnitudeAndAngle(double magnitude, double angle) {
    	double r = magnitude * Math.cos(Math.PI / 180 * angle);
    	double i = magnitude * Math.sin(Math.PI / 180 * angle);
    	return new ComplexNumber(r, i);
	}
	
	/**
	 * Constructs a new <code>ComplexNumber</code> represented by the specified String
	 * @param s the string to be parsed
	 * @return  a <code>ComplexNumber</code> represented by the specified String
	 */
	public static ComplexNumber parse(String s) {
        ComplexNumber parsedNumber = null;
        s = s.replaceAll(" ", "");
        
        if (s.contains(String.valueOf("+")) && s.lastIndexOf('+') > 0 || (s.contains(String.valueOf("-")) && s.lastIndexOf('-') > 0)) {
            String r;
            String i;
            s = s.replaceAll("i","");
            
            if(s.lastIndexOf('+') > 0) {
                r = s.substring(0, s.lastIndexOf('+'));
                i = s.substring(s.lastIndexOf('+') + 1, s.length());
                parsedNumber = new ComplexNumber(Double.parseDouble(r), Double.parseDouble(i));
            }
            
            else if(s.lastIndexOf('-') > 0) {
                r = s.substring(0, s.lastIndexOf('-'));
                i = s.substring(s.lastIndexOf('-') + 1, s.length());
                parsedNumber = new ComplexNumber(Double.parseDouble(r), Double.parseDouble(i));
            }
        }
        
        else {
            if(s.endsWith("i")) {
                s = s.replaceAll("i", "");
                parsedNumber = new ComplexNumber(0, Double.parseDouble(s));
            }

            else {
                parsedNumber = new ComplexNumber(Double.parseDouble(s), 0);
            }
        }
        return parsedNumber;
	}
	
	/**
	 * @return real part of complex number
	 */
	public double getReal() {
		return this.real;
	}
	
	/**
	 * @return imaginary part of complex number
	 */
	public double getImaginary() {
		return this.imaginary;
	}
	
	/**
	 * @return magnitude of complex number
	 */
	public double getMagnitude() {
		return Math.sqrt(Math.pow(this.getReal(), 2) + Math.pow(this.getImaginary(), 2));
	}
	
	/**
	 * @return angle of complex number
	 */
	public double getAngle() {
		return Math.atan(this.getImaginary() / this.getReal());
	}
	
	/**
	 * Adds another <code>ComplexNumber</code> to the current complex number.
	 * @param c the complex number to be added to the current complex number
	 * @return a new <code>ComplexNumber</code>
	 */
	public ComplexNumber add(ComplexNumber c) {
		return new ComplexNumber(this.getReal() + c.getReal(), this.getImaginary() + c.getImaginary());
	}
	
	/**
	 * Subtracts another <code>ComplexNumber</code> from the current complex number.
	 * @param c the complex number to be subtracted from the current complex number
	 * @return a new <code>ComplexNumber</code>
	 */
	public ComplexNumber sub(ComplexNumber c) {
		return new ComplexNumber(this.getReal() - c.getReal(), this.getImaginary() - c.getImaginary());
	}
	
	/**
	 * Multiplies another <code>ComplexNumber</code> to the current complex number.
	 * @param c the complex number to be multiplied to the current complex number
	 * @return a new <code>ComplexNumber</code>
	 */
	public ComplexNumber mul(ComplexNumber c) {
		double r = this.getReal() * c.getReal() - this.getImaginary() * c.getImaginary();
        double i = this.getReal() * c.getImaginary() + this.getImaginary() * c.getReal();
        return new ComplexNumber(r, i);
	}
	
	/**
	 * Divides the current <code>ComplexNumber</code> by another <code>ComplexNumber</code>.
	 * @param c the divisor
	 * @return a new <code>ComplexNumber</code>
	 */
	public ComplexNumber div(ComplexNumber c) {
		ComplexNumber first = this.mul(new ComplexNumber(c.getReal(), -1.0 * c.getImaginary()));
		double second = c.getReal() * c.getReal() + c.getImaginary() * c.getImaginary();
		return new ComplexNumber(first.getReal() / second, first.getImaginary() / second);
	}
	
	/**
	 * Calculates the <code>ComplexNumber</code> to the passed integer power.
	 * @param n the power
	 * @return a new <code>ComplexNumber</code>
	 * @throws IllegalArgumentException if n < 0
	 */
	public ComplexNumber power(int n) {
		if (n < 0) throw new IllegalArgumentException();
		ComplexNumber solution = new ComplexNumber(this.getReal(), this.getImaginary());
        for(int j = 1; j < n; j++)
        {
            solution = solution.mul(this);
        }
        return solution;	
	}
	
	/**
	 * Calculates the root of the <code>ComplexNumber</code>
	 * @param n the root
	 * @return an array of <code>ComplexNumber</code>
	 * @throws IllegalArgumentException if n <= 0
	 */
	public ComplexNumber[] root(int n) {
		if (n <= 0) throw new IllegalArgumentException();
		ComplexNumber[] array = new ComplexNumber[n];
	   
    	double R = Math.pow(this.getMagnitude(), 1 / n);
    	double pi = Math.PI;
    	double s = this.getAngle();
        for (int k = 0; k < n; k++) {
    	    array[k] = new ComplexNumber(R * Math.cos((s + 2*k*pi) / n), R * Math.sin((s + 2*k*pi) / n));
        }
	    
	    return array;
	}
	
	/**
	 * @return the complex number in x + yi format
	 */
	public String toString() {
		if (this.getImaginary() > 0) {
			if (this.getReal() != 0)
				return String.valueOf(this.getReal()) + "+" + String.valueOf(this.getImaginary()) + "i";
			else
				return String.valueOf(this.getImaginary()) + "i";
		}
		
		if (this.getImaginary() < 0) {
			if (this.getReal() != 0)
				return String.valueOf(this.getReal()) + "" + String.valueOf(this.getImaginary()) + "i";
			else
				return String.valueOf(this.getImaginary()) + "i";
		}
		
		else {
			return String.valueOf(this.getReal());
		}
	}
}
