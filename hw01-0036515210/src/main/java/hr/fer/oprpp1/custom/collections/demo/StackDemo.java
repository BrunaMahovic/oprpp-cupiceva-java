package hr.fer.oprpp1.custom.collections.demo;

import hr.fer.oprpp1.custom.collections.ObjectStack;

public class StackDemo {

	public static void main(String[] args) {
		
		String[] input = args[0].split(" ");
		
		ObjectStack stack = new ObjectStack();

		for (String i: input) {
			if (isNumeric(i))
				stack.push(i);
			else {
				double second = Double.valueOf(stack.pop().toString());
				double first = Double.valueOf(stack.pop().toString());

				switch(i) {
				case "+":
					stack.push(first + second);
					break;
				case "-":
					stack.push(first - second);
					break;
				case "*":
					stack.push(first * second);
					break;
				case "/":
					stack.push(first / second);
					break;
				case "%":
					stack.push(first % second);
					break;
				}
			}
		}
		
		if (stack.size() != 1) {
			throw new Error();
		}
		
		else {
			System.out.println("Expression evaluates to " + stack.pop().toString() + ".");
		}
	}
	
	public static boolean isNumeric(String str) {
	    if (str == null) {
	        return false;
	    }
	    try {
	        double d = Double.parseDouble(str);
	    } catch (NumberFormatException e) {
	        return false;
	    }
	    return true;
	}
}
