package hr.fer.oprpp1.custom.collections.demo;

import java.util.EmptyStackException;

import hr.fer.oprpp1.custom.collections.ObjectStack;

public class CalcUtil {

	public static String izracunaj(String izraz) {
		String[] input = izraz.split(" ");
		ObjectStack stack = new ObjectStack();
		
		int index = 0;
		for (String i: input) {
			try {
		        double d = Double.parseDouble(i);
		        stack.push(i);
		    } catch (NumberFormatException e) {
		    	Object s = stack.pop();
		    	if (i.equals("#") || i.equals("fact")) {
		    		if (index < input.length - 1 && input[index + 1].equals("+")) {
		    			double factor = Double.valueOf(s.toString());
			    		double second = Double.valueOf(stack.pop().toString());
						double first = Double.valueOf(stack.pop().toString());
						stack.push(factor * first + factor * second);
						break;
		    		}
		    		if (i.equals("fact")) {
						int first = Integer.valueOf(s.toString());
						int rez = 1;
						for (int k = 1; k <= first; k++)
							rez = rez * k;
						stack.push(rez);
						break;
		    		}
		    		else
		    			throw new EmptyStackException();
		    	}
		    	
		    	double second = 0;
		    	double first = 0;
		    	if (!i.equals("#") || !i.equals("fact")) {
		    		second = Double.valueOf(s.toString());
					first = Double.valueOf(stack.pop().toString());
		    	}
		    	else
		    		stack.push(s);
	
				switch(i) {
				case "+":
					stack.push(first + second);
					break;
				case "-":
					stack.push(first - second);
					break;
				case "*":
					stack.push(first * second);
					break;
				case "/":
					stack.push(first / second);
					break;
				case "%":
					stack.push(first % second);
					break;
				case "#":
					stack.push(i);
					break;
				}
		    }
			index++;
		}
		
		try {
			return stack.pop().toString();
		}
		catch (Exception e){
			return null;
		}
	}
}