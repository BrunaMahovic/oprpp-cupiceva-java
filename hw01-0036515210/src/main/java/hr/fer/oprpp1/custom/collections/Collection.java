package hr.fer.oprpp1.custom.collections;

/**
 * Class Collection represents some general collection of objects.
 * 
 * @author Bruna
 *
 */
public class Collection {
	
	/**
	 * The Collection model.
	 */
	protected Collection() {}
	
	/**
	 * Checks if the collection contains objects.
	 * @return true if size() is 0, otherwise false
	 */
	public boolean isEmpty() {
		return size() == 0;
	}
	
	/**
	 * Shows the number of elements in the collection
	 * @return the number of elements in this collection
	 */
	public int size() {
		return 0;
	}
	
	/** 
	 * Ensures that the collection contains the specified element
	 * @param value an element added to the collection
	 */
	public void add(Object value) {}
	
	/**
	 * Checks if this collection contains the specified element
	 * @param value element which is to be tested to see if it is in a collection
	 * @return true if this collection contains the specified element, otherwise false
	 */
	public boolean contains(Object value) {
		return false;
	}
	
	/**
	 * Removes a single instance of the specified element from this collection
	 * @param value element which is to be removed from this collection
	 * @return true if an element was removed, otherwise false
	 */
	public boolean remove(Object value) {
		return false;
	}
	
	/**
	 * Creates an array containing all of the elements in this collection
	 * @return an array containing all of the elements in this collection
	 */
	public Object[] toArray() {
		throw new UnsupportedOperationException();
	}
	
	/**
	 * Performs the given action for each element of the collection
	 * @param processor instance whose action to be performed for each element
	 */
	public void forEach(Processor processor) {}
	
	/**
	 * Adds all of the elements in the specified collection to this collection
	 * @param other collection whose elements are to be added to this collection
	 */
	public void addAll(Collection other) {
		class LocalProcessor extends Processor{
			public void process(Object value) {
				add(value);
			}
		}
		LocalProcessor lp = new LocalProcessor();
		other.forEach(lp);
	}
	
	/**
	 * Removes all of the elements from the collection
	 */
	public void clear() {}
}

