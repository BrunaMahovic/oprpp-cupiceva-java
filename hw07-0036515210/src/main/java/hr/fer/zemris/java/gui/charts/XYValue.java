package hr.fer.zemris.java.gui.charts;

/**
 * Razred definira točku (x,y) na grafu.
 * 
 * @author Bruna
 *
 */
public class XYValue {

	private int x;
	private int y;
	
	/**
	 * Konstruktor.
	 * @param x
	 * @param y
	 */
	public XYValue(int x, int y) {
		this.x = x;
		this.y = y;
	}

	/**
	 * Vraća x vrijednost.
	 * @return
	 */
	public int getX() {
		return x;
	}

	/**
	 * Vraća y vrijednost.
	 * @return
	 */
	public int getY() {
		return y;
	}
}
