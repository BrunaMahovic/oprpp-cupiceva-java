package hr.fer.zemris.java.gui.prim;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Container;
import java.awt.GridLayout;
import java.util.ArrayList;
import java.util.List;

import javax.swing.BorderFactory;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JList;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.ListModel;
import javax.swing.SwingUtilities;
import javax.swing.WindowConstants;
import javax.swing.event.ListDataEvent;
import javax.swing.event.ListDataListener;

/**
 * Demostracijski program s listama prostih brojeva.
 * 
 * @author Bruna
 *
 */
public class PrimDemo extends JFrame {

	private static final long serialVersionUID = 1L;

	public PrimDemo() {
		super();
		setDefaultCloseOperation(WindowConstants.DISPOSE_ON_CLOSE);
		setTitle("PrimNumbers");
		setLocation(20, 20);
		setSize(500, 200);
		initGUI();
	}
	
	private void initGUI() {
		Container cp = getContentPane();
		cp.setLayout(new BorderLayout());
		
		MojModelListe<Integer> modelListe = new MojModelListe<>();
		modelListe.dodajElement(1);

		JList<Integer> lista1 = new JList<>(modelListe);
		JList<Integer> lista2 = new JList<>(modelListe);
		lista1.setBorder(BorderFactory.createLineBorder(Color.BLACK));
		lista2.setBorder(BorderFactory.createLineBorder(Color.BLACK));
		lista2.setSelectionModel(lista1.getSelectionModel());
		
		JPanel p = new JPanel(new GridLayout(1, 0));
		p.add(new JScrollPane(lista1));
		p.add(new JScrollPane(lista2));
		
		cp.add(p, BorderLayout.CENTER);
		
		JButton dodavanje = new JButton("sljedeći");
		cp.add(dodavanje, BorderLayout.PAGE_END);
		PrimListModel model = new PrimListModel();
		
		dodavanje.addActionListener(e->{
			int slucajni = model.next();
			modelListe.dodajElement(slucajni);
		});
	}

	/**
	 * Razred implementira model liste.
	 * 
	 * @author Bruna
	 *
	 * @param <E>
	 */
	private static class MojModelListe<E> implements ListModel<E> {
		private List<ListDataListener> promatraci = new ArrayList<>();
		private List<E> mojiPodatci = new ArrayList<>();
		
		/**
		 * Dodavanje elementa u listu.
		 * @param element
		 */
		public void dodajElement(E element) {
			mojiPodatci.add(element);

			ListDataEvent e = new ListDataEvent(this, ListDataEvent.INTERVAL_ADDED, mojiPodatci.size()-1, mojiPodatci.size()-1);
			for(ListDataListener l : promatraci) {
				l.intervalAdded(e);
			}
		}
		
		@Override
		public int getSize() {
			return mojiPodatci.size();
		}
		
		@Override
		public E getElementAt(int index) {
			return mojiPodatci.get(index);
		}
		
		@Override
		public void addListDataListener(ListDataListener l) {
			promatraci.add(l);
		}
		
		@Override
		public void removeListDataListener(ListDataListener l) {
			promatraci.remove(l);
		}
	}
	
	public static void main(String[] args) {
		SwingUtilities.invokeLater(()->{
			new PrimDemo().setVisible(true);
		});
	}
}






