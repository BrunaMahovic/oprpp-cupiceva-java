package hr.fer.zemris.java.gui.calc;

import java.util.ArrayList;
import java.util.List;
import java.util.function.DoubleBinaryOperator;

import hr.fer.zemris.java.gui.calc.model.*;

/**
 * Razred implementira model jednostavnog kalkulatora.
 * 
 * @author Bruna
 *
 */
public class CalcModelImpl implements CalcModel {

	private List<CalcValueListener> list = new ArrayList<>();
	
	boolean editable = true;
	boolean positive = true;
	String digits = "";
	double decimalValue = 0;
	String frozenValue = null;
	double activeOperand;
	boolean isActiveOperandSet = false;
	DoubleBinaryOperator pendingOperation = null;
	
	@Override
	public void addCalcValueListener(CalcValueListener l) {
		list.add(l);
	}

	@Override
	public void removeCalcValueListener(CalcValueListener l) {
		list.remove(l);
	}

	@Override
	public double getValue() {
		return positive ? decimalValue : -1 * decimalValue;
	}

	@Override
	public void setValue(double value) {
		decimalValue = value < 0 ? -1 * value : value;
		digits = String.valueOf(decimalValue);
		frozenValue = digits;
		positive = value >= 0 ? true : false;
		
		editable = false;
		notifyListeners();
	}

	@Override
	public boolean isEditable() {
		return editable;
	}

	@Override
	public void clear() {
		digits = "";
		editable = true;
		notifyListeners();
	}

	@Override
	public void clearAll() {
		setValue(0);
		clear();
		isActiveOperandSet = false;
		setPendingBinaryOperation(null);		
	}

	@Override
	public void swapSign() throws CalculatorInputException {
		if (!isEditable())
			throw new CalculatorInputException();
		positive = !positive;
		frozenValue = null;
		notifyListeners();
	}

	@Override
	public void insertDecimalPoint() throws CalculatorInputException {
		if (digits.contains(".") || !isEditable() || digits.isBlank())
			throw new CalculatorInputException();
		digits = digits + ".";
		frozenValue = null;
		notifyListeners();
	}

	@Override
	public void insertDigit(int digit) throws CalculatorInputException, IllegalArgumentException {
		if (!isEditable())
			throw new CalculatorInputException();
		if (digits.length() > 307)
			throw new CalculatorInputException();
		digits = digits + digit;
		decimalValue = Double.parseDouble(digits);
		frozenValue = null;
		notifyListeners();
	}

	@Override
	public boolean isActiveOperandSet() {
		return isActiveOperandSet;
	}

	@Override
	public double getActiveOperand() throws IllegalStateException {
		if (!isActiveOperandSet())
			throw new IllegalStateException();
		return activeOperand;
	}

	@Override
	public void setActiveOperand(double activeOperand) {
		this.activeOperand = activeOperand;
		digits = String.valueOf(activeOperand);
		decimalValue = activeOperand;
		isActiveOperandSet = true;
		notifyListeners();
		digits = "";
	}

	@Override
	public void clearActiveOperand() {
		isActiveOperandSet = false; 
		editable = true;
	}

	@Override
	public DoubleBinaryOperator getPendingBinaryOperation() {
		return pendingOperation;
	}

	@Override
	public void setPendingBinaryOperation(DoubleBinaryOperator op) {
		pendingOperation = op;
	}
	
	@Override
	public String toString() {
		if (digits.isBlank())
			return positive ? "0" : "-0";
		
		if(decimalValue == (long) decimalValue)
			return positive ? String.format("%d", (long)decimalValue) : "-" + String.format("%d", (long)decimalValue);
		
		return positive ? String.format("%s", decimalValue) : "-" + String.format("%s", decimalValue);
	}
	
	/**
	 * Metoda javlja promjene promatračima.
	 */
	private void notifyListeners() {
		for (CalcValueListener listener: list)
			listener.valueChanged(this);
	}
}
