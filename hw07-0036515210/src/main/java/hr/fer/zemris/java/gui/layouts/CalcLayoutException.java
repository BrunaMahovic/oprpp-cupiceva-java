package hr.fer.zemris.java.gui.layouts;

/**
 * Iznimka koja je bačena kog nepravilnog rada upravljača razmještaja.
 * 
 * @author Bruna
 *
 */
public class CalcLayoutException extends RuntimeException {

	private static final long serialVersionUID = 1L;
	
	/**
	 * Konstruktor.
	 * @param message
	 */
	public CalcLayoutException(String message) {
		super();
	}
}
