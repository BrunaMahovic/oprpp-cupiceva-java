package hr.fer.zemris.java.gui.charts;

import java.awt.Color;
import java.awt.Font;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.Rectangle;
import java.awt.geom.AffineTransform;
import java.util.List;

import javax.swing.JComponent;

/**
 * Razred definira komponente grafa.
 * 
 * @author Bruna
 *
 */
public class BarChartComponent extends JComponent {

	private static final long serialVersionUID = 1L;
		
    public static final int AXIS_OFFSET = 40;

    BarChart chart;
    private List<XYValue> list;
    private int chartwidth, chartheight, chartX, chartY;
    private int space, max, min;
    private String xLabel, yLabel;

    /**
     * Konstruktor.
     * @param chart
     */
    public BarChartComponent(BarChart chart) {
		this.chart = chart;
		list = chart.getList();
		xLabel = chart.getxDescription();
		yLabel = chart.getyDescription();
		space = chart.getSpace();
		max = chart.getMaxY();
		min = chart.getMinY();
	}

    @Override
    public void paintComponent(Graphics g) {
    	int width = this.getWidth();
        int height = this.getHeight();

        chartwidth = width - 2 * AXIS_OFFSET;
        chartheight = height - 2 * AXIS_OFFSET;

        chartX = AXIS_OFFSET;
        chartY = height - AXIS_OFFSET;


        Graphics2D g2 = (Graphics2D) g;
        drawBars(g2);
        drawAxes(g2);
    }

    /**
     * Crtanje stupaca u grafu.
     * @param g2
     */
    public void drawBars(Graphics2D g2) {

    	Font font = g2.getFont();
    	g2.setFont(font.deriveFont(Font.BOLD));
    	
        Color original = g2.getColor();

        int numBars = list.size();
        int barWidth = (int) ((chartwidth - 10) / numBars);

        int value, height, xLeft = 0, yTopLeft;
        int counter = 0;
        for (XYValue val: list) {
            value = val.getY();

            double height2 = (chartheight - 10) / (max - min) * value;
            height = (int) height2;

            xLeft = AXIS_OFFSET + counter * barWidth + counter * 1;
            yTopLeft = chartY - height;
            Rectangle rec = new Rectangle(xLeft, yTopLeft, barWidth, height);

            g2.setColor(Color.ORANGE);
            g2.fill(rec);
            
            g2.setColor(Color.BLACK);
            g2.drawLine(xLeft + barWidth, chartY, xLeft + barWidth, chartY + 5);
            counter++;
            g2.drawString(String.valueOf(counter), xLeft + barWidth / 2 - 2, chartY + 15); 
        }
        g2.setColor(original);
    }

    /**
     * Crtanje osi grafa.
     * @param g2
     */
    private void drawAxes(Graphics2D g2) {

        int rightX = chartX + chartwidth;
        int topY = chartY - chartheight;

        g2.drawLine(chartX, chartY, rightX, chartY);		// horizontal line
        g2.drawLine(chartX, chartY, chartX, topY);  		// vertical line
        
        Font original = g2.getFont();
        for (int i = 0; i <= (max - min); i = i + space) {		// small horizontal lines on vertical line
        	g2.drawLine(chartX, chartY - (chartheight - 10) / (max - min) * i, chartX - 5, chartY - (chartheight - 10) / (max - min) * i); 
        	g2.setFont(original.deriveFont(Font.BOLD));
        	g2.drawString(String.valueOf(min + i), chartX - (10 + 5 * String.valueOf(min + i).length()), chartY - (chartheight - 10) / (max - min) * i + 3);
        }	
        
        int[] horizontalXPoints = {getWidth() - AXIS_OFFSET, getWidth() - AXIS_OFFSET - 6, getWidth() - AXIS_OFFSET - 6};
        int[] verticalXPoints = {AXIS_OFFSET, AXIS_OFFSET - 3, AXIS_OFFSET + 3};
        int[] horizontalYPoints = {getHeight() - AXIS_OFFSET, getHeight() - AXIS_OFFSET - 3, getHeight() - AXIS_OFFSET + 3};
        int[] verticalYPoints = {AXIS_OFFSET, AXIS_OFFSET + 6, AXIS_OFFSET + 6};
        
        g2.drawPolygon(horizontalXPoints, horizontalYPoints, 3);		// polygon on the end of main lines
        g2.drawPolygon(verticalXPoints, verticalYPoints, 3);
        g2.fillPolygon(horizontalXPoints, horizontalYPoints, 3);
        g2.fillPolygon(verticalXPoints, verticalYPoints, 3);

        g2.setFont(original.deriveFont(Font.PLAIN));
        g2.drawString(xLabel, chartX + chartwidth / 2 - xLabel.length() * 3, chartY + AXIS_OFFSET / 2 + 10) ;		// text on horizontal line

        Font font = new Font(null, original.getStyle(), original.getSize());    
        AffineTransform affineTransform = new AffineTransform();
        affineTransform.rotate(Math.toRadians(-90), 0, 0);
        
        Font rotatedFont = font.deriveFont(affineTransform);
        g2.setFont(rotatedFont.deriveFont(Font.PLAIN));
        g2.drawString(yLabel, AXIS_OFFSET / 2 - 7, chartY - chartheight / 2);		// text on vertical line
        g2.setFont(original);
    }
}
