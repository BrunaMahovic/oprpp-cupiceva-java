package hr.fer.zemris.java.gui.calc;

import java.awt.Color;
import java.awt.Container;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.swing.JButton;
import javax.swing.JCheckBox;
import javax.swing.JFrame;
import javax.swing.SwingConstants;
import javax.swing.SwingUtilities;
import javax.swing.WindowConstants;

import hr.fer.zemris.java.gui.calc.model.CalcModel;
import hr.fer.zemris.java.gui.layouts.*;

/**
 * Program za prikaz jednostavnog modela kalkulatora.
 * 
 * @author Bruna
 *
 */
public class Calculator extends JFrame {

	private static final long serialVersionUID = 1L;

	CalcModel calc = new CalcModelImpl();
	Map<JButton, String[]> map = new HashMap<JButton, String[]>();
	List<Double> list = new ArrayList<>();
	JCheckBox cb = cb("Inv", false);
	
	/**
	 * Konstruktor.
	 */
	public Calculator() {
		setDefaultCloseOperation(WindowConstants.DISPOSE_ON_CLOSE);
		setSize(700, 500);
		initGUI();
	}
	
	public void initGUI() {
		Display dis = new Display();
		Container cp = getContentPane();
		
		cp.setLayout(new CalcLayout(3));
		calc.addCalcValueListener(dis);
		
		cp.add(dis, new RCPosition(1,1));
		cp.add(b("=", a -> {
			showResult();
		}), new RCPosition(1,6));
		cp.add(b("clr", a -> {
			calc.clear();
		}), new RCPosition(1,7));
		cp.add(b("reset", a -> {
			calc.clearAll();
		}), new RCPosition(2,7));
		
		cp.add(b("push", a -> {
			list.add(calc.getValue());
			calc.clear();
		}), new RCPosition(3,7));
		cp.add(b("pop", a -> {
			calc.clear();
			calc.setValue(list.get(list.size() - 1));
			list.remove(list.size() - 1);
		}), new RCPosition(4,7));
		
		cp.add(b("1/x", a -> {
			calc.setPendingBinaryOperation(MathematicalFunctions.divX);
			calc.setActiveOperand(calc.getValue());
		}), new RCPosition(2,1));
		
		JButton button1 = b(cb.isSelected() ? "arcsin" : "sin", a -> {
			calc.setPendingBinaryOperation(cb.isSelected() ? MathematicalFunctions.asin : MathematicalFunctions.sin);
			calc.setActiveOperand(calc.getValue());
		});
		map.put(button1, new String[]{"arcsin", "sin"});
		cp.add(button1, new RCPosition(2,2));
		
		cp.add(b("/", a -> {
			if (calc.isActiveOperandSet()) {
				double result = calc.getPendingBinaryOperation().applyAsDouble(calc.getActiveOperand(), calc.getValue());
				calc.setActiveOperand(result);
				calc.setPendingBinaryOperation(MathematicalFunctions.div);
			}
			else {
				calc.setPendingBinaryOperation(MathematicalFunctions.div);
				calc.setActiveOperand(calc.getValue());
			}
		}), new RCPosition(2,6));
		
		JButton button2 = b(cb.isSelected() ? "10^x" : "log", a -> {
			calc.setPendingBinaryOperation(cb.isSelected() ? MathematicalFunctions.pow10 : MathematicalFunctions.log);
			calc.setActiveOperand(calc.getValue());
		});
		map.put(button2, new String[]{"10^x", "log"});
		cp.add(button2, new RCPosition(3,1));
		
		JButton button3 = b(cb.isSelected() ? "arccos" : "cos", a -> {
			calc.setPendingBinaryOperation(cb.isSelected() ? MathematicalFunctions.acos : MathematicalFunctions.cos);
			calc.setActiveOperand(calc.getValue());
		});
		map.put(button3, new String[]{"arccos", "cos"});
		cp.add(button3, new RCPosition(3,2));
		
		cp.add(b("*", a -> {
			if (calc.isActiveOperandSet()) {
				double result = calc.getPendingBinaryOperation().applyAsDouble(calc.getActiveOperand(), calc.getValue());
				calc.setActiveOperand(result);
				calc.setPendingBinaryOperation(MathematicalFunctions.mul);
			}
			else {
				calc.setPendingBinaryOperation(MathematicalFunctions.mul);
				calc.setActiveOperand(calc.getValue());
			}
		}), new RCPosition(3,6));
		
		JButton button4 = b(cb.isSelected() ? "e^x" : "ln", a -> {
			calc.setPendingBinaryOperation(cb.isSelected() ? MathematicalFunctions.powe : MathematicalFunctions.ln);
			calc.setActiveOperand(calc.getValue());
		});
		map.put(button4, new String[]{"e^x", "ln"});
		cp.add(button4, new RCPosition(4,1));
		
		JButton button5 = b(cb.isSelected() ? "arctan" : "tan", a -> {
			calc.setPendingBinaryOperation(cb.isSelected() ? MathematicalFunctions.atan : MathematicalFunctions.tan);
			calc.setActiveOperand(calc.getValue());
		});
		map.put(button5, new String[]{"arctan", "tan"});
		cp.add(button5, new RCPosition(4,2));
		
		cp.add(b("-", a -> {
			if (calc.isActiveOperandSet()) {
				double result = calc.getPendingBinaryOperation().applyAsDouble(calc.getActiveOperand(), calc.getValue());
				calc.setActiveOperand(result);
				calc.setPendingBinaryOperation(MathematicalFunctions.sub);
			}
			else {
				calc.setPendingBinaryOperation(MathematicalFunctions.sub);
				calc.setActiveOperand(calc.getValue());
			}
		}), new RCPosition(4,6));
		
		JButton button6 = b(cb.isSelected() ? "x^(1/n)" : "x^n", a -> {
			calc.setPendingBinaryOperation(cb.isSelected() ? MathematicalFunctions.sqrt : MathematicalFunctions.pow);
			calc.setActiveOperand(calc.getValue());
		});
		map.put(button6, new String[]{"x^(1/n)", "x^n"});
		cp.add(button6, new RCPosition(5,1));
		
		JButton button7 = b(cb.isSelected() ? "arcctg" : "ctg", a -> {
			calc.setPendingBinaryOperation(cb.isSelected() ? MathematicalFunctions.actg : MathematicalFunctions.ctg);
			calc.setActiveOperand(calc.getValue());
		});
		map.put(button7, new String[]{"arcctg", "ctg"});
		cp.add(button7, new RCPosition(5,2));
		
		cp.add(b("+", a -> {
			if (calc.isActiveOperandSet()) {
				double result = calc.getPendingBinaryOperation().applyAsDouble(calc.getActiveOperand(), calc.getValue());
				calc.setActiveOperand(result);
				calc.setPendingBinaryOperation(MathematicalFunctions.sum);
			}
			else {
				calc.setPendingBinaryOperation(MathematicalFunctions.sum);
				calc.setActiveOperand(calc.getValue());
			}
		}), new RCPosition(5,6));
		
		cp.add(br(0), new RCPosition(5,3));
		
		cp.add(b("+/-", a -> {
			calc.swapSign();
		}), new RCPosition(5,4));
		System.out.println(" ");
		cp.add(b(".", a -> {
			calc.insertDecimalPoint();
		}), new RCPosition(5,5));
		
		cp.add(cb, new RCPosition(5,7));
		
		int c = 1;
		for (int i = 3; i < 6; i++) {
			for (int j = 2; j < 5; j++) {
				cp.add(br(c), new RCPosition(j,i));
				c++;
			}
		}
	}
	
	/**
	 * Metoda implementira značajke checkBoxa.
	 * @param text
	 * @param checked
	 * @return
	 */
	private JCheckBox cb(String text, boolean checked) {
		JCheckBox cb = new JCheckBox("Inv", false);
		cb.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent arg0) {
				for (Map.Entry<JButton, String[]> entry: map.entrySet()) {
					if (cb.isSelected())
						entry.getKey().setText(entry.getValue()[0]);
					else 
						entry.getKey().setText(entry.getValue()[1]);
				}
			}
        });
		return cb;
	}
	
	/**
	 * Metoda implementira značajke funkcijskih gumbi.
	 * @param text
	 * @param listener
	 * @return
	 */
	private JButton b(String text, ActionListener listener) {
		JButton b = new JButton(text);
		b.setBackground(Color.LIGHT_GRAY);
		b.setOpaque(true);
		b.setHorizontalAlignment(SwingConstants.CENTER);
		b.setVerticalAlignment(SwingConstants.CENTER);
		b.setFont(b.getFont().deriveFont(15f));
		b.addActionListener(listener);
		return b;
	}
	
	/**
	 * Metoda implementira značajke gumbi s brojevima.
	 * @param number
	 * @return
	 */
	private JButton br(int number) {
		JButton b = new JButton(String.valueOf(number));
		b.setBackground(Color.LIGHT_GRAY);
		b.setOpaque(true);
		b.setHorizontalAlignment(SwingConstants.CENTER);
		b.setVerticalAlignment(SwingConstants.CENTER);
		b.setFont(b.getFont().deriveFont(30f));
		b.addActionListener(a -> calc.insertDigit(number));
		return b;
	}
	
	private void showResult() {
		double result = calc.getPendingBinaryOperation().applyAsDouble(calc.getActiveOperand(), calc.getValue());
		calc.setValue(result);
		calc.clearActiveOperand();
		calc.setPendingBinaryOperation(null);
	}
	
	public static void main(String[] args) {
		SwingUtilities.invokeLater(()->{
			new Calculator().setVisible(true);
		});
	}
}
