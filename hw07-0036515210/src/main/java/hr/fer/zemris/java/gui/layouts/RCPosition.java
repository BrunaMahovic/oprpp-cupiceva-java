package hr.fer.zemris.java.gui.layouts;

/**
 * Razred implementira neku poziciju.
 * 
 * @author Bruna
 *
 */
public class RCPosition {

	private int row;
	private int column;
	
	/**
	 * Konstruktor.
	 * @param row redak
	 * @param column stupac
	 */
	public RCPosition(int row, int column) {
		this.row = row;
		this.column = column;
		
		if (row < 1 || row > 5)
			throw new CalcLayoutException("Illegal number of rows.");
		if (column < 1 || column > 7)
			throw new CalcLayoutException("Illegal number of columns.");
		if (row == 1 && (column > 1 && column < 6))
			throw new CalcLayoutException("Illegal number of columns.");
	}
	
	/**
	 * Vraća broj stupca.
	 * @return broj retka
	 */
	public int getColumn() {
		return column;
	}

	/**
	 * Vraća broj retka.
	 * @return broj stupca
	 */
	public int getRow() {
		return row;
	}
	
	/**
	 * Parsira string i vraća nađenu poziciju.
	 * @param text tekst
	 * @return poziciju
	 * @throws NumberFormatException
	 */
	public static RCPosition parse(String text) throws NumberFormatException {
		String[] number = text.split(",");
		return new RCPosition(Integer.parseInt(number[0].trim()), Integer.parseInt(number[1].trim()));
	}
}
