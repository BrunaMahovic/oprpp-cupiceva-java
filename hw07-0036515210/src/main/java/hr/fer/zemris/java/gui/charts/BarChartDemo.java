package hr.fer.zemris.java.gui.charts;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Container;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.swing.JFrame;
import javax.swing.SwingUtilities;
import javax.swing.WindowConstants;

/**
 * Demonstracijski program rada grafa.
 * 
 * @author Bruna
 *
 */
public class BarChartDemo extends JFrame {

	private static final long serialVersionUID = 1L;

	public BarChartDemo() {
		setDefaultCloseOperation(WindowConstants.DISPOSE_ON_CLOSE);
		setSize(800, 500);
        setTitle("BarChart");
		try {
			initGUI();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
	
	public void initGUI() throws IOException {
		BufferedReader br = new BufferedReader(new FileReader(new File("primjer.txt"))); 
		
		Container cp = getContentPane();
		cp.setBackground(Color.WHITE);
		
		List<XYValue> list = new ArrayList<>();
		for (String value: br.readLine().split(" ")) {
			String[] val = value.split(",");
			list.add(new XYValue(Integer.valueOf(val[0]), Integer.valueOf(val[1])));
		}
			
        BarChart model = new BarChart(
				list,  
				br.readLine(),  
				br.readLine(),  
				Integer.valueOf(br.readLine()),      // y-os kreće od 0  
				Integer.valueOf(br.readLine()),     // y-os ide do 22  
				Integer.valueOf(br.readLine())
			);
		BarChartComponent component = new BarChartComponent(model);

        this.setLayout(new BorderLayout(2, 2));
        this.add(component, BorderLayout.CENTER);
		cp.add(component);	
	}
	
	public static void main(String[] args) {
		SwingUtilities.invokeLater(()->{
			new BarChartDemo().setVisible(true);
		});
	}
}
