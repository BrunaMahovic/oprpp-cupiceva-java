package hr.fer.zemris.java.gui.calc;

import java.util.function.DoubleBinaryOperator;

/**
 * Razred predstavlja strategiju kod implementiranja matematičkih funkcija.
 * 
 * @author Bruna
 *
 */
public class MathematicalFunctions {

	public static DoubleBinaryOperator sum = (d1, d2) -> d1 + d2;
	public static DoubleBinaryOperator mul = (d1, d2) -> d1 * d2;
	public static DoubleBinaryOperator div = (d1, d2) -> d1 / d2;
	public static DoubleBinaryOperator sub = (d1, d2) -> d1 - d2;
	
	public static DoubleBinaryOperator sin = (d1, d2) -> Math.sin(d1);
	public static DoubleBinaryOperator log = (d1, d2) -> Math.log10(d1);
	public static DoubleBinaryOperator cos = (d1, d2) -> Math.cos(d1);
	public static DoubleBinaryOperator ln = (d1, d2) -> Math.log(d1);
	public static DoubleBinaryOperator tan = (d1, d2) -> Math.tan(d1);
	public static DoubleBinaryOperator ctg = (d1, d2) -> 1 / Math.tan(d1);
	public static DoubleBinaryOperator divX = (d1, d2) -> 1 / d1;
	public static DoubleBinaryOperator pow = (d1, d2) -> Math.pow(d1, d2);
	
	public static DoubleBinaryOperator asin = (d1, d2) -> Math.asin(d1);
	public static DoubleBinaryOperator pow10 = (d1, d2) -> Math.pow(10, d1);
	public static DoubleBinaryOperator acos = (d1, d2) -> Math.acos(d1);
	public static DoubleBinaryOperator powe = (d1, d2) -> Math.pow(Math.E, d1);
	public static DoubleBinaryOperator atan = (d1, d2) -> Math.atan(d1);
	public static DoubleBinaryOperator actg = (d1, d2) -> 1 / Math.atan(d1);
	public static DoubleBinaryOperator sqrt = (d1, d2) -> Math.pow(d1, 1 / d2);
}
