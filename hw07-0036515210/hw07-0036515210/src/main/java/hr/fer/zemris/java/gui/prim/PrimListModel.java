package hr.fer.zemris.java.gui.prim;

/**
 * Razred implementira računanje prostih brojeva.
 * 
 * 
 * @author Bruna
 *
 */
public class PrimListModel {

	int cnt = 1;
	
	/**
	 * Vraća sljedeći prosti broj u nizu.
	 * @return
	 */
	public int next() {
		int num = 0;
		for (int i = cnt; ; i++) {
			int counter = 0; 		  
	        for(num = i; num >= 1; num--) {
	        	if(i % num == 0)
	        		counter++;
	        }
	        if (counter == 2) {
	        	cnt = i + 1;
	        	return i;
	        } 	
		}
	}
}
