package hr.fer.zemris.java.gui.charts;

import java.util.ArrayList;
import java.util.List;

/**
 * Razred definira osnovne značajke grafa.
 * 
 * @author Bruna
 *
 */
public class BarChart {

	private List<XYValue> list = new ArrayList<>();
	private String xDescription;
	private String yDescription;
	private int minY;
	private int maxY;
	private int space;
	
	/**
	 * Konstruktor.
	 * @param list
	 * @param xDescription
	 * @param yDescription
	 * @param minY
	 * @param maxY
	 * @param space
	 */
	public BarChart(List<XYValue> list, String xDescription, String yDescription, int minY, int maxY, int space) {
		this.list = list;
		this.xDescription = xDescription;
		this.yDescription = yDescription;
		this.minY = minY;
		this.maxY = maxY;
		this.space = space;
		
		if (minY < 0 || maxY <= minY)
			throw new IllegalArgumentException();
		
		while (this.maxY % this.space != 0)
			this.maxY++;
		
		for (XYValue value: list) {
			if (value.getY() < minY)
				throw new IllegalArgumentException();
		}
	}

	/**
	 * Vraća listu vrijednosti.
	 * @return listu
	 */
	public List<XYValue> getList() {
		return list;
	}

	/**
	 * Vraća opis osi x.
	 * @return opis
	 */
	public String getxDescription() {
		return xDescription;
	}

	/**
	 * Vraća opis osi y.
	 * @return opis
	 */
	public String getyDescription() {
		return yDescription;
	}

	/**
	 * Vraća minimalnu vrijednost y-osi.
	 * @return 
	 */
	public int getMinY() {
		return minY;
	}

	/**
	 * Vraća maksimalnu vrijednost y-osi.
	 * @return
	 */
	public int getMaxY() {
		return maxY;
	}

	/**
	 * Vraća razmak na y-osi.
	 * @return
	 */
	public int getSpace() {
		return space;
	}
}
