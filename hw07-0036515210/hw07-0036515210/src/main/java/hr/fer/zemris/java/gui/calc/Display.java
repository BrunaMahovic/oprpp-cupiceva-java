package hr.fer.zemris.java.gui.calc;

import java.awt.Color;

import javax.swing.JLabel;
import javax.swing.SwingConstants;

import hr.fer.zemris.java.gui.calc.model.*;

/**
 * Razred implementira značajke prostora u kojem se prikazuje ispis kalkulatora.
 * @author Bruna
 *
 */
public class Display extends JLabel implements CalcValueListener{

	private static final long serialVersionUID = 1L;
	
	/**
	 * Konstruktor.
	 */
	public Display() {
		setBackground(Color.YELLOW);
		setOpaque(true);
		setHorizontalAlignment(SwingConstants.RIGHT);
		setVerticalAlignment(SwingConstants.CENTER);
		setFont(this.getFont().deriveFont(20f));
	}
	
	@Override
	public void valueChanged(CalcModel model) {
		setText(model.toString());
	}

}
