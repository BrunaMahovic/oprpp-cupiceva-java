package hr.fer.zemris.java.gui.layouts;

import java.awt.Component;
import java.awt.Container;
import java.awt.Dimension;
import java.awt.LayoutManager2;
import java.util.HashMap;
import java.util.Map;

/**
 * Razred predstavlja upravljač razmještaja.
 * 
 * @author Bruna
 *
 */
public class CalcLayout implements LayoutManager2 {
	
	public int space;
	Map<Component, RCPosition> map = new HashMap<Component, RCPosition>();
	
	/**
	 * Konstruktor.
	 */
	public CalcLayout() {
		this(0);
	}
	
	/**
	 * Konstruktor.
	 * @param space razmak među komponentama
	 */
	public CalcLayout(int space) {
		this.space = space;
	}

	/**
	 * @throws UnsupportedOperationException
	 */
	@Override
	public void addLayoutComponent(String arg0, Component arg1) {
		throw new UnsupportedOperationException();	
	}

	/**
	 * Radi razmještaj komponenata.
	 */
	@Override
	public void layoutContainer(Container arg0) {
		Component comp;
		for (Map.Entry<Component, RCPosition> e: map.entrySet()) {
	        comp = e.getKey();
	        
	        int col = e.getValue().getColumn();
	        int row = e.getValue().getRow();
	        
	        int width = (arg0.getWidth() - 6 * space) / 7;
	        int height = (arg0.getHeight() - 4 * space) / 5;
	        if (col == 1 && row == 1)
	        	width = width * 5 + 4 * space;
	        
			int x = (col - 1) * width + (col - 1) * space;
			int y = (row - 1) * height + (row - 1) * space;
			comp.setBounds(x, y, width, height);
		}
	}

	/**
	 * Vraća minimalnu dimenziju kontejnera.
	 */
	@Override
	public Dimension minimumLayoutSize(Container arg0) {
		return getDimension(arg0, 0);
	}

	/**
	 * Vraća preferiranu dimenziju kontejnera.
	 */
	@Override
	public Dimension preferredLayoutSize(Container arg0) {
		return getDimension(arg0, 1);
	}

	/**
	 * Briše komponentu.
	 */
	@Override
	public void removeLayoutComponent(Component arg0) {
		map.remove(arg0);
	}

	/**
	 * Dodaje komponentu.
	 */
	@Override
	public void addLayoutComponent(Component arg0, Object arg1) {
		if (arg0 == null || arg1 == null)
			throw new NullPointerException();
		
		if (!(arg1 instanceof RCPosition) && !(arg1 instanceof String))
			throw new IllegalArgumentException();
		
		if (arg1 instanceof String) {
			try {
				arg1 = RCPosition.parse(arg1.toString());
			}
			catch (Exception e) {
				throw new IllegalArgumentException();
			}
		}
		
		if (map.containsValue((RCPosition) arg1))
			throw new CalcLayoutException("There is already a component associated with the same constraint.");
		
		map.put(arg0, (RCPosition) arg1);
	}

	/**
	 * Vraća 0.
	 */
	@Override
	public float getLayoutAlignmentX(Container arg0) {
		return 0;
	}

	/**
	 * Vraća 0.
	 */
	@Override
	public float getLayoutAlignmentY(Container arg0) {
		return 0;
	}

	/**
	 * Prazna metoda.
	 */
	@Override
	public void invalidateLayout(Container arg0) {
		
	}

	/**
	 * Vraća maksimalnu dimenziju kontejnera.
	 */
	@Override
	public Dimension maximumLayoutSize(Container arg0) {
		return getDimension(arg0, 2);
	}

	/**
	 * Računa dimenziju.
	 * @param arg0 kontejner
	 * @param size veličina
	 * @return vraća dimenziju kontejnera
	 */
	public Dimension getDimension(Container arg0, int size) {
		int w = 0;
	    int h = 0;
	    Component comp;
	    Dimension d;
		for (Map.Entry<Component, RCPosition> e: map.entrySet()) {
	        comp = e.getKey();
	        switch (size) {
	        case 0:
	        	d = comp.getMinimumSize();
	        	break;
	        case 1:
	        	d = comp.getPreferredSize();
	        	break;
	        case 2:
	        	d = comp.getMaximumSize();
	        	break;
	        default:
	        	d = null;
	        }
	        
	        if (w < d.width) {
	            w = d.width;
	        }
	        if (h < d.height) {
	            h = d.height;
	        }
	    }
		
		return new Dimension(7 * w + 6 * space, 5 * h + 4 * space);
	}
}
