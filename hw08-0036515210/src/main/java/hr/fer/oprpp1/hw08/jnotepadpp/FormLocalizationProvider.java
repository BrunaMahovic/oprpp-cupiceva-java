package hr.fer.oprpp1.hw08.jnotepadpp;

import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;

import javax.swing.JFrame;

/**
 * Class FormLocalizationProvider registers itself in constructor as a WindowListener to its JFrame.
 * 
 * @author Bruna
 *
 */
public class FormLocalizationProvider extends LocalizationProviderBridge {
	
	JFrame frame;

	/**
	 * Constructor.
	 * @param provider localization provider
	 * @param frame frame
	 */
	public FormLocalizationProvider(ILocalizationProvider provider, JFrame frame) {
		super(provider);
		this.frame = frame;
		this.frame.addWindowListener(new WindowAdapter() {
			@Override
			public void windowOpened(WindowEvent arg0) {
				connect();
			}
			@Override
			public void windowClosed(WindowEvent arg0) {
				disconnect();
			}
		});
	}

}
