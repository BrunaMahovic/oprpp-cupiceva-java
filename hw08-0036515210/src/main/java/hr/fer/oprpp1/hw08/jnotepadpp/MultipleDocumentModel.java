package hr.fer.oprpp1.hw08.jnotepadpp;

import java.nio.file.Path;

/**
 * Interface MultipleDocumentModel represents a model capable of holding zero, one or more documents and having a concept of current document.
 * 
 * @author Bruna
 *
 */
public interface MultipleDocumentModel extends Iterable<SingleDocumentModel> {
	
	/**
	 * Creates new document.
	 * @return created document
	 */
	SingleDocumentModel createNewDocument();
	
	/**
	 * Gets current document.
	 * @return current document
	 */
	SingleDocumentModel getCurrentDocument();
	
	/**
	 * Loads document.
	 * @param path given path
	 * @return loaded document
	 */
	SingleDocumentModel loadDocument(Path path);
	
	/**
	 * Saves document on given path.
	 * @param model document
	 * @param newPath new path
	 */
	void saveDocument(SingleDocumentModel model, Path newPath);
	
	/**
	 * Closes document.
	 * @param model document
	 */
	void closeDocument(SingleDocumentModel model);
	
	/**
	 * Adds new listener.
	 * @param l new listener
	 */
	void addMultipleDocumentListener(MultipleDocumentListener l);
	
	/**
	 * Removes given listener.
	 * @param l listener
	 */
	void removeMultipleDocumentListener(MultipleDocumentListener l);
	
	/**
	 * Gets number of documents.
	 * @return number of documents
	 */
	int getNumberOfDocuments();
	
	/**
	 * Gets document from given index.
	 * @param index index
	 * @return document
	 */
	SingleDocumentModel getDocument(int index);

}
