package hr.fer.oprpp1.hw08.jnotepadpp;

/**
 * Interface ILocalizationListener models a listener.
 * 
 * @author Bruna
 *
 */
public interface ILocalizationListener {

	/**
	 * Method which is called by the Subject when the selected language changes.
	 */
	void localizationChanged();
}
