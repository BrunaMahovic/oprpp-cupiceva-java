package hr.fer.oprpp1.hw08.jnotepadpp;

/**
 * Interface MultipleDocumentListener models a listener for documents.
 * 
 * @author Bruna
 *
 */
public interface MultipleDocumentListener {

	/**
	 * Method defines listener for document that is changed.
	 * @param previousModel previous model
	 * @param currentModel current model
	 */
	void currentDocumentChanged(SingleDocumentModel previousModel, SingleDocumentModel currentModel);
	
	/**
	 * Adds new document.
	 * @param model model
	 */
	void documentAdded(SingleDocumentModel model);
	
	/**
	 * Removes document.
	 * @param model model
	 */
	void documentRemoved(SingleDocumentModel model);
}
