package hr.fer.oprpp1.hw08.jnotepadpp;

import java.nio.file.Path;
import java.util.ArrayList;
import java.util.List;

import javax.swing.JTextArea;
import javax.swing.event.DocumentEvent;
import javax.swing.event.DocumentListener;

/**
 * Class DefaultSingleDocumentModel is implementation of SingleDocumentModel.
 * 
 * @author Bruna
 *
 */
public class DefaultSingleDocumentModel implements SingleDocumentModel {
	
	private Path path;
	private String content;
	private JTextArea textArea;
	private boolean modified;
	private List<SingleDocumentListener> list = new ArrayList<>();
	
	/**
	 * Constructor.
	 * @param path path
	 * @param content content
	 */
	public DefaultSingleDocumentModel(Path path, String content) {
		this.path = path;
		this.content = content;
		textArea = new JTextArea(content);
		textArea.getDocument().addDocumentListener(new DocumentListener() {
			
			@Override
			public void removeUpdate(DocumentEvent arg0) {
				setModified(true);
			}
			
			@Override
			public void insertUpdate(DocumentEvent arg0) {
				setModified(true);
			}
			
			@Override
			public void changedUpdate(DocumentEvent arg0) {
				setModified(true);
			}
		});
	}

	@Override
	public JTextArea getTextComponent() {
		return this.textArea;
	}

	@Override
	public Path getFilePath() {
		return this.path;
	}

	@Override
	public void setFilePath(Path path) {
		if (path == null)
			throw new NullPointerException();
		this.path = path;
	}

	@Override
	public boolean isModified() {
		return modified;
	}

	@Override
	public void setModified(boolean modified) {
		if (this.modified != modified) {
			this.modified = modified;
			notifyListeners();			
		}
	}

	@Override
	public void addSingleDocumentListener(SingleDocumentListener l) {
		list.add(l);
	}

	@Override
	public void removeSingleDocumentListener(SingleDocumentListener l) {
		list.remove(l);
	}

	/**
	 * Notifies all listeners.
	 */
	private void notifyListeners() {
		for (SingleDocumentListener listener: list) {
			listener.documentModifyStatusUpdated(this);
		}
	}
	
	/**
	 * Gets content.
	 * @return content
	 */
	public String getContent() {
		return this.content;
	}
}
