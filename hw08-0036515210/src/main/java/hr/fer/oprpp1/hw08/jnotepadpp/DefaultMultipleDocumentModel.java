package hr.fer.oprpp1.hw08.jnotepadpp;

import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import javax.swing.ImageIcon;
import javax.swing.JScrollPane;
import javax.swing.JTabbedPane;

/**
 * Class DefaultMultipleDocumentModel has a collection of SingleDocumentModel objects, a reference to current SingleDocumentModel and a support for listeners management.
 * 
 * @author Bruna
 *
 */
public class DefaultMultipleDocumentModel extends JTabbedPane implements MultipleDocumentModel {
	
	private static final long serialVersionUID = 1L;
	
	List<SingleDocumentModel> list = new ArrayList<>();
	List<MultipleDocumentListener> listListeners = new ArrayList<>();
	SingleDocumentModel currentDocumentModel;
	private int index = 0;

	@Override
	public Iterator<SingleDocumentModel> iterator() {
		Iterator<SingleDocumentModel> iterator = new Iterator<SingleDocumentModel>() {
			
			@Override
			public SingleDocumentModel next() {
				return list.get(index++);
			}
			
			@Override
			public boolean hasNext() {
				if (index <= list.size() - 1)
					return true;
				return false;
			}
		};
		return iterator;
	}

	@Override
	public SingleDocumentModel createNewDocument() {
		this.currentDocumentModel = new DefaultSingleDocumentModel(null, "");;
		list.add(currentDocumentModel);
		this.addTab("(unnamed)", this.getRedIcon(), new JScrollPane(this.currentDocumentModel.getTextComponent()));
		this.currentDocumentModel.addSingleDocumentListener(new SingleDocumentListener() {
			
			@Override
			public void documentModifyStatusUpdated(SingleDocumentModel model) {
				if (model.isModified())
					setIconAt(list.indexOf(model), getRedIcon());
				else 
					setIconAt(list.indexOf(model), getGreenIcon());
			}
			
			@Override
			public void documentFilePathUpdated(SingleDocumentModel model) {
				// TODO Auto-generated method stub
				
			}
		});
		this.setSelectedIndex(list.indexOf(this.currentDocumentModel));
		return this.currentDocumentModel;
	}

	@Override
	public SingleDocumentModel getCurrentDocument() {
		int num = this.getSelectedIndex();
		return this.list.get(num);
	}

	@Override
	public SingleDocumentModel loadDocument(Path path) {
		if (path == null)
			throw new NullPointerException();
		
		Stream<String> lines;
		String data = "";
		try {
			lines = Files.lines(path);
			data = lines.collect(Collectors.joining("\n"));
		    lines.close();
		} catch (IOException e) {
			e.printStackTrace();
		}
	    
		this.currentDocumentModel = new DefaultSingleDocumentModel(path, data);
		
		if (!list.contains(currentDocumentModel))
			list.add(currentDocumentModel);
		this.addTab(new File(path.toString()).getName(), this.getGreenIcon(), this.currentDocumentModel.getTextComponent());
		this.currentDocumentModel.addSingleDocumentListener(new SingleDocumentListener() {
			
			@Override
			public void documentModifyStatusUpdated(SingleDocumentModel model) {
				if (model.isModified())
					setIconAt(list.indexOf(model), getRedIcon());
				else 
					setIconAt(list.indexOf(model), getGreenIcon());
			}
			
			@Override
			public void documentFilePathUpdated(SingleDocumentModel model) {
				// TODO Auto-generated method stub
				
			}
		});
		this.setSelectedIndex(list.indexOf(this.currentDocumentModel));
		return this.currentDocumentModel;
	}

	@Override
	public void saveDocument(SingleDocumentModel model, Path newPath) {
		this.currentDocumentModel.setModified(false);
		model.setFilePath(newPath);
		this.setTitleAt(list.indexOf(model), newPath.toFile().getName());
	}

	@Override
	public void closeDocument(SingleDocumentModel model) {
//		this.removeTabAt(list.indexOf(model));
		this.remove(list.indexOf(model));
		list.remove(model);
		if (getNumberOfDocuments() > 1)
			this.currentDocumentModel = getDocument(getNumberOfDocuments() - 1);
		if (getNumberOfDocuments() == 0)
			System.exit(0);
		
	}

	@Override
	public void addMultipleDocumentListener(MultipleDocumentListener l) {
		listListeners.add(l);
	}

	@Override
	public void removeMultipleDocumentListener(MultipleDocumentListener l) {
		listListeners.remove(l);
	}

	@Override
	public int getNumberOfDocuments() {
		return list.size();
	}

	@Override
	public SingleDocumentModel getDocument(int index) {
		return list.get(index);
	}
	
	/**
	 * Gets red icon.
	 * @return red icon
	 */
	private ImageIcon getRedIcon() {
		InputStream is = this.getClass().getResourceAsStream("/images/red-icon.png");
		if (is==null) 
			throw new Error();
		byte[] bytes = null;
		try {
			bytes = is.readAllBytes();
			is.close();
		} catch (IOException e) {
			e.printStackTrace();
		}
		return new ImageIcon(bytes);
	}
	
	/**
	 * Gets green icon.
	 * @return green icon
	 */
	private ImageIcon getGreenIcon() {
		InputStream is = this.getClass().getResourceAsStream("/images/green-icon.png");
		if (is==null) 
			throw new Error();
		byte[] bytes = null;
		try {
			bytes = is.readAllBytes();
			is.close();
		} catch (IOException e) {
			e.printStackTrace();
		}
		return new ImageIcon(bytes);
	}

}
