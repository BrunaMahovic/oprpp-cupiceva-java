package hr.fer.oprpp1.hw08.jnotepadpp;

/**
 * Interface ILocalizationProvider declares a method for registration and a method for de-registration of listeners.
 * 
 * @author Bruna
 *
 */
public interface ILocalizationProvider {

	/**
	 * Method takes a key and gives back the localization.
	 * @param key given key
	 * @return localization
	 */
	String getString(String key);
	
	/**
	 * Registers listeners.
	 * @param listener new listener
	 */
	void addLocalizationListener(ILocalizationListener listener);
	
	/**
	 * De-registers listeners.
	 * @param listener listener
	 */
	void removeLocalizationListener(ILocalizationListener listener);
}
