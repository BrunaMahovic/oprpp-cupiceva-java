package hr.fer.oprpp1.hw08.jnotepadpp;

import java.util.ArrayList;
import java.util.List;

/**
 * Abstract class AbstractLocalizationProvider adds it the ability to register, de-register and inform listeners.
 * 
 * @author Bruna
 *
 */
public abstract class AbstractLocalizationProvider implements ILocalizationProvider {
	
	List<ILocalizationListener> list = new ArrayList<>();

	
	public void addLocalizationListener(ILocalizationListener listener) {
		list.add(listener);
	}
	
	public void removeLocalizationListener(ILocalizationListener listener) {
		list.remove(listener);
	}
	
	/**
	 * Inform listeners.
	 */
	public void fire() {
		for (ILocalizationListener listener: list)
			listener.localizationChanged();
	}
}
