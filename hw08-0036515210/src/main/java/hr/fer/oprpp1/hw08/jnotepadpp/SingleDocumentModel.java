package hr.fer.oprpp1.hw08.jnotepadpp;

import java.nio.file.Path;

import javax.swing.JTextArea;

/**
 * Interface SingleDocumentModel represents a model of single document, having information about file path from which document was loaded, document modification status and reference to Swing component which is used for editing.
 * 
 * @author Bruna
 *
 */
public interface SingleDocumentModel {

	/**
	 * Gets text component.
	 * @return text component
	 */
	JTextArea getTextComponent();
	
	/**
	 * Gets file path.
	 * @return file path
	 */
	Path getFilePath();
	
	/**
	 * Sets file path.
	 * @param path file path
	 */
	void setFilePath(Path path);
	
	/**
	 * Checks if document is modified.
	 * @return true if modified, otherwise false
	 */
	boolean isModified();
	
	/**
	 * Sets modified value.
	 * @param modified new value.
	 */
	void setModified(boolean modified);
	
	/**
	 * Adds new listener.
	 * @param l new listener
	 */
	void addSingleDocumentListener(SingleDocumentListener l);
	
	/**
	 * Removes given listener.
	 * @param l listener.
	 */
	void removeSingleDocumentListener(SingleDocumentListener l);
}
