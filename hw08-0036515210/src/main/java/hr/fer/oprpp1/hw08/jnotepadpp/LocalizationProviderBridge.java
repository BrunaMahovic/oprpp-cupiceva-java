package hr.fer.oprpp1.hw08.jnotepadpp;

/**
 * Class LocalizationProviderBridge is a decorator for some other IlocalizationProvider.
 * 
 * @author Bruna
 *
 */
public class LocalizationProviderBridge extends AbstractLocalizationProvider implements ILocalizationListener {
	
	ILocalizationProvider provider;
	private boolean connected;

	/**
	 * Constructor.
	 * @param provider localization provider
	 */
	public LocalizationProviderBridge(ILocalizationProvider provider) {
		this.provider = provider;
	}
	
	/**
	 * Registers an instance of anonymous ILocalizationListener on the decorated object.
	 */
	public void connect() {
		if (!connected) {
			connected = true;
			provider.addLocalizationListener(this);
		}
	}
	
	/**
	 * De-registers from decorated object
	 */
	public void disconnect() {
		if (connected) {
			connected = false;
			provider.removeLocalizationListener(this);
		}
	}
	
	@Override
	public String getString(String key) {
		return provider.getString(key);
	}

	@Override
	public void localizationChanged() {
		fire();
	}

}
