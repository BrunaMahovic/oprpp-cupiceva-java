package hr.fer.oprpp1.hw08.jnotepadpp;

import java.awt.event.ActionEvent;

import javax.swing.AbstractAction;
import javax.swing.Action;
import javax.swing.KeyStroke;

/**
 * Class LocalizableAction registers itself on provider in constructor and when it receives a notification, it translates the key again and sets the translation as a new text that it displays.
 * 
 * @author Bruna
 *
 */
public class LocalizableAction extends AbstractAction {
	
	private static final long serialVersionUID = 1L;

	/**
	 * Constructor.
	 * @param key key
	 * @param lp localization provider
	 * @param accKey accelerator key
	 * @param vkN mnemonic key
	 * @param description description
	 */
	public LocalizableAction(String key, ILocalizationProvider lp, String accKey, int vkN, String description) {
		this.putValue(Action.NAME, lp.getString(key));
		this.putValue(Action.ACCELERATOR_KEY, KeyStroke.getKeyStroke(accKey));
		this.putValue(Action.MNEMONIC_KEY, vkN);
		this.putValue(Action.SHORT_DESCRIPTION, description);
		
		lp.addLocalizationListener(new ILocalizationListener() {
			
			@Override
			public void localizationChanged() {
				putValue(Action.NAME, lp.getString(key));
				putValue(Action.ACCELERATOR_KEY, KeyStroke.getKeyStroke(accKey));
				putValue(Action.MNEMONIC_KEY, vkN);
				putValue(Action.SHORT_DESCRIPTION, description);
			}
		});
	}
	
	/**
	 * Constructor.
	 * @param key key
	 * @param lp localization provider
	 */
	public LocalizableAction(String key, ILocalizationProvider lp) {
		this.putValue(Action.NAME, lp.getString(key));
		
		lp.addLocalizationListener(new ILocalizationListener() {
			
			@Override
			public void localizationChanged() {
				putValue(Action.NAME, lp.getString(key));
			}
		});
	}

	@Override
	public void actionPerformed(ActionEvent arg0) {
		// TODO Auto-generated method stub
		
	}

}
