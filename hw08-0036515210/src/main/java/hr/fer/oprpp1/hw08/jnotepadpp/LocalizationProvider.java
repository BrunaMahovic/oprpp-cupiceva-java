package hr.fer.oprpp1.hw08.jnotepadpp;

import java.util.Locale;
import java.util.ResourceBundle;

/**
 * Class LocalizationProvider is singleton which sets the language, loads the resource bundle for this language and stores reference to it.
 * 
 * @author Bruna
 *
 */
public class LocalizationProvider extends AbstractLocalizationProvider {

	private static final LocalizationProvider instance = new LocalizationProvider();
	private String language;
	public ResourceBundle bundle;
	
	/**
	 * Contructor.
	 */
	private LocalizationProvider() {
		this.language = "en";
		Locale locale = Locale.forLanguageTag(language);
		this.bundle = ResourceBundle.getBundle("hr.fer.oprpp1.hw08.jnotepadpp.prijevodi", locale);
	}    
	
	/**
	 * Gets instance.
	 * @return instance
	 */
	public static LocalizationProvider getInstance() {
		return instance;    
	}

	@Override
	public String getString(String key) {
		return this.bundle.getString(key);
	}
	
	/**
	 * Gets language.
	 * @return language
	 */
	public String getLanguage() {
		return this.language;    
	}
	
	/**
	 * Sets language.
	 * @param language new language
	 */
	public void setLanguage(String language) {
		this.language = language;
		Locale locale = Locale.forLanguageTag(language);
		this.bundle = ResourceBundle.getBundle("hr.fer.oprpp1.hw08.jnotepadpp.prijevodi", locale);
		fire();
	}
}
