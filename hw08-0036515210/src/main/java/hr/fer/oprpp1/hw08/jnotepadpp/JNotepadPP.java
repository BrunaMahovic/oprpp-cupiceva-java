package hr.fer.oprpp1.hw08.jnotepadpp;

import java.awt.BorderLayout;
import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyEvent;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.io.File;
import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Path;
import java.text.Collator;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Date;
import java.util.List;
import java.util.Locale;

import javax.swing.Action;
import javax.swing.JButton;
import javax.swing.JFileChooser;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JMenu;
import javax.swing.JMenuBar;
import javax.swing.JMenuItem;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JTextArea;
import javax.swing.JToolBar;
import javax.swing.SwingConstants;
import javax.swing.SwingUtilities;
import javax.swing.Timer;
import javax.swing.WindowConstants;
import javax.swing.event.CaretEvent;
import javax.swing.event.CaretListener;
import javax.swing.event.ChangeEvent;
import javax.swing.event.ChangeListener;
import javax.swing.text.BadLocationException;
import javax.swing.text.Document;

/**
 * Main class.
 * 
 * @author Bruna
 *
 */
public class JNotepadPP extends JFrame {

	private static final long serialVersionUID = 1L;
	DefaultMultipleDocumentModel tabbedPanel = new DefaultMultipleDocumentModel();	
	SingleDocumentModel tab = new DefaultSingleDocumentModel(null, "");
	private FormLocalizationProvider flp = new FormLocalizationProvider(LocalizationProvider.getInstance(), this);;

	private Path openedFilePath = null;
	private int length = 0;
	private JLabel statusLabel1 = new JLabel(" length: " + length);
	private JLabel statusLabel2 = new JLabel("Ln:0  Col:0  Sel:0");
	private String savedText = "";
	int index = 0;
	JMenuItem invertItem;
	JMenuItem upperCaseItem;
	JMenuItem lowerCaseItem;
	JMenuItem ascendingItem;
	JMenuItem descendingItem;
	JMenuItem uniqueItem;
	
	/**
	 * Constructor.
	 */
	public JNotepadPP() {
		
		setDefaultCloseOperation(WindowConstants.DO_NOTHING_ON_CLOSE);
		addWindowListener(new WindowAdapter() {
			@Override
			public void windowClosing(WindowEvent arg0) {
				String[] options = {"YES", "NO"};
				List<SingleDocumentModel> list = new ArrayList<>();
				tabbedPanel.iterator().forEachRemaining(a -> {
					if(a.isModified())
						list.add(a);
					});
				if (list.size() > 0) {
					for (SingleDocumentModel model: list) {
						int x = JOptionPane.showOptionDialog(JNotepadPP.this, "Do you want to save changes?",
				                "Changes", JOptionPane.DEFAULT_OPTION, JOptionPane.PLAIN_MESSAGE, null, options, options[0]);

				        if (x == 0) 
				        	saveDocumentAction.actionPerformed(null);
				        else {
				        	if (tabbedPanel.getNumberOfDocuments() == 1)
				        		System.exit(0);
				        	else 
				        		tabbedPanel.closeDocument(model);
				        }
					}
				}
				System.exit(0);

			}
		});
		setLocation(0, 0);
		setSize(600, 600);
		
		setTitle((tab.getTextComponent().getName() == null ? "(unnamed)" : tabbedPanel.getCurrentDocument().getTextComponent().getName()) + " - JNotepad++");	
				
		initGUI();
	}
	
	/**
	 * Creates GUI.
	 */
	private void initGUI() {
		
		tab = tabbedPanel.createNewDocument();

		tabbedPanel.getCurrentDocument().getTextComponent().addCaretListener(new CaretListener() {
			@Override
			public void caretUpdate(CaretEvent arg0) {
				length = tabbedPanel.getCurrentDocument().getTextComponent().getDocument().getLength();
				statusLabel1.setText(" length: " + length);
				
				int pos = tabbedPanel.getCurrentDocument().getTextComponent().getCaretPosition();
				int row = tabbedPanel.getCurrentDocument().getTextComponent().getDocument().getDefaultRootElement().getElementIndex(pos);
				int col = pos - tabbedPanel.getCurrentDocument().getTextComponent().getDocument().getDefaultRootElement().getStartOffset();
				int sel = tabbedPanel.getCurrentDocument().getTextComponent().getSelectionEnd() - tabbedPanel.getCurrentDocument().getTextComponent().getSelectionStart();
				statusLabel2.setText("Ln:" + (row + 1) + "  Col:" + (col + 1) + "  Sel:" + sel);
				
				int dot = arg0.getDot();
				int mark = arg0.getMark();
				if(dot == mark || length == 0) {
					invertItem.setEnabled(false);
					upperCaseItem.setEnabled(false);
					lowerCaseItem.setEnabled(false);
					ascendingItem.setEnabled(false);
					descendingItem.setEnabled(false);
					uniqueItem.setEnabled(false);
				}
				else {
					invertItem.setEnabled(true);
					upperCaseItem.setEnabled(true);
					lowerCaseItem.setEnabled(true);
					ascendingItem.setEnabled(true);
					descendingItem.setEnabled(true);
					uniqueItem.setEnabled(true);
				}
			}	
		});
		
		this.getContentPane().setLayout(new BorderLayout());
		this.getContentPane().add(tabbedPanel, BorderLayout.CENTER);
		
		tabbedPanel.addChangeListener(new ChangeListener() {        
			public void stateChanged(ChangeEvent e) { 
				String title;
				try {
					title = tabbedPanel.getCurrentDocument().getFilePath().getFileName().toString();
				}
				catch (NullPointerException | IndexOutOfBoundsException ex) {
					title = null;
				}
				setTitle((title == null ? "(unnamed)" : title) + " - JNotepad++");
				length = tabbedPanel.getCurrentDocument().getTextComponent().getDocument().getLength();
				statusLabel1.setText(" length: " + length);
				
				tabbedPanel.getCurrentDocument().getTextComponent().addCaretListener(new CaretListener() {
					@Override
					public void caretUpdate(CaretEvent arg0) {
						length = tabbedPanel.getCurrentDocument().getTextComponent().getDocument().getLength();
						statusLabel1.setText(" length: " + length);
						
						int pos = tabbedPanel.getCurrentDocument().getTextComponent().getCaretPosition();
						int row = tabbedPanel.getCurrentDocument().getTextComponent().getDocument().getDefaultRootElement().getElementIndex(pos);
						int col = pos - tabbedPanel.getCurrentDocument().getTextComponent().getDocument().getDefaultRootElement().getStartOffset();
						int sel = tabbedPanel.getCurrentDocument().getTextComponent().getSelectionEnd() - tabbedPanel.getCurrentDocument().getTextComponent().getSelectionStart();
						statusLabel2.setText("Ln:" + (row + 1) + "  Col:" + (col + 1) + "  Sel:" + sel);
						
						int dot = arg0.getDot();
						int mark = arg0.getMark();
						if(dot == mark || length == 0) {
							invertItem.setEnabled(false);
							upperCaseItem.setEnabled(false);
							lowerCaseItem.setEnabled(false);
							ascendingItem.setEnabled(false);
							descendingItem.setEnabled(false);
							uniqueItem.setEnabled(false);
						}
						else {
							invertItem.setEnabled(true);
							upperCaseItem.setEnabled(true);
							lowerCaseItem.setEnabled(true);
							ascendingItem.setEnabled(true);
							descendingItem.setEnabled(true);
							uniqueItem.setEnabled(true);
						}
					}
				});		
			}    
		});
		
		createMenus();
		createToolbars();
		createStatusBar();
	}
	
	private Action newFileAction = new LocalizableAction("New", flp, "control N", KeyEvent.VK_N, "Create new blank document.") {		
		
		private static final long serialVersionUID = 1L;

		@Override
		public void actionPerformed(ActionEvent arg0) {
			tab = tabbedPanel.createNewDocument();
			tab.addSingleDocumentListener(new SingleDocumentListener() {
				
				@Override
				public void documentModifyStatusUpdated(SingleDocumentModel model) {
					// TODO Auto-generated method stub
					
				}
				
				@Override
				public void documentFilePathUpdated(SingleDocumentModel model) {
					// TODO Auto-generated method stub
					
				}
			});
		}
		
	};

	private Action openDocumentAction = new LocalizableAction("Open", flp, "control O", KeyEvent.VK_O, "Used to open existing file from disk.") {
		
		private static final long serialVersionUID = 1L;

		@Override
		public void actionPerformed(ActionEvent e) {
			JFileChooser fc = new JFileChooser();
			fc.setDialogTitle("Open file");
			if(fc.showOpenDialog(JNotepadPP.this)!=JFileChooser.APPROVE_OPTION) {
				return;
			}
			File fileName = fc.getSelectedFile();
			Path filePath = fileName.toPath();
			tabbedPanel.loadDocument(filePath);
		}
	};
	
	private Action saveDocumentAction = new LocalizableAction("Save", flp, "control S", KeyEvent.VK_S, "Used to save current file to disk.") {
		
		private static final long serialVersionUID = 1L;
		
		@Override
		public void actionPerformed(ActionEvent e) {
			tab = tabbedPanel.getCurrentDocument();
			openedFilePath = tab.getFilePath();
			if(openedFilePath==null) {
				JFileChooser jfc = new JFileChooser();
				jfc.setDialogTitle("Save document");
				if(jfc.showSaveDialog(JNotepadPP.this)!=JFileChooser.APPROVE_OPTION) {
					JOptionPane.showMessageDialog(
							JNotepadPP.this, 
							"Ništa nije snimljeno.", 
							"Upozorenje", 
							JOptionPane.WARNING_MESSAGE);
					return;
				}
				openedFilePath = jfc.getSelectedFile().toPath();
			}
			byte[] podatci = tab.getTextComponent().getText().getBytes(StandardCharsets.UTF_8);
			try {
				Files.write(openedFilePath, podatci);
				tabbedPanel.saveDocument(tab, openedFilePath);
			} catch (IOException e1) {
				JOptionPane.showMessageDialog(
						JNotepadPP.this, 
						"Pogreška prilikom zapisivanja datoteke "+openedFilePath.toFile().getAbsolutePath()+".\nPažnja: nije jasno u kojem je stanju datoteka na disku!", 
						"Pogreška", 
						JOptionPane.ERROR_MESSAGE);
				return;
			}
			JOptionPane.showMessageDialog(
					JNotepadPP.this, 
					"Datoteka je snimljena.", 
					"Informacija", 
					JOptionPane.INFORMATION_MESSAGE);
		}
	};
	
	private Action saveAsDocumentAction = new LocalizableAction("SaveAs", flp, "control A", KeyEvent.VK_A, "Used to save current file to disk.") {
		
		private static final long serialVersionUID = 1L;
		
		@Override
		public void actionPerformed(ActionEvent e) {
			tab = tabbedPanel.getCurrentDocument();
			JFileChooser jfc = new JFileChooser();
			jfc.setDialogTitle("Save document");
			if(jfc.showSaveDialog(JNotepadPP.this)!=JFileChooser.APPROVE_OPTION) {
				JOptionPane.showMessageDialog(
						JNotepadPP.this, 
						"Ništa nije snimljeno.", 
						"Upozorenje", 
						JOptionPane.WARNING_MESSAGE);
				return;
			}
			openedFilePath = jfc.getSelectedFile().toPath();
			byte[] podatci = tab.getTextComponent().getText().getBytes(StandardCharsets.UTF_8);
			try {
				Files.write(openedFilePath, podatci);
				tabbedPanel.saveDocument(tab, openedFilePath);
			} catch (IOException e1) {
				JOptionPane.showMessageDialog(
						JNotepadPP.this, 
						"Pogreška prilikom zapisivanja datoteke "+openedFilePath.toFile().getAbsolutePath()+".\nPažnja: nije jasno u kojem je stanju datoteka na disku!", 
						"Pogreška", 
						JOptionPane.ERROR_MESSAGE);
				return;
			}
			JOptionPane.showMessageDialog(
					JNotepadPP.this, 
					"Datoteka je snimljena.", 
					"Informacija", 
					JOptionPane.INFORMATION_MESSAGE);
		}
	};
	
	private Action copyAction = new LocalizableAction("Copy", flp, "control C", KeyEvent.VK_C, "Copy the selected part of text.") {
		
		private static final long serialVersionUID = 1L;
		
		@Override
		public void actionPerformed(ActionEvent e) {
			tab = tabbedPanel.getCurrentDocument();
			Document doc = tab.getTextComponent().getDocument();
			int len = Math.abs(tab.getTextComponent().getCaret().getDot()-tab.getTextComponent().getCaret().getMark());
			if(len==0) return;
			int offset = Math.min(tab.getTextComponent().getCaret().getDot(),tab.getTextComponent().getCaret().getMark());
			try {
				savedText = doc.getText(offset, len);
			} catch (BadLocationException e1) {
				e1.printStackTrace();
			}
		}
	};
	
	private Action cutAction = new LocalizableAction("Cut", flp, "control X", KeyEvent.VK_X, "Cut the selected part of text.") {
		
		private static final long serialVersionUID = 1L;
		
		@Override
		public void actionPerformed(ActionEvent e) {
			tab = tabbedPanel.getCurrentDocument();
			Document doc = tab.getTextComponent().getDocument();
			int len = Math.abs(tab.getTextComponent().getCaret().getDot()-tab.getTextComponent().getCaret().getMark());
			if(len==0) return;
			int offset = Math.min(tab.getTextComponent().getCaret().getDot(),tab.getTextComponent().getCaret().getMark());
			try {
				savedText = doc.getText(offset, len);
				doc.remove(offset, len);
			} catch (BadLocationException e1) {
				e1.printStackTrace();
			}
		}
	};
	
	private Action pasteAction = new LocalizableAction("Paste", flp, "control P", KeyEvent.VK_P, "Paste the selected part of text.") {
		
		private static final long serialVersionUID = 1L;
		
		@Override
		public void actionPerformed(ActionEvent e) {
			tab = tabbedPanel.getCurrentDocument();
			Document doc = tab.getTextComponent().getDocument();
			int len = Math.abs(tab.getTextComponent().getCaret().getDot()-tab.getTextComponent().getCaret().getMark());
			int offset = Math.min(tab.getTextComponent().getCaret().getDot(),tab.getTextComponent().getCaret().getMark());
			if(len==0) {
				try {
					doc.insertString(offset, savedText, null);
				} catch (BadLocationException e1) {
					e1.printStackTrace();
				}
				return;
			}
			try {
				doc.remove(offset, len);
				doc.insertString(offset, savedText, null);
			} catch (BadLocationException e1) {
				e1.printStackTrace();
			}
		}
	};
	
	private Action invertAction = new LocalizableAction("Invert", flp, "control T", KeyEvent.VK_T, "Used to toggle character case in selected part of text or in entire document.") {
		
		private static final long serialVersionUID = 1L;
		
		@Override
		public void actionPerformed(ActionEvent e) {
			Document doc = tabbedPanel.getCurrentDocument().getTextComponent().getDocument();
			int len = Math.abs(tabbedPanel.getCurrentDocument().getTextComponent().getCaret().getDot()-tabbedPanel.getCurrentDocument().getTextComponent().getCaret().getMark());
			int offset = 0;
			if(len!=0) {
				offset = Math.min(tabbedPanel.getCurrentDocument().getTextComponent().getCaret().getDot(),tabbedPanel.getCurrentDocument().getTextComponent().getCaret().getMark());
			} 
			try {
				String text = doc.getText(offset, len);
				text = changeCase(text);
				doc.remove(offset, len);
				doc.insertString(offset, text, null);
			} catch(BadLocationException ex) {
				ex.printStackTrace();
			}
		}

		private String changeCase(String text) {
			char[] znakovi = text.toCharArray();
			for(int i = 0; i < znakovi.length; i++) {
				char c = znakovi[i];
				if(Character.isLowerCase(c)) {
					znakovi[i] = Character.toUpperCase(c);
				} else if(Character.isUpperCase(c)) {
					znakovi[i] = Character.toLowerCase(c);
				}
			}
			return new String(znakovi);
		}
	};
	
	
	private Action upperCaseAction = new LocalizableAction("Uppercase", flp) {
		
		private static final long serialVersionUID = 1L;
		
		@Override
		public void actionPerformed(ActionEvent e) {
			Document doc = tabbedPanel.getCurrentDocument().getTextComponent().getDocument();
			int len = Math.abs(tabbedPanel.getCurrentDocument().getTextComponent().getCaret().getDot()-tabbedPanel.getCurrentDocument().getTextComponent().getCaret().getMark());
			int offset = 0;
			if(len!=0) {
				offset = Math.min(tabbedPanel.getCurrentDocument().getTextComponent().getCaret().getDot(),tabbedPanel.getCurrentDocument().getTextComponent().getCaret().getMark());
			}
			try {
				String text = doc.getText(offset, len);
				text = changeCase(text);
				doc.remove(offset, len);
				doc.insertString(offset, text, null);
			} catch(BadLocationException ex) {
				ex.printStackTrace();
			}
		}

		private String changeCase(String text) {
			char[] znakovi = text.toCharArray();
			for(int i = 0; i < znakovi.length; i++) {
				char c = znakovi[i];
				if(Character.isLowerCase(c)) {
					znakovi[i] = Character.toUpperCase(c);
				} 
			}
			return new String(znakovi);
		}
	};
	
	private Action lowerCaseAction = new LocalizableAction("Lowercase", flp) {
		
		private static final long serialVersionUID = 1L;
		
		@Override
		public void actionPerformed(ActionEvent e) {
			Document doc = tabbedPanel.getCurrentDocument().getTextComponent().getDocument();
			int len = Math.abs(tabbedPanel.getCurrentDocument().getTextComponent().getCaret().getDot()-tabbedPanel.getCurrentDocument().getTextComponent().getCaret().getMark());
			int offset = 0;
			if(len!=0) {
				offset = Math.min(tabbedPanel.getCurrentDocument().getTextComponent().getCaret().getDot(),tabbedPanel.getCurrentDocument().getTextComponent().getCaret().getMark());
			} 
			try {
				String text = doc.getText(offset, len);
				text = changeCase(text);
				doc.remove(offset, len);
				doc.insertString(offset, text, null);
			} catch(BadLocationException ex) {
				ex.printStackTrace();
			}
		}

		private String changeCase(String text) {
			char[] znakovi = text.toCharArray();
			for(int i = 0; i < znakovi.length; i++) {
				char c = znakovi[i];
				if(Character.isUpperCase(c)) {
					znakovi[i] = Character.toLowerCase(c);
				} 
			}
			return new String(znakovi);
		}
	};
	
	private Action ascendingAction = new LocalizableAction("Ascending", flp) {
		
		private static final long serialVersionUID = 1L;
		
		@Override
		public void actionPerformed(ActionEvent e) {
			JTextArea text = tabbedPanel.getCurrentDocument().getTextComponent();
			Document doc = text.getDocument();
			int len = Math.abs(tabbedPanel.getCurrentDocument().getTextComponent().getCaret().getDot()-tabbedPanel.getCurrentDocument().getTextComponent().getCaret().getMark());
			int startOffset = 0;
			int endOffset = doc.getLength();
			if(len!=0) {
				startOffset = Math.min(tabbedPanel.getCurrentDocument().getTextComponent().getCaret().getDot(),tabbedPanel.getCurrentDocument().getTextComponent().getCaret().getMark());
				endOffset = Math.max(tabbedPanel.getCurrentDocument().getTextComponent().getCaret().getDot(),tabbedPanel.getCurrentDocument().getTextComponent().getCaret().getMark());
			} else {
				len = doc.getLength();
			}
			try {
				int startLine = text.getLineOfOffset(startOffset);
				int endLine = text.getLineOfOffset(endOffset);
				
				String[] lines = new String[endLine - startLine + 1];
				int index = 0;
				for (int i = startLine; i <= endLine; i++) {
					int leng = text.getLineEndOffset(i) - text.getLineStartOffset(i);
					lines[index++] = doc.getText(text.getLineStartOffset(i), leng).replaceAll("\n", "");					
				}
				doc.remove(text.getLineStartOffset(startLine), text.getLineEndOffset(endLine));
				for (int j = 1; j < lines.length; j++) {
					String line = lines[j];
					Locale locale = new Locale(LocalizationProvider.getInstance().getLanguage());
					Collator collator = Collator.getInstance(locale);
					int k = j - 1;
					while (k >= 0 && collator.compare(line, lines[k]) <= 0){
			           lines[k+1] = lines[k];
			            k--;
			        }
			        lines[k+1] = line;
				}
				
				doc.insertString(text.getLineStartOffset(startLine), String.join("\n", lines), null);				
			} catch (BadLocationException e1) {
				e1.printStackTrace();
			}
		}
	};
	
	
	private Action descendingAction = new LocalizableAction("Descending", flp) {
		
		private static final long serialVersionUID = 1L;
		
		@Override
		public void actionPerformed(ActionEvent e) {
			JTextArea text = tabbedPanel.getCurrentDocument().getTextComponent();
			Document doc = text.getDocument();
			int len = Math.abs(tabbedPanel.getCurrentDocument().getTextComponent().getCaret().getDot()-tabbedPanel.getCurrentDocument().getTextComponent().getCaret().getMark());
			int startOffset = 0;
			int endOffset = doc.getLength();
			if(len!=0) {
				startOffset = Math.min(tabbedPanel.getCurrentDocument().getTextComponent().getCaret().getDot(),tabbedPanel.getCurrentDocument().getTextComponent().getCaret().getMark());
				endOffset = Math.max(tabbedPanel.getCurrentDocument().getTextComponent().getCaret().getDot(),tabbedPanel.getCurrentDocument().getTextComponent().getCaret().getMark());
			} else {
				len = doc.getLength();
			}
			try {
				int startLine = text.getLineOfOffset(startOffset);
				int endLine = text.getLineOfOffset(endOffset);
				
				String[] lines = new String[endLine - startLine + 1];
				int index = 0;
				for (int i = startLine; i <= endLine; i++) {
					int leng = text.getLineEndOffset(i) - text.getLineStartOffset(i);
					lines[index++] = doc.getText(text.getLineStartOffset(i), leng).replaceAll("\n", "");					
				}
				doc.remove(text.getLineStartOffset(startLine), text.getLineEndOffset(endLine));
				for (int j = 1; j < lines.length; j++) {
					String line = lines[j];
					Locale locale = new Locale(LocalizationProvider.getInstance().getLanguage());
					Collator collator = Collator.getInstance(locale);
					int k = j - 1;
					while (k >= 0 && collator.compare(line, lines[k]) >= 0){
			           lines[k+1] = lines[k];
			            k--;
			        }
			        lines[k+1] = line;
				}
				
				doc.insertString(text.getLineStartOffset(startLine), String.join("\n", lines), null);				
			} catch (BadLocationException e1) {
				e1.printStackTrace();
			}
		}
	};
	
	private Action uniqueAction = new LocalizableAction("Unique", flp) {
		
		private static final long serialVersionUID = 1L;
		
		@Override
		public void actionPerformed(ActionEvent e) {
			JTextArea text = tabbedPanel.getCurrentDocument().getTextComponent();
			Document doc = text.getDocument();
			int len = Math.abs(tabbedPanel.getCurrentDocument().getTextComponent().getCaret().getDot()-tabbedPanel.getCurrentDocument().getTextComponent().getCaret().getMark());
			int startOffset = 0;
			int endOffset = doc.getLength();
			if(len!=0) {
				startOffset = Math.min(tabbedPanel.getCurrentDocument().getTextComponent().getCaret().getDot(),tabbedPanel.getCurrentDocument().getTextComponent().getCaret().getMark());
				endOffset = Math.max(tabbedPanel.getCurrentDocument().getTextComponent().getCaret().getDot(),tabbedPanel.getCurrentDocument().getTextComponent().getCaret().getMark());
			} else {
				len = doc.getLength();
			}
			try {
				int startLine = text.getLineOfOffset(startOffset);
				int endLine = text.getLineOfOffset(endOffset);
				
				String[] lines = new String[endLine - startLine + 1];
				List<String> result = new ArrayList<>();
				int index = 0;
				for (int i = startLine; i <= endLine; i++) {
					int leng = text.getLineEndOffset(i) - text.getLineStartOffset(i);
					lines[index++] = doc.getText(text.getLineStartOffset(i), leng).replaceAll("\n", "");					
				}
				doc.remove(text.getLineStartOffset(startLine), text.getLineEndOffset(endLine));
				for (int j = lines.length - 1; j >= 0; j--) {
					Locale locale = new Locale(LocalizationProvider.getInstance().getLanguage());
					Collator collator = Collator.getInstance(locale);
					boolean equal = false;
					for (int k = j - 1; k >= 0; k--) {
						if (collator.compare(lines[j], lines[k]) == 0)
							equal = true;
					}
					if (!equal)
						result.add(lines[j]);
				}
				
				Collections.reverse(result);
				doc.insertString(text.getLineStartOffset(startLine), String.join("\n", result), null);				
			} catch (BadLocationException e1) {
				e1.printStackTrace();
			}
		}
	};
	
	
	private Action fileStatistcsAction = new LocalizableAction("Statistics", flp, "control I", KeyEvent.VK_I, "Statistical info on document.") {
		
		private static final long serialVersionUID = 1L;
		
		@Override
		public void actionPerformed(ActionEvent e) {
			String[] parts = tab.getTextComponent().getText().trim().split("\\s+");
			JOptionPane.showMessageDialog(JNotepadPP.this, "Your document has " + tab.getTextComponent().getText().length() + 
					" characters, " + parts.length + 
					" non-blank characters and " + tab.getTextComponent().getLineCount() + " lines.");
		}
	};
	
	
	private Action closeTabAction = new LocalizableAction("CT", flp, "control R", KeyEvent.VK_R, "Close selected tab.") {
		
		private static final long serialVersionUID = 1L;
		
		@Override
		public void actionPerformed(ActionEvent e) {
			tab = tabbedPanel.getCurrentDocument();
			if (tabbedPanel.getNumberOfDocuments() == 1)
        		System.exit(0);
        	else 
        		tabbedPanel.closeDocument(tab);
		}
	};
	
	private Action exitAction = new LocalizableAction("Exit", flp, "control E", KeyEvent.VK_E, "Exit application.") {
		
		private static final long serialVersionUID = 1L;
		
		@Override
		public void actionPerformed(ActionEvent e) {
			tab = tabbedPanel.getCurrentDocument();
			String[] options = {"YES", "NO"};
			List<SingleDocumentModel> list = new ArrayList<>();
			tabbedPanel.iterator().forEachRemaining(a -> {
				if(a.isModified())
					list.add(a);
				});
			if (list.size() > 0) {
				for (SingleDocumentModel model: list) {
					int x = JOptionPane.showOptionDialog(null, "Do you want to save changes?",
			                "Changes", JOptionPane.DEFAULT_OPTION, JOptionPane.PLAIN_MESSAGE, null, options, options[0]);

			        if (x == 0) 
			        	saveDocumentAction.actionPerformed(null);
			        else {
			        	if (tabbedPanel.getNumberOfDocuments() == 1)
			        		System.exit(0);
			        	else 
			        		tabbedPanel.closeDocument(model);
			        }
				}
			}
			System.exit(0);
		}
	};

	/**
	 * Creates menus.
	 */
	private void createMenus() {
		JMenuBar menuBar = new JMenuBar();
		
		JMenu fileMenu = new JMenu(new LocalizableAction("File", flp) {

			private static final long serialVersionUID = 1L;
			
		});
		menuBar.add(fileMenu);

		fileMenu.add(new JMenuItem(newFileAction));
		fileMenu.add(new JMenuItem(openDocumentAction));
		fileMenu.add(new JMenuItem(saveDocumentAction));
		fileMenu.add(new JMenuItem(saveAsDocumentAction));
		fileMenu.add(new JMenuItem(fileStatistcsAction));
		fileMenu.add(new JMenuItem(closeTabAction));
		fileMenu.addSeparator();
		fileMenu.add(new JMenuItem(exitAction));
		
		JMenu editMenu = new JMenu(new LocalizableAction("Edit", flp) {

			private static final long serialVersionUID = 1L;
			
		});
		menuBar.add(editMenu);
		
		editMenu.add(new JMenuItem(copyAction));
		editMenu.add(new JMenuItem(cutAction));
		editMenu.add(new JMenuItem(pasteAction));
		
		JMenu languageMenu = new JMenu(new LocalizableAction("Language", flp) {

			private static final long serialVersionUID = 1L;
			
		});
		menuBar.add(languageMenu);
		
		JMenuItem hrItem = new JMenuItem(new LocalizableAction("Croatian", flp) {

			private static final long serialVersionUID = 1L;
			
		});
		JMenuItem enItem = new JMenuItem(new LocalizableAction("English", flp) {

			private static final long serialVersionUID = 1L;
			
		});
		JMenuItem deItem = new JMenuItem(new LocalizableAction("German", flp) {

			private static final long serialVersionUID = 1L;
			
		});
		
		hrItem.addActionListener(l -> LocalizationProvider.getInstance().setLanguage("hr"));
		enItem.addActionListener(l -> LocalizationProvider.getInstance().setLanguage("en"));
		deItem.addActionListener(l -> LocalizationProvider.getInstance().setLanguage("de"));
		
		languageMenu.add(hrItem);
		languageMenu.add(enItem);
		languageMenu.add(deItem);
		
		JMenu toolsMenu = new JMenu(new LocalizableAction("Tools", flp) {

			private static final long serialVersionUID = 1L;
			
		});
		menuBar.add(toolsMenu);
		
		JMenu changeCaseMenu = new JMenu(new LocalizableAction("Change_case", flp) {

			private static final long serialVersionUID = 1L;
			
		});
		toolsMenu.add(changeCaseMenu);
		
		invertItem = new JMenuItem(invertAction);
		upperCaseItem = new JMenuItem(upperCaseAction);
		lowerCaseItem = new JMenuItem(lowerCaseAction);
		changeCaseMenu.add(upperCaseItem);
		changeCaseMenu.add(lowerCaseItem);
		changeCaseMenu.add(invertItem);
		
		JMenu sortMenu = new JMenu(new LocalizableAction("Sort", flp) {

			private static final long serialVersionUID = 1L;
			
		});
		toolsMenu.add(sortMenu);
		
		ascendingItem = new JMenuItem(ascendingAction);
		descendingItem = new JMenuItem(descendingAction);
		uniqueItem = new JMenuItem(uniqueAction);
		sortMenu.add(ascendingItem);
		sortMenu.add(descendingItem);
		toolsMenu.add(uniqueItem);
		
		flp.addLocalizationListener(new ILocalizationListener() {
			@Override
			public void localizationChanged() {
				fileMenu.setText(LocalizationProvider.getInstance().getString("File"));
				editMenu.setText(LocalizationProvider.getInstance().getString("Edit"));
				languageMenu.setText(LocalizationProvider.getInstance().getString("Language"));
				hrItem.setText(LocalizationProvider.getInstance().getString("Croatian"));
				enItem.setText(LocalizationProvider.getInstance().getString("English"));
				deItem.setText(LocalizationProvider.getInstance().getString("German"));
				changeCaseMenu.setText(LocalizationProvider.getInstance().getString("Change_case"));
				toolsMenu.setText(LocalizationProvider.getInstance().getString("Tools"));
			}
		});	
		
		this.setJMenuBar(menuBar);
	}

	/**
	 * Creates tool bars.
	 */
	private void createToolbars() {
		JToolBar toolBar = new JToolBar("Alati");
		toolBar.setFloatable(true);
		
		toolBar.add(new JButton(newFileAction));
		toolBar.add(new JButton(openDocumentAction));
		toolBar.add(new JButton(saveDocumentAction));
		toolBar.add(new JButton(saveAsDocumentAction));
		toolBar.add(new JButton(closeTabAction));
		toolBar.addSeparator();
		toolBar.add(new JButton(copyAction));
		toolBar.add(new JButton(cutAction));
		toolBar.add(new JButton(pasteAction));
		
		this.getContentPane().add(toolBar, BorderLayout.PAGE_START);
	}
	
	/**
	 * Creates status bar.
	 */
	private void createStatusBar() {
		JPanel statusBar = new JPanel(new GridLayout());
			
		statusLabel1.setHorizontalAlignment(SwingConstants.LEFT);
		statusBar.add(statusLabel1);
		
		statusLabel2.setHorizontalAlignment(SwingConstants.LEFT);
		statusBar.add(statusLabel2);
		
		SimpleDateFormat dt = new SimpleDateFormat("yyyy/MM/dd HH:mm:ss"); 
		Date date = new Date(System.currentTimeMillis());
		JLabel statusLabel3 = new JLabel(dt.format(date));
		statusLabel3.setHorizontalAlignment(SwingConstants.RIGHT);
		statusBar.add(statusLabel3);
        
		Timer t = new Timer(1000, new ActionListener() {  
            public void actionPerformed(ActionEvent e) {  
                statusLabel3.setText(dt.format(new Date(System.currentTimeMillis())));
            }  
        });  
        t.start(); 
        
		this.getContentPane().add(statusBar, BorderLayout.PAGE_END);
	}
	
	/**
	 * Main method.
	 * @param args
	 */
	public static void main(String[] args) {
		SwingUtilities.invokeLater(new Runnable() {
			
			@Override
			public void run() {
				new JNotepadPP().setVisible(true);
			}
		});
	}

}
