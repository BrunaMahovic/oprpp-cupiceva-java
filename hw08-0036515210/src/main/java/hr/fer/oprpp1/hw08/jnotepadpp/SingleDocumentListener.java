package hr.fer.oprpp1.hw08.jnotepadpp;

/**
 * Interface SingleDocumentListener models a listener for document.
 * 
 * @author Bruna
 *
 */
public interface SingleDocumentListener {

	/**
	 * Updates the status of the document.
	 * @param model
	 */
	void documentModifyStatusUpdated(SingleDocumentModel model);
	
	/**
	 * Updates file path.
	 * @param model
	 */
	void documentFilePathUpdated(SingleDocumentModel model);
}
